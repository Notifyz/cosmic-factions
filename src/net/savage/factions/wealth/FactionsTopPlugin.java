package net.savage.factions.wealth;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.P;

import net.milkbowl.vault.economy.Economy;
import net.savage.factions.wealth.command.FTopCommand;
import net.savage.factions.wealth.listener.EssentialsEcoListener;
import net.savage.factions.wealth.listener.FactionEventsListener;
import net.savage.factions.wealth.listener.FactionPlayerListener;
import net.savage.factions.wealth.listener.PListener;
import net.savage.factions.wealth.object.BlockWorth;
import net.savage.factions.wealth.object.FTop;
import net.savage.factions.wealth.object.ItemWorth;
import net.savage.factions.wealth.object.SpawnerWorth;
import net.savage.factions.wealth.runnable.AutoRecalculate;
import net.savage.factions.wealth.runnable.UpdateRankings;
import net.savage.factions.wealth.task.AutoCalculateServerFtopTotal;
import net.savage.factions.wealth.task.AutoRecalculateRichestMember;
import net.savage.factions.wealth.util.FTopHelper;
import net.savage.factions.wealth.util.NumberFormattingUtil;
import net.savage.factions.wealth.util.SortingUtil;
import net.savage.factions.wealth.util.UpdateChecker;
import net.savage.factions.wealth.util.UserInterface;
import net.savage.factions.wealth.util.Util;
import net.savage.factions.wealth.util.Files.FileUtil;
import net.savage.factions.wealth.util.Files.ItemsUtilv2;
import net.savage.factions.wealth.util.Files.SpawnerStackingHandler;
import net.savage.factions.wealth.util.Files.SpawnerUtilv2;
import net.savage.factions.wealth.util.Scanners.Scannerv2;

public class FactionsTopPlugin {
    private static FactionsTopPlugin instance;
    public HashMap<String, FTop> unsorted;
    public HashMap<Integer, String> rankings;
    private HashMap<String, Integer> rankings_by_factionid;
    public int total_amount_of_chunks_to_calculate;
    public int total_amount_of_chunks_calculated;
    public Economy economy;
    public long calculation_start_ms;
    public long calculation_stop_ms;
    public double server_total;
    public ArrayList<FLocation> tocalculate;
    public boolean is_calculating;
    public boolean above1_7;
    private ArrayList<BlockWorth> blockworth;
    private ArrayList<SpawnerWorth> spawnerworth;
    private ArrayList<ItemWorth> itemworth;
    private FileUtil fileutil;
    private ItemsUtilv2 itemsutilv2;
    private SpawnerUtilv2 spawnerutilv2;
    private SortingUtil sortingutil;
    private Util util;
    private UserInterface userinterface;
    private FTopHelper ftophelper;
    private Scannerv2 scannerv2;
    private NumberFormattingUtil numberformattingutil;
    private UpdateChecker updatechecker;
    private SpawnerStackingHandler spawnerhandler;

    public FactionsTopPlugin() {
        this.unsorted = new HashMap<>();
        this.rankings = new HashMap<>();
        this.rankings_by_factionid = new HashMap<>();
        this.total_amount_of_chunks_to_calculate = 0;
        this.server_total = 0.0;
        this.tocalculate = new ArrayList<>();
        this.is_calculating = false;
        this.blockworth = new ArrayList<>();
        this.spawnerworth = new ArrayList<>();
        this.itemworth = new ArrayList<>();
        this.numberformattingutil = new NumberFormattingUtil();
        this.updatechecker = new UpdateChecker();
        this.spawnerhandler = new SpawnerStackingHandler();
    }

	public void onEnable() {
        FactionsTopPlugin.instance = this;
        this.fileutil = new FileUtil();
        this.itemsutilv2 = new ItemsUtilv2();
        this.spawnerutilv2 = new SpawnerUtilv2();
        this.userinterface = new UserInterface();
        this.ftophelper = new FTopHelper();
        this.scannerv2 = new Scannerv2();
        this.util = new Util();
        this.sortingutil = new SortingUtil();
        this.getFileUtil().setup(P.getInstance().getDataFolder());
        this.getItemsUtil().setup(P.getInstance().getDataFolder());
        this.getSpawnerUtil().setup(P.getInstance().getDataFolder());
        if (!this.setupEconomy()) {
        	P.getInstance().onDisable();
            return;
        }
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(P.getInstance(), new AutoRecalculate(), 0L, 1800 * 20);
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(P.getInstance(), new UpdateRankings(), 0L, 1200L);
        Bukkit.getServer().getPluginManager().registerEvents(new PListener(), P.getInstance());
        if (Bukkit.getServer().getPluginManager().isPluginEnabled("Essentials")) {
            Bukkit.getServer().getPluginManager().registerEvents(new EssentialsEcoListener(), P.getInstance());
        }
        if (Bukkit.getServer().getPluginManager().isPluginEnabled("Factions")) {
            Bukkit.getServer().getPluginManager().registerEvents(new FactionPlayerListener(), P.getInstance());
            Bukkit.getServer().getPluginManager().registerEvents(new FactionEventsListener(), P.getInstance());
        }
        Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask(P.getInstance(), new AutoRecalculateRichestMember(), 0L, 60 * 20);
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(P.getInstance(), new AutoCalculateServerFtopTotal(), 0L, 60 * 20);
        P.getInstance().getCommand("ftop").setExecutor(new FTopCommand());
        final String name = P.getInstance().getServer().getClass().getPackage().getName();
        if (!name.substring(name.lastIndexOf(46) + 1).startsWith("v1_7")) {
            this.above1_7 = true;
        }

        calculateFactions();
    }

	public void calculateFactions() {
        Factions.getInstance().getAllFactions().forEach(faction -> {
        	unsorted.put(faction.getId(), new FTop(faction.getId()));
        });
	}

    public boolean pingURL(final String s, final int n) {
        try {
            return InetAddress.getByName(s).isReachable(n);
        }
        catch (IOException ex) {
            return false;
        }
    }

	public void onDisable() {
        this.getItemWorth().clear();
        this.getBlockWorth().clear();
        this.getSpawnerWorth().clear();
        this.unsorted.clear();
        this.tocalculate.clear();
        this.rankings.clear();

        instance = null;
    }

    private boolean setupEconomy() {
        final RegisteredServiceProvider<Economy> registration = P.getInstance().getServer().getServicesManager().getRegistration(Economy.class);
        if (registration != null) {
            this.economy = registration.getProvider();
        }
        return this.economy != null;
    }

    public NumberFormattingUtil getNumberFormattingUtil() {
        return this.numberformattingutil;
    }

    public Scannerv2 getScannerv2() {
        return this.scannerv2;
    }

    public FTopHelper getFTopHelper() {
        return this.ftophelper;
    }

    public UserInterface getUserInterface() {
        return this.userinterface;
    }

    public ItemsUtilv2 getItemsUtil() {
        return this.itemsutilv2;
    }

    public SpawnerUtilv2 getSpawnerUtil() {
        return this.spawnerutilv2;
    }

    public UpdateChecker getUpdateChecker() {
        return this.updatechecker;
    }

    public FileUtil getFileUtil() {
        return this.fileutil;
    }

    public Util getUtil() {
        return this.util;
    }

    public SortingUtil getSortingUtil() {
        return this.sortingutil;
    }

    public ArrayList<BlockWorth> getBlockWorth() {
        return this.blockworth;
    }

    public ArrayList<ItemWorth> getItemWorth() {
        return this.itemworth;
    }

    public ArrayList<SpawnerWorth> getSpawnerWorth() {
        return this.spawnerworth;
    }

    public HashMap<String, Integer> getFactionsRankingByID() {
        return this.rankings_by_factionid;
    }

    public SpawnerStackingHandler getSpawnerHandler() {
        return this.spawnerhandler;
    }

    public static FactionsTopPlugin getPluginInstance() {
        return FactionsTopPlugin.instance;
    }
}
