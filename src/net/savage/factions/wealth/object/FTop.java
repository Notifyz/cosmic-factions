package net.savage.factions.wealth.object;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

import net.savage.factions.wealth.FactionsTopPlugin;

public class FTop
{

    private FactionsTopPlugin factionstopplugin;
    public String faction_id;
    public double block_balance;
    public double spawner_balance;
    public double item_worth;
    public String leader;
    public String faction_name;
    public String richest_member;
    public double richest_member_balance;
    public double current_ftop_totalworth;
    public double old_ftop_totalworth;
    public HashMap<BlockWorth, Integer> blocks;
    public HashMap<SpawnerWorth, Integer> spawners;
    public HashMap<ItemWorth, Integer> items;
    private double money_balance;

	private final Comparator<FPlayer> BALANCE_COMPARATOR = (player1, player2) -> {
		return Double.compare(factionstopplugin.economy.getBalance(Bukkit.getOfflinePlayer(UUID.fromString(player1.getId()))), factionstopplugin.economy.getBalance(Bukkit.getOfflinePlayer(UUID.fromString(player2.getId()))));
	};

    public FTop(final String faction_id) {
        this.richest_member = "Not Calculated";
        this.richest_member_balance = 0.0;
        this.current_ftop_totalworth = 0.0;
        this.old_ftop_totalworth = 0.0;
        this.blocks = new HashMap<>();
        this.spawners = new HashMap<>();
        this.items = new HashMap<>();
        this.faction_id = faction_id;
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public double getMoneyWorth() {
        return this.money_balance;
    }

    public void addMoneyToBalance(final double n) {
        this.money_balance += n;
    }

    public void removeFromFromBalance(final double n) {
        this.money_balance -= n;
    }

    public void resetMoneyBalance() {
        this.money_balance = 0.0;
    }

    public double getTotalValue() {
        double n = 0.0;
        if (true) {
            n += this.block_balance;
        }
        if (true) {
            n += this.spawner_balance;
        }
        if (true) {
            n += this.getMoneyWorth();
        }
        if (true) {
            n += this.item_worth;
        }
        return n;
    }

    public void setLeader(final String leader) {
        this.leader = leader;
    }

    public void setFactionName(final String faction_name) {
        this.faction_name = faction_name;
    }

    public void setSpawnerBalance(final double spawner_balance) {
        this.spawner_balance = spawner_balance;
    }

    public void setRichestMember(final String s, final double n) {
        if (this.richest_member.equalsIgnoreCase("Not Calculated")) {
            this.richest_member = s;
            this.money_balance = n;
            return;
        }
        this.richest_member = s;
        this.richest_member_balance = n;
    }

    public String[] getRichestMember() {
    	if (getFaction() != null) {
    		resetMoneyBalance();

    		List<FPlayer> balances = new ArrayList<>();
    		getFaction().getFPlayers().forEach(balances::add);
    		for (FPlayer player : getFaction().getFPlayers()) {
    			if (!factionstopplugin.economy.hasAccount(Bukkit.getOfflinePlayer(UUID.fromString(player.getId())))) {
    				factionstopplugin.economy.createPlayerAccount(Bukkit.getOfflinePlayer(UUID.fromString(player.getId())));
    			}

    			double playerBalance = factionstopplugin.economy.getBalance(Bukkit.getOfflinePlayer(UUID.fromString(player.getId())));
    			addMoneyToBalance(playerBalance);
    		}
    		Collections.sort(balances, BALANCE_COMPARATOR);
    		Collections.reverse(balances);

    		if (balances.get(0) == null) {
    	    	return new String[] {"None", "$0"};
    		}
    		OfflinePlayer richestPlayer = Bukkit.getOfflinePlayer(UUID.fromString(balances.get(0).getId()));
    		if (richestPlayer == null) {
    	    	return new String[] {"None", "$0"};
    		}
    		setRichestMember(richestPlayer.getName(), factionstopplugin.economy.getBalance(richestPlayer));
    		return new String[] {richestPlayer.getName(), factionstopplugin.getNumberFormattingUtil().formatProperly(factionstopplugin.economy.getBalance(richestPlayer), false)};
    	}
    	return new String[] {"None", "$0"};
    }

    public void findRichestMember() {
        if (this.getFaction() == null) {
            return;
        }
        this.resetMoneyBalance();
        Map<String, Double> hashMap = new HashMap<>();
        for (FPlayer object2 : this.getFaction().getFPlayers()) {
            OfflinePlayer offlinePlayer;
            if (!Bukkit.getServer().getOfflinePlayer(UUID.fromString(object2.getId())).hasPlayedBefore() || !this.factionstopplugin.economy.hasAccount(offlinePlayer = Bukkit.getServer().getOfflinePlayer(UUID.fromString(object2.getId())))) continue;
            double d = this.factionstopplugin.economy.getBalance(offlinePlayer);
            hashMap.put(offlinePlayer.getName(), d);
            this.addMoneyToBalance(d);
        }
        Map<String, Double> object2 = this.factionstopplugin.getUtil().sortByValue(hashMap);
        if (object2.size() >= 1) {
            Map.Entry<String, Double> entry = object2.entrySet().iterator().next();
            this.setRichestMember(entry.getKey(), entry.getValue());
            return;
        }
    }

    public void resetValues() {
        this.items.clear();
        this.spawners.clear();
        this.blocks.clear();
        this.item_worth = 0.0;
        this.block_balance = 0.0;
        this.spawner_balance = 0.0;
        this.richest_member = "Not Calculated";
        this.richest_member_balance = 0.0;
    }

    public int getAmountOfSpawner(final int n) {
        int intValue = 0;
        Object o = null;
        for (final SpawnerWorth spawnerWorth : FactionsTopPlugin.getPluginInstance().getSpawnerWorth()) {
            if (spawnerWorth.data == n) {
                o = spawnerWorth;
                break;
            }
        }
        if (o != null && this.spawners.containsKey(o)) {
            intValue = this.spawners.get(o);
        }
        return intValue;
    }

    public int getTotalSpawnerCount() {
        int n = 0;
        final Iterator<Map.Entry<SpawnerWorth, Integer>> iterator = this.spawners.entrySet().iterator();
        while (iterator.hasNext()) {
            n += iterator.next().getValue();
        }
        return n;
    }

    public int getTotalAmountOfSpawnersInContainers() {
        int n = 0;
        for (final Map.Entry<ItemWorth, Integer> entry : this.items.entrySet()) {
            if (entry.getKey().material != Material.MOB_SPAWNER) {
                continue;
            }
            n += entry.getValue();
        }
        return n;
    }

    public Faction getFaction() {
        for (final Faction faction : Factions.getInstance().getAllFactions()) {
            if (faction.getId().equalsIgnoreCase(this.faction_id)) {
                return faction;
            }
        }
        return null;
    }

    public String getPercentageChanged() {
        if (this.current_ftop_totalworth == 0.0 && this.old_ftop_totalworth == 0.0) {
            return "0";
        }
        String s = "";
        if (this.current_ftop_totalworth >= this.old_ftop_totalworth) {
            s = "+";
        }
        final double n = (this.current_ftop_totalworth - this.old_ftop_totalworth) / this.old_ftop_totalworth * 100.0;
        double round = 1000.0;
        try {
            round = this.factionstopplugin.getNumberFormattingUtil().round(n, 2);
        }
        catch (NumberFormatException ex) {}
        return String.valueOf(s) + round;
    }
}
