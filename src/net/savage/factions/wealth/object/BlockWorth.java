package net.savage.factions.wealth.object;

import org.bukkit.*;

public class BlockWorth
{
    public Material material;
    public short data;
    public double price;
    
    public BlockWorth(final Material material, final short data, final double price) {
        this.material = material;
        this.data = data;
        this.price = price;
    }
    
    public BlockWorth(final Material material, final int n, final double price) {
        this.material = material;
        this.data = (short)n;
        this.price = price;
    }
}
