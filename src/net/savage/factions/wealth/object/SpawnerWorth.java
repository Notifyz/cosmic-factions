package net.savage.factions.wealth.object;

import org.bukkit.*;

public class SpawnerWorth
{
    public Material material;
    public short data;
    public double price;
    public String spawnername;
    
    public SpawnerWorth(final Material material, final short data, final double price, final String spawnername) {
        this.material = material;
        this.data = data;
        this.price = price;
        this.spawnername = spawnername;
    }
    
    public SpawnerWorth(final Material material, final int n, final double price, final String spawnername) {
        this.material = material;
        this.data = (short)n;
        this.price = price;
        this.spawnername = spawnername;
    }
}
