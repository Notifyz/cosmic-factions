package net.savage.factions.wealth.runnable;

import org.bukkit.Bukkit;

import com.massivecraft.factions.P;

import net.savage.factions.wealth.task.ProvideBasicFactionInformation;

public class AutoRecalculate implements Runnable {
	@Override
	public void run() {
		Bukkit.getServer().getScheduler().runTaskAsynchronously(P.getInstance(),
				new ProvideBasicFactionInformation());
	}
}
