/*
 * Decompiled with CFR 0_132.
 *
 * Could not load the following classes:
 *  org.bukkit.plugin.Plugin
 *  org.bukkit.scheduler.BukkitRunnable
 *  org.bukkit.scheduler.BukkitTask
 */
package net.savage.factions.wealth.runnable;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.scheduler.BukkitRunnable;

import com.massivecraft.factions.P;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.FTop;

public class UpdateRankings
implements Runnable {
    private FactionsTopPlugin factionstopplugin = FactionsTopPlugin.getPluginInstance();

    public String formatProperly(long l, boolean bl) {
        String string;
        String string2 = string = NumberFormat.getNumberInstance(Locale.US).format(l);
        if (bl && string.contains(".")) {
            String[] arrstring = string.split(".");
            string2 = arrstring[0];
        }
        return string2;
    }

    @Override
    public void run() {
        if (this.factionstopplugin.unsorted.size() == 0) {
            return;
        }
        if (this.factionstopplugin.is_calculating) {
            return;
        }
        new BukkitRunnable(){

            @Override
			public void run() {
                int n = 0;
                Iterator<FTop> iterator = UpdateRankings.this.getFactions().iterator();
                while (iterator.hasNext()) {
                    FTop fTop = iterator.next();
                    UpdateRankings.this.factionstopplugin.rankings.put(++n, fTop.faction_id);
                    UpdateRankings.this.factionstopplugin.getFactionsRankingByID().put(fTop.faction_id, n);
                }
                if (!iterator.hasNext()) {
                    this.cancel();
                    return;
                }
            }
        }.runTaskAsynchronously(P.getInstance());
    }

    public ArrayList<FTop> getFactions() {
        ArrayList<FTop> arrayList = new ArrayList<>();
        HashMap<Object, Double> hashMap = new HashMap<>();
        for (Map.Entry<String, FTop> entry : this.factionstopplugin.unsorted.entrySet()) {
            FTop fTop = entry.getValue();
            hashMap.put(fTop, fTop.getTotalValue());
        }
        Map<Object, Double> hashMap2 = sortByValue(hashMap);
        for (Entry<Object, Double> entry : hashMap2.entrySet()) {
            FTop fTop = (FTop)entry.getKey();
            arrayList.add(fTop);
        }
        hashMap.clear();
        hashMap2.clear();
        return arrayList;
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
    	List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
    	Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
    		@Override
    		public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
    			return (e1.getValue()).compareTo(e2.getValue());
    		}
    	});

    	Map<K, V> result = new LinkedHashMap<>();
    	for (Map.Entry<K, V> entry : list) {
    		result.put(entry.getKey(), entry.getValue());
    	}

    	return result;
    }

}

