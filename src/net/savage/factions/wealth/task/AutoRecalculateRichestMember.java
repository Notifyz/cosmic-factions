package net.savage.factions.wealth.task;

import java.util.*;

import net.savage.factions.wealth.*;
import net.savage.factions.wealth.object.*;

public class AutoRecalculateRichestMember implements Runnable
{
    private FactionsTopPlugin factionstopplugin;
    
    public AutoRecalculateRichestMember() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }
    
    @Override
    public void run() {
        if (this.factionstopplugin.unsorted.size() == 0) {
            return;
        }
        final Iterator<Map.Entry<String, FTop>> iterator = this.factionstopplugin.unsorted.entrySet().iterator();
        while (iterator.hasNext()) {
            iterator.next().getValue().findRichestMember();
        }
    }
}
