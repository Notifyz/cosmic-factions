package net.savage.factions.wealth.task;

import java.util.ArrayList;
import java.util.Iterator;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.scheduler.BukkitRunnable;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.P;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.BlockWorth;
import net.savage.factions.wealth.object.FTop;
import net.savage.factions.wealth.object.SpawnerWorth;

public class CalculateSpawnerValue
{
    public static void calculateChunkRegularBlocks(final FLocation fLocation) {
        final World world = fLocation.getWorld();
        if (world == null) {
            return;
        }
        final FactionsTopPlugin pluginInstance = FactionsTopPlugin.getPluginInstance();
        ++pluginInstance.total_amount_of_chunks_calculated;
        final Chunk chunk = new Location(world, fLocation.getX() * 16L, 0.0, fLocation.getZ() * 16L).getChunk();
        new BukkitRunnable() {
            private final /* synthetic */ BlockState[] val$entities = chunk.getTileEntities();

            @Override
			public void run() {
                final Faction faction = Board.getInstance().getFactionAt(fLocation);
                if (!FactionsTopPlugin.getPluginInstance().unsorted.containsKey(faction.getId())) {
                    this.cancel();
                    return;
                }
                final FTop fTop = FactionsTopPlugin.getPluginInstance().unsorted.get(faction.getId());
                BlockState[] val$entities;
                for (int length = (val$entities = this.val$entities).length, i = 0; i < length; ++i) {
                    FactionsTopPlugin.getPluginInstance().getScannerv2().scanBlockStateInventory(fTop, val$entities[i]);
                }
                for (int k = 0; k < 16; ++k) {
                    for (int l = 0; l < 16; ++l) {
                        for (int n = 0; n < 255; ++n) {
                            final Block block = chunk.getBlock(k, n, l);
                            if (block.getType() != Material.AIR) {
                                if (block.getType() != Material.BEACON && block.getType() != Material.CHEST && block.getType() != Material.DISPENSER && block.getType() != Material.DROPPER && block.getType() != Material.HOPPER) {
                                    if (block.getType() != Material.TRAPPED_CHEST) {
                                        if (block.getType() == Material.MOB_SPAWNER) {
                                            final CreatureSpawner creatureSpawner = (CreatureSpawner)block.getState();
                                            final short typeId = creatureSpawner.getSpawnedType().getTypeId();
                                            if (FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(typeId) != null) {
                                                final SpawnerWorth spawnerWorth4 = FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(typeId);
                                                final int n2 = 1;
                                                final FTop fTop5 = fTop;
                                                fTop5.spawner_balance += n2 * spawnerWorth4.price;
                                                if (fTop.spawners.containsKey(spawnerWorth4)) {
                                                    fTop.spawners.put(spawnerWorth4, fTop.spawners.get(spawnerWorth4) + n2);
                                                }
                                                else {
                                                    fTop.spawners.put(spawnerWorth4, n2);
                                                }
                                            }
                                        }
                                        else {
                                            final BlockWorth blockWorth = FactionsTopPlugin.getPluginInstance().getUtil().getBlockWorth(block.getType(), block.getData());
                                            if (blockWorth != null) {
                                                final FTop fTop6 = fTop;
                                                fTop6.block_balance += blockWorth.price;
                                                if (fTop.blocks.containsKey(blockWorth)) {
                                                    fTop.blocks.put(blockWorth, fTop.blocks.get(blockWorth) + 1);
                                                }
                                                else {
                                                    fTop.blocks.put(blockWorth, 1);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                this.cancel();
            }
        }.runTaskAsynchronously(P.getInstance());
    }

    public static void runTask() {
        if (FactionsTopPlugin.getPluginInstance().tocalculate.size() == 0) {
            FactionsTopPlugin.getPluginInstance().is_calculating = false;
            return;
        }
        final int chunks_calculated_per_second = 40;
        FactionsTopPlugin.getPluginInstance().calculation_start_ms = System.currentTimeMillis();
        new BukkitRunnable() {
            @Override
			public void run() {
                final ArrayList<FLocation> list = new ArrayList<>();
                for (int i = 0; i < chunks_calculated_per_second; ++i) {
                    if (FactionsTopPlugin.getPluginInstance().tocalculate.size() <= i) {
                        FactionsTopPlugin.getPluginInstance().calculation_stop_ms = System.currentTimeMillis();
                        FactionsTopPlugin.getPluginInstance().total_amount_of_chunks_calculated = 0;
                        FactionsTopPlugin.getPluginInstance().total_amount_of_chunks_to_calculate = 0;
                        FactionsTopPlugin.getPluginInstance().is_calculating = false;
                        this.cancel();
                        final Iterator<FLocation> iterator2 = list.iterator();
                        while (iterator2.hasNext()) {
                            FactionsTopPlugin.getPluginInstance().tocalculate.remove(iterator2.next());
                        }
                        list.clear();
                        return;
                    }
                    final FLocation fLocation = FactionsTopPlugin.getPluginInstance().tocalculate.get(i);
                    CalculateSpawnerValue.calculateChunkRegularBlocks(fLocation);
                    list.add(fLocation);
                }
                final Iterator<FLocation> iterator3 = list.iterator();
                while (iterator3.hasNext()) {
                    FactionsTopPlugin.getPluginInstance().tocalculate.remove(iterator3.next());
                }
                list.clear();
            }
        }.runTaskTimer(P.getInstance(), 20L, 20L);
    }
}
