package net.savage.factions.wealth.task;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.FTop;

public class ProvideBasicFactionInformation implements Runnable
{
    private FactionsTopPlugin factionstopplugin;

    public ProvideBasicFactionInformation() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public String fixColour(final String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    @Override
    public void run() {
        this.factionstopplugin.total_amount_of_chunks_to_calculate = 0;
        for (final Faction faction : Factions.getInstance().getAllFactions()) {
            final String id = faction.getId();
            if (Factions.getInstance().getFactionById(id) == null) {
                System.out.println("Skipped a invalid faction with [FactionId=" + id + "] [FactionName=" + faction.getTag() + "]");
            }
            else {
                final String stripColor = ChatColor.stripColor(faction.getTag());
                if (this.factionstopplugin.getUtil().isSystemFaction(stripColor)) {
                    continue;
                }
                if (this.factionstopplugin.getUtil().isSystemFactionById(id)) {
                    continue;
                }
                if (faction.getFPlayerAdmin() == null) {
                    System.out.println("Skipped a invalid faction with [FactionId=" + id + "] [FactionName=" + faction.getTag() + "] Reason = Invalid Faction Owner");
                }
                else {
                    final String name = faction.getFPlayerAdmin().getName();
                    if (!Bukkit.getServer().getOfflinePlayer(name).hasPlayedBefore()) {
                        System.out.println("Skipped a invalid faction with [FactionId=" + id + "] [FactionName=" + faction.getTag() + "] Reason = Faction Owner never joined the server");
                    }
                    else if (!this.factionstopplugin.unsorted.containsKey(id)) {
                        final FTop fTop = new FTop(id);
                        fTop.resetValues();
                        fTop.setLeader(name);
                        fTop.setFactionName(stripColor);
                        this.factionstopplugin.unsorted.put(id, fTop);
                    }
                    else {
                        final FTop fTop2 = this.factionstopplugin.unsorted.get(id);
                        fTop2.setLeader(name);
                        fTop2.setFactionName(stripColor);
                        fTop2.old_ftop_totalworth = fTop2.current_ftop_totalworth;
                        fTop2.current_ftop_totalworth = 0.0;
                        fTop2.resetValues();
                    }
                }
            }
        }
        ProvideCorrectChunksToCalculate.runTask();
    }
}
