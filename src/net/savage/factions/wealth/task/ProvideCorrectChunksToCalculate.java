package net.savage.factions.wealth.task;

import java.util.Iterator;

import org.bukkit.ChatColor;

import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

import net.savage.factions.wealth.FactionsTopPlugin;

public class ProvideCorrectChunksToCalculate
{
    public static void runTask() {
        int n = 0;
        for (final Faction faction : Factions.getInstance().getAllFactions()) {
            if (!FactionsTopPlugin.getPluginInstance().getUtil().isSystemFactionById(faction.getId())) {
                if (FactionsTopPlugin.getPluginInstance().getUtil().isSystemFaction(ChatColor.stripColor(faction.getTag()))) {
                    continue;
                }
                if (faction.getAllClaims().size() == 0) {
                    continue;
                }
                n += faction.getAllClaims().size();
                final FactionsTopPlugin pluginInstance = FactionsTopPlugin.getPluginInstance();
                pluginInstance.total_amount_of_chunks_to_calculate += faction.getAllClaims().size();
                final Iterator<FLocation> iterator2 = faction.getAllClaims().iterator();
                while (iterator2.hasNext()) {
                    FactionsTopPlugin.getPluginInstance().tocalculate.add(iterator2.next());
                }
            }
        }
        if (n == 0) {
            return;
        }
        FactionsTopPlugin.getPluginInstance().is_calculating = true;
        CalculateSpawnerValue.runTask();
    }
}
