package net.savage.factions.wealth.task;

import java.text.NumberFormat;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

import org.bukkit.scheduler.BukkitRunnable;

import com.massivecraft.factions.P;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.FTop;

public class AutoCalculateServerFtopTotal implements Runnable
{
    private FactionsTopPlugin factionstopplugin;

    public AutoCalculateServerFtopTotal() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public String formatProperly(final double n, final boolean b) {
        String format;
        final String s = format = NumberFormat.getNumberInstance(Locale.US).format(n);
        if (b && s.contains(".")) {
            format = s.split(".")[0];
        }
        return format;
    }

    public String formatProperlyTroubleShoot(final double n, final boolean b) {
        String format = "";
        try {
            final String s = format = NumberFormat.getNumberInstance(Locale.US).format(n);
            if (b && s.contains(".")) {
                format = s.split(".")[0];
            }
        }
        catch (Exception ex) {
        }
        return format;
    }

    @Override
    public void run() {
        this.factionstopplugin.server_total = 0.0;
        new BukkitRunnable() {
            @Override
			public void run() {
                double server_total = 0.0;
                final Iterator<Map.Entry<String, FTop>> iterator = AutoCalculateServerFtopTotal.this.factionstopplugin.unsorted.entrySet().iterator();
                while (iterator.hasNext()) {
                    final FTop fTop = iterator.next().getValue();
                    fTop.current_ftop_totalworth = fTop.getTotalValue();
                    server_total += fTop.getTotalValue();
                }
                AutoCalculateServerFtopTotal.this.factionstopplugin.server_total = server_total;
                this.cancel();
            }
        }.runTaskAsynchronously(P.getInstance());
    }
}
