package net.savage.factions.wealth.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;

import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.P;
import com.massivecraft.factions.event.FactionCreateEvent;
import com.massivecraft.factions.event.FactionDisbandEvent;
import com.massivecraft.factions.event.FactionRenameEvent;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.FTop;

public class FactionEventsListener implements Listener
{
    private FactionsTopPlugin factionstopplugin;

    public FactionEventsListener() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFactionCreate(final FactionCreateEvent factionCreateEvent) {
        new BukkitRunnable() {
            private final /* synthetic */ String val$factionname = factionCreateEvent.getFactionTag();
            private final /* synthetic */ String val$leader = factionCreateEvent.getFPlayer().getName();

            @Override
			public void run() {
                final Faction byTag = Factions.getInstance().getByTag(this.val$factionname);
                if (byTag == null) {
                    return;
                }
                final String id = byTag.getId();
                final FTop fTop = new FTop(id);
                fTop.setFactionName(this.val$factionname);
                fTop.setLeader(this.val$leader);
                FactionEventsListener.this.factionstopplugin.unsorted.put(id, fTop);
                this.cancel();
            }
        }.runTaskLater(P.getInstance(), 40L);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFactionDisband(final FactionDisbandEvent factionDisbandEvent) {
        final Faction faction = factionDisbandEvent.getFaction();
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        if (this.factionstopplugin.is_calculating) {
            for (final FLocation fLocation : faction.getAllClaims()) {
                if (this.factionstopplugin.tocalculate.contains(fLocation)) {
                    this.factionstopplugin.tocalculate.remove(fLocation);
                }
            }
        }
        this.factionstopplugin.unsorted.remove(id);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onRenameFaction(final FactionRenameEvent factionRenameEvent) {
        final String id = factionRenameEvent.getFaction().getId();
        if (this.factionstopplugin.unsorted.containsKey(id)) {
            this.factionstopplugin.unsorted.get(id).setFactionName(factionRenameEvent.getFactionTag());
        }
    }
}
