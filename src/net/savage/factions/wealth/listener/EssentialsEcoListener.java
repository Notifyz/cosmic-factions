package net.savage.factions.wealth.listener;

import net.ess3.api.events.*;
import net.savage.factions.wealth.*;
import net.savage.factions.wealth.object.*;

import com.massivecraft.factions.*;
import org.bukkit.event.*;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.event.player.*;

public class EssentialsEcoListener implements Listener
{
    private FactionsTopPlugin factionstopplugin;
    
    public EssentialsEcoListener() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }
    
    @EventHandler
    public void onEcoUpdate(final UserBalanceUpdateEvent userBalanceUpdateEvent) {
        final FPlayer byPlayer = FPlayers.getInstance().getByPlayer(userBalanceUpdateEvent.getPlayer());
        if (!byPlayer.hasFaction()) {
            return;
        }
        if (!byPlayer.isOnline()) {
            return;
        }
        final String id = byPlayer.getFaction().getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        final FTop fTop = this.factionstopplugin.unsorted.get(id);
        if (userBalanceUpdateEvent.getNewBalance().doubleValue() >= userBalanceUpdateEvent.getOldBalance().doubleValue()) {
            fTop.addMoneyToBalance(userBalanceUpdateEvent.getNewBalance().doubleValue() - userBalanceUpdateEvent.getOldBalance().doubleValue());
            return;
        }
        fTop.removeFromFromBalance(userBalanceUpdateEvent.getOldBalance().doubleValue() - userBalanceUpdateEvent.getNewBalance().doubleValue());
    }
    
    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent playerJoinEvent) {
        final Player player = playerJoinEvent.getPlayer();
        final FPlayer byPlayer = FPlayers.getInstance().getByPlayer(player);
        if (!byPlayer.hasFaction()) {
            return;
        }
        final String id = byPlayer.getFaction().getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        this.factionstopplugin.unsorted.get(id).addMoneyToBalance(this.factionstopplugin.economy.getBalance((OfflinePlayer)player));
    }
    
    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent playerQuitEvent) {
        final Player player = playerQuitEvent.getPlayer();
        final FPlayer byPlayer = FPlayers.getInstance().getByPlayer(player);
        if (!byPlayer.hasFaction()) {
            return;
        }
        final String id = byPlayer.getFaction().getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        this.factionstopplugin.unsorted.get(id).removeFromFromBalance(this.factionstopplugin.economy.getBalance((OfflinePlayer)player));
    }
    
    @EventHandler
    public void onPlayerKick(final PlayerKickEvent playerKickEvent) {
        final Player player = playerKickEvent.getPlayer();
        final FPlayer byPlayer = FPlayers.getInstance().getByPlayer(player);
        if (!byPlayer.hasFaction()) {
            return;
        }
        final String id = byPlayer.getFaction().getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        this.factionstopplugin.unsorted.get(id).removeFromFromBalance(this.factionstopplugin.economy.getBalance((OfflinePlayer)player));
    }
}
