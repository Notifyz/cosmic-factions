package net.savage.factions.wealth.listener;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.FTop;

public class PListener implements Listener
{
    private FactionsTopPlugin factionstopplugin;
    public static String uid;

    static {
        PListener.uid = "229606";
    }

    public PListener() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public boolean isftopcommand(final String s) {
        return s.startsWith("/f top") || s.startsWith("/factions top");
    }

    public boolean isfvaluecommand(final String s) {
        return s.startsWith("/f value") || s.startsWith("/factions value");
    }

    public boolean isStackedSpawner(final Block block) {
        return false;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onBlockPlace(final BlockPlaceEvent blockPlaceEvent) {
        if (blockPlaceEvent.isCancelled()) {
            return;
        }
        final Block block = blockPlaceEvent.getBlock();
        if (this.isStackedSpawner(block)) {
            return;
        }
        final FLocation fLocation = new FLocation(block.getLocation());
        final Faction faction = Board.getInstance().getFactionAt(fLocation);
        if (faction.isWilderness() || faction.isSafeZone() || faction.isWarZone()) {
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        if (this.factionstopplugin.is_calculating) {
            final FTop fTop = this.factionstopplugin.unsorted.get(id);
            if (!this.factionstopplugin.tocalculate.contains(fLocation)) {
                this.factionstopplugin.getUtil().addBlock(fTop, block);
            }
            return;
        }
        this.factionstopplugin.getUtil().addBlock(this.factionstopplugin.unsorted.get(id), block);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onBlockBreak(final BlockBreakEvent blockBreakEvent) {
        if (blockBreakEvent.isCancelled()) {
            return;
        }
        final Block block = blockBreakEvent.getBlock();
        if (this.isStackedSpawner(block)) {
            return;
        }
        final FLocation fLocation = new FLocation(block.getLocation());
        final Faction faction = Board.getInstance().getFactionAt(fLocation);
        if (faction.isWilderness() || faction.isSafeZone() || faction.isWarZone()) {
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        if (this.factionstopplugin.is_calculating) {
            final FTop fTop = this.factionstopplugin.unsorted.get(id);
            if (!this.factionstopplugin.tocalculate.contains(fLocation)) {
                this.factionstopplugin.getUtil().removeBlock(fTop, block);
            }
            return;
        }
        this.factionstopplugin.getUtil().removeBlock(this.factionstopplugin.unsorted.get(id), block);
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onCommand(final PlayerCommandPreprocessEvent playerCommandPreprocessEvent) {
        final Player player = playerCommandPreprocessEvent.getPlayer();
        final String lowerCase = playerCommandPreprocessEvent.getMessage().toLowerCase();
        if (!lowerCase.startsWith("/f ") && !lowerCase.startsWith("/factions ")) {
            return;
        }
        final String[] split = lowerCase.split(" ");
        if (split.length == 0) {
            return;
        }
        final String s = split[1];
        if (s.equalsIgnoreCase("top") || s.equalsIgnoreCase("wealth")) {
            playerCommandPreprocessEvent.setCancelled(true);
            int int1 = 1;
            if (split.length >= 2) {
                try {
                    int1 = Integer.parseInt(split[2]);
                }
                catch (Exception ex) {
                    int1 = 1;
                }
            }
            this.factionstopplugin.getUserInterface().send(player, int1);
            return;
        }
        if (!s.equalsIgnoreCase("value")) {
            return;
        }
        playerCommandPreprocessEvent.setCancelled(true);
        if (split.length >= 3) {
            Bukkit.dispatchCommand(player, "fvalue " + split[2]);
            return;
        }
        Bukkit.dispatchCommand(player, "fvalue");
    }
}
