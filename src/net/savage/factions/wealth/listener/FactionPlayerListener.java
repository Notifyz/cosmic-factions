package net.savage.factions.wealth.listener;

import org.bukkit.*;
import org.bukkit.entity.*;
import com.massivecraft.factions.*;
import org.bukkit.event.*;
import com.massivecraft.factions.event.*;

import net.savage.factions.wealth.*;
import net.savage.factions.wealth.object.*;

public class FactionPlayerListener implements Listener
{
    private FactionsTopPlugin factionstopplugin;
    
    public FactionPlayerListener() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }
    
    @EventHandler
    public void onPlayerJoinFaction(final FPlayerJoinEvent fPlayerJoinEvent) {
        if (fPlayerJoinEvent.isCancelled()) {
            return;
        }
        final FPlayer getfPlayer = fPlayerJoinEvent.getfPlayer();
        if (!getfPlayer.isOnline()) {
            return;
        }
        final String name = getfPlayer.getName();
        final Player player = Bukkit.getServer().getPlayer(name);
        if (player == null) {
            return;
        }
        final Faction faction = fPlayerJoinEvent.getFaction();
        if (faction == null) {
            System.out.println("Error: faction is null");
            System.out.println("This is a bug in FactionsUUID");
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        final FTop fTop = this.factionstopplugin.unsorted.get(id);
        fTop.setRichestMember(name, this.factionstopplugin.economy.getBalance((OfflinePlayer)player));
        fTop.addMoneyToBalance(this.factionstopplugin.economy.getBalance((OfflinePlayer)player));
    }
    
    @EventHandler
    public void onPlayerLeaveFaction(final FPlayerLeaveEvent fPlayerLeaveEvent) {
        if (fPlayerLeaveEvent.isCancelled()) {
            return;
        }
        final FPlayer getfPlayer = fPlayerLeaveEvent.getfPlayer();
        if (!getfPlayer.isOnline()) {
            return;
        }
        final Player player = Bukkit.getServer().getPlayer(getfPlayer.getName());
        if (player == null) {
            return;
        }
        final Faction faction = fPlayerLeaveEvent.getFaction();
        if (faction == null) {
            System.out.println("Error: faction is null");
            System.out.println("This is a bug in FactionsUUID!");
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        this.factionstopplugin.unsorted.get(id).removeFromFromBalance(this.factionstopplugin.economy.getBalance((OfflinePlayer)player));
    }
}
