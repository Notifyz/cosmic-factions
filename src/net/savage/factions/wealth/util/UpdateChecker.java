package net.savage.factions.wealth.util;

import java.util.*;
import java.net.*;
import java.io.*;

public class UpdateChecker
{
    public UpdateStatus getVersionStatus(final String s, final String s2) {
        final ArrayList<String> list = new ArrayList<String>(Arrays.asList(s2.split("\\.")));
        final ArrayList<Object> list2 = new ArrayList<Object>(Arrays.asList(s.split("\\.")));
        int n = 0;
        for (int n2 = 0; n2 < list.size() && n2 < list2.size(); ++n2) {
            if (list.get(n2).equals(list2.get(n2))) {
                ++n;
            }
        }
        if (n == list.size() && n == list2.size()) {
            return UpdateStatus.UP_TO_DATE;
        }
        if (n > list.size() || n < list.size()) {
            return UpdateStatus.UNKNOWN;
        }
        return UpdateStatus.OUT_OF_DATE;
    }
    
    public String getVersion() {
        try {
            final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new URL("http://customenchantsdevelopment.ovh/factionstopversion.txt").openStream()));
            final String line;
            if ((line = bufferedReader.readLine()) != null) {
                return line;
            }
            bufferedReader.close();
        }
        catch (Exception ex) {
            return "Null";
        }
        return "Null";
    }
    
    public enum UpdateStatus
    {
        UP_TO_DATE("UP_TO_DATE", 0, "Great! FactionsTop is up-to-date!"), 
        OUT_OF_DATE("OUT_OF_DATE", 1, "Oh no! FactionsTop is out-of-date!"), 
        UNKNOWN("UNKNOWN", 2, "You, my friend are a time-traveller; please spare my life when you kill the planet!");
        
        private String message;
        
        private UpdateStatus(final String s, final int n, final String message) {
            this.message = message;
        }
        
        public String getMessage() {
            return this.message;
        }
    }
}
