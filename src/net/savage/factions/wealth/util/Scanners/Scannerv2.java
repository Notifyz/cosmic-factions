package net.savage.factions.wealth.util.Scanners;

import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.block.Dispenser;
import org.bukkit.block.DoubleChest;
import org.bukkit.block.Dropper;
import org.bukkit.block.Hopper;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BlockStateMeta;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.BlockWorth;
import net.savage.factions.wealth.object.FTop;
import net.savage.factions.wealth.object.ItemWorth;

public class Scannerv2
{
    private FactionsTopPlugin factionstopplugin;

    public Scannerv2() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public void scanBlockStateInventory(final FTop fTop, final BlockState blockState) {
        Inventory inventory = null;
        int n = 1;
        if (blockState instanceof Chest) {
            inventory = ((Chest)blockState).getBlockInventory();
        }
        if (blockState instanceof DoubleChest) {
            inventory = ((DoubleChest)blockState).getInventory();
            n = 2;
        }
        if (blockState instanceof Dropper) {
            inventory = ((Dropper)blockState).getInventory();
        }
        if (blockState instanceof Dispenser) {
            inventory = ((Dispenser)blockState).getInventory();
        }
        if (blockState instanceof Hopper) {
            inventory = ((Hopper)blockState).getInventory();
        }
        if (inventory == null) {
            return;
        }
        final BlockWorth blockWorth = this.factionstopplugin.getUtil().getBlockWorth(blockState.getType(), (short)0);
        if (blockWorth != null) {
            fTop.block_balance += n * blockWorth.price;
            if (fTop.blocks.containsKey(blockWorth)) {
                fTop.blocks.put(blockWorth, fTop.blocks.get(blockWorth) + n);
            }
            else {
                fTop.blocks.put(blockWorth, n);
            }
        }
        this.scanInventoryv2(inventory, fTop);
    }

    public void scanInventoryv2(final Inventory inventory, final FTop fTop) {
        ItemStack[] contents;
        for (int length = (contents = inventory.getContents()).length, i = 0; i < length; ++i) {
            final ItemStack itemStack = contents[i];
            if (itemStack != null) {
                final Material type = itemStack.getType();
                short n = itemStack.getData().getData();
                final int amount = itemStack.getAmount();
                if (this.factionstopplugin.above1_7 && itemStack.hasItemMeta() && itemStack.getItemMeta() instanceof BlockStateMeta) {
                    final BlockStateMeta blockStateMeta = (BlockStateMeta)itemStack.getItemMeta();
                    if (blockStateMeta.hasBlockState() && blockStateMeta.getBlockState() instanceof CreatureSpawner) {
                        n = ((CreatureSpawner)blockStateMeta.getBlockState()).getSpawnedType().getTypeId();
                    }
                }
                boolean b = false;
                for (final ItemWorth itemWorth : this.factionstopplugin.getItemWorth()) {
                    if (itemWorth.material == type && itemWorth.data == n) {
                        fTop.item_worth += amount * itemWorth.price;
                        if (fTop.items.containsKey(itemWorth)) {
                            fTop.items.put(itemWorth, fTop.items.get(itemWorth) + amount);
                        }
                        else {
                            fTop.items.put(itemWorth, amount);
                        }
                        b = true;
                        break;
                    }
                }
                if (!b) {}
            }
        }
    }
}
