package net.savage.factions.wealth.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.FTop;

public class SortingUtil {

	public static final Comparator<FTop> VALUE_COMPARATOR = (faction1, faction2) -> {
		return Double.compare(faction1.getTotalValue(), faction2.getTotalValue());
	};

    private FactionsTopPlugin factionstopplugin = FactionsTopPlugin.getPluginInstance();

    public List<FTop> getFactions() {
    	List<FTop> factions = new ArrayList<>(factionstopplugin.unsorted.values().stream().collect(Collectors.toList()));
    	Collections.sort(factions, VALUE_COMPARATOR);
    	Collections.reverse(factions);

    	return factions;
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
    	List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
    	Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
    		@Override
    		public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
    			return (e1.getValue()).compareTo(e2.getValue());
    		}
    	});

    	Map<K, V> result = new LinkedHashMap<>();
    	for (Map.Entry<K, V> entry : list) {
    		result.put(entry.getKey(), entry.getValue());
    	}

    	return result;
    }

}

