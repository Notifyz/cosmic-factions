package net.savage.factions.wealth.util;

import java.math.*;
import java.util.*;
import java.text.*;

public class NumberFormattingUtil
{
    public double round(final double n, final int n2) {
        if (n2 < 0) {
            throw new IllegalArgumentException();
        }
        return new BigDecimal(n).setScale(n2, RoundingMode.HALF_UP).doubleValue();
    }
    
    public String formatProperly(final long n, final boolean b) {
        final String format = NumberFormat.getNumberInstance(Locale.US).format(n);
        if (b && format.contains(".")) {
            final String[] split = format.split(".");
            if (split.length >= 1) {
                return split[0];
            }
        }
        return format;
    }
    
    public String formatProperly(final double n, final boolean b) {
        final String format = NumberFormat.getNumberInstance(Locale.US).format(n);
        if (b && format.contains(".")) {
            final String[] split = format.split(".");
            if (split.length >= 1) {
                return split[0];
            }
        }
        return format;
    }
    
    public String formatProperlyTroubleShoot(final double n, final boolean b) {
        String format;
        try {
            format = NumberFormat.getNumberInstance(Locale.US).format(n);
            if (b && format.contains(".")) {
                final String[] split = format.split(".");
                if (split.length >= 1) {
                    return split[0];
                }
            }
        }
        catch (Exception ex) {
            return new StringBuilder().append(n).toString();
        }
        return format;
    }
}
