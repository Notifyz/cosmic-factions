package net.savage.factions.wealth.util;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.massivecraft.factions.P;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.FTop;

public class UserInterface
{
    private FactionsTopPlugin factionstopplugin;

    public UserInterface() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public String fixColour(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public void sendDatav2(Player player, int currentPage, int startIndex, int factionsPerPage, int maximumPages) {
    	String formatProperly = "0";
        if (this.factionstopplugin.server_total >= 1.0) {
            formatProperly = this.factionstopplugin.getNumberFormattingUtil().formatProperly(this.factionstopplugin.server_total, true);
        }
        player.sendMessage("");
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b&lTop Factions (&d&l" + currentPage + "&b&l/&d&l" + maximumPages + "&b&l)"));
        for (int rank = startIndex; rank < factionsPerPage/*&& factionstopplugin.getSortingUtil().getFactions().size() > rank*/; rank++) {
        	FTop foundFaction = factionstopplugin.getSortingUtil().getFactions().get(rank);
        	formatProperly = this.factionstopplugin.getNumberFormattingUtil().formatProperly(foundFaction.getTotalValue(), false);
            player.sendMessage(new ComponentBuilder(ChatColor.translateAlternateColorCodes('&', "&b&l" + (rank + 1) + ". " + "&f" + foundFaction.faction_name + " &b- " + "&d&l$" + formatProperly))
            		.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(ChatColor.translateAlternateColorCodes('&',
            				"&f" + foundFaction.faction_name + "\n" +
            				"&dLeader: &b" + foundFaction.leader + "\n" +
            				" " + "\n" +
            				"&dFaction Wealth: &b$" + formatProperly + "\n" +
            				"&dMember Wealth: &b$" + factionstopplugin.getNumberFormattingUtil().formatProperly(foundFaction.getMoneyWorth(), false) + "\n" +
            				"&dSpawner Value: &b$" + factionstopplugin.getNumberFormattingUtil().formatProperly(foundFaction.spawner_balance, false) + "\n" +
            				"&dFaction Upgrades: &b$0" + "\n" +
            				"&dRichest Member:" + "\n" +
            				" &b" + foundFaction.getRichestMember()[0] + ": $" + foundFaction.getRichestMember()[1]))
            				.create()))
            		.event(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/f who " + foundFaction.faction_name))
            		.create());
        }
        player.sendMessage("");
    }

    public void send(final Player player, final int n) {
        new BukkitRunnable() {
            @Override
			public void run() {
                final int factions_per_page = 10;
                final int max = Math.max((int)Math.ceil(UserInterface.this.factionstopplugin.unsorted.size() / factions_per_page), 1);
                int val$page = n;
                if (n > max) {
                    val$page = max;
                }
                if (val$page == 1) {
                    UserInterface.this.sendDatav2(player, val$page, factions_per_page - factions_per_page, factions_per_page, max);
                    this.cancel();
                    return;
                }
                if (val$page >= 2) {
                    final int n = val$page * factions_per_page - factions_per_page;
                    UserInterface.this.sendDatav2(player, val$page, n, n + factions_per_page, max);
                    this.cancel();
                    return;
                }
                this.cancel();
            }
        }.runTaskLaterAsynchronously(P.getInstance(), 5);
    }
}
