package net.savage.factions.wealth.util.Files;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.ItemWorth;

public class ItemsUtilv2
{
    private FactionsTopPlugin factionstopplugin;
    public File itemsv2;

    public ItemsUtilv2() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public void saveItemWorth(final ItemWorth itemWorth) {
        final YamlConfiguration loadConfiguration = YamlConfiguration.loadConfiguration(this.itemsv2);
        final int n = ((FileConfiguration)loadConfiguration).getKeys(false).size() + 1;
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".material", itemWorth.material.name());
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".data", itemWorth.data);
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".price", itemWorth.price);
        try {
            ((FileConfiguration)loadConfiguration).save(this.itemsv2);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void loadItems() {
        final YamlConfiguration loadConfiguration = YamlConfiguration.loadConfiguration(this.itemsv2);
        final Iterator<String> iterator = ((FileConfiguration)loadConfiguration).getKeys(false).iterator();
        while (iterator.hasNext()) {
            final ConfigurationSection configurationSection = ((FileConfiguration)loadConfiguration).getConfigurationSection(iterator.next());
            this.factionstopplugin.getItemWorth().add(new ItemWorth(Material.getMaterial(configurationSection.getString("material")), (short)configurationSection.getInt("data"), configurationSection.getDouble("price")));
        }
    }

    public void setup(final File file) {
        if (!file.exists()) {
            file.mkdirs();
        }
        this.itemsv2 = new File(file + File.separator + "item-wealth.yml");
        if (!this.itemsv2.exists()) {
            try {
                this.itemsv2.createNewFile();
                this.saveItemWorth(new ItemWorth(Material.BEDROCK, 0, 200.0));
                this.saveItemWorth(new ItemWorth(Material.OBSIDIAN, 0, 10.0));
                this.saveItemWorth(new ItemWorth(Material.TNT, 0, 20.0));
                this.saveItemWorth(new ItemWorth(Material.GOLDEN_APPLE, 0, 50.0));
                this.saveItemWorth(new ItemWorth(Material.GOLDEN_APPLE, 1, 5000.0));
                this.saveItemWorth(new ItemWorth(Material.DIAMOND_BLOCK, 0, 1700.0));
                this.saveItemWorth(new ItemWorth(Material.EMERALD_BLOCK, 0, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.GOLD_BLOCK, 0, 500.0));
                this.saveItemWorth(new ItemWorth(Material.IRON_BLOCK, 0, 1200.0));
                this.saveItemWorth(new ItemWorth(Material.COAL_BLOCK, 0, 300.0));
                this.saveItemWorth(new ItemWorth(Material.REDSTONE_BLOCK, 0, 450.0));
                this.saveItemWorth(new ItemWorth(Material.DIAMOND_HELMET, 0, 250.0));
                this.saveItemWorth(new ItemWorth(Material.DIAMOND_CHESTPLATE, 0, 350.0));
                this.saveItemWorth(new ItemWorth(Material.DIAMOND_LEGGINGS, 0, 350.0));
                this.saveItemWorth(new ItemWorth(Material.DIAMOND_BOOTS, 0, 250.0));
                this.saveItemWorth(new ItemWorth(Material.DRAGON_EGG, 0, 1.5E7));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 4, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 6, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 23, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 31, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 32, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 34, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 35, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 36, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 37, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 50, 500000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 51, 70000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 52, 45000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 53, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 54, 70000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 55, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 56, 20000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 57, 750000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 58, 200000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 59, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 60, 1.5E7));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 61, 350000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 62, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 64, 500000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 65, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 66, 15000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 67, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 68, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 90, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 91, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 92, 50000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 93, 5000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 94, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 95, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 96, 100000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 97, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 98, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 99, 1500000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 100, 100000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 101, 10000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 102, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 103, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 105, 2000.0));
                this.saveItemWorth(new ItemWorth(Material.MOB_SPAWNER, 120, 120000.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 4, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 6, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 23, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 31, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 32, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 34, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 35, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 36, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 37, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 50, 2500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 51, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 52, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 53, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 54, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 55, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 56, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 57, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 58, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 59, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 60, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 61, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 62, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 64, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 65, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 66, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 67, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 68, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 90, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 91, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 92, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 93, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 94, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 95, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 96, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 97, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 98, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 99, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 100, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 101, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 102, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 103, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 105, 500.0));
                this.saveItemWorth(new ItemWorth(Material.MONSTER_EGG, 120, 500.0));
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        this.loadItems();
    }
}
