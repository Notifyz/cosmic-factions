package net.savage.factions.wealth.util.Files;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.BlockWorth;

public class FileUtil
{
    private FactionsTopPlugin factionstopplugin;
    public File blocksv2;

    public FileUtil() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public String fixColour(final String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public void saveBlockWorth(final BlockWorth blockWorth) {
        final YamlConfiguration loadConfiguration = YamlConfiguration.loadConfiguration(this.blocksv2);
        final int n = ((FileConfiguration)loadConfiguration).getKeys(false).size() + 1;
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".material", blockWorth.material.name());
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".data", blockWorth.data);
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".price", blockWorth.price);
        try {
            ((FileConfiguration)loadConfiguration).save(this.blocksv2);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void loadBlocks() {
        final YamlConfiguration loadConfiguration = YamlConfiguration.loadConfiguration(this.blocksv2);
        final Iterator<String> iterator = ((FileConfiguration)loadConfiguration).getKeys(false).iterator();
        while (iterator.hasNext()) {
            final ConfigurationSection configurationSection = ((FileConfiguration)loadConfiguration).getConfigurationSection(iterator.next());
            Material material;
            try {
                material = Material.getMaterial(configurationSection.getString("material"));
            }
            catch (Exception ex) {
                continue;
            }
            this.factionstopplugin.getBlockWorth().add(new BlockWorth(material, (short)configurationSection.getInt("data"), configurationSection.getDouble("price")));
        }
    }

    public void setup(final File file) {
        if (!file.exists()) {
            file.mkdirs();
        }
        this.blocksv2 = new File(file + File.separator + "block-wealth.yml");
        if (!this.blocksv2.exists()) {
            try {
                this.blocksv2.createNewFile();
                this.saveBlockWorth(new BlockWorth(Material.BEACON, 0, 25000.0));
                this.saveBlockWorth(new BlockWorth(Material.CHEST, 0, 20.0));
                this.saveBlockWorth(new BlockWorth(Material.TRAPPED_CHEST, 0, 20.0));
                this.saveBlockWorth(new BlockWorth(Material.HOPPER, 0, 5000.0));
                this.saveBlockWorth(new BlockWorth(Material.DISPENSER, 0, 1000.0));
                this.saveBlockWorth(new BlockWorth(Material.DROPPER, 0, 1000.0));
                this.saveBlockWorth(new BlockWorth(Material.DIAMOND_BLOCK, 0, 1700.0));
                this.saveBlockWorth(new BlockWorth(Material.EMERALD_BLOCK, 0, 2000.0));
                this.saveBlockWorth(new BlockWorth(Material.GOLD_BLOCK, 0, 500.0));
                this.saveBlockWorth(new BlockWorth(Material.IRON_BLOCK, 0, 1200.0));
                this.saveBlockWorth(new BlockWorth(Material.COAL_BLOCK, 0, 300.0));
                this.saveBlockWorth(new BlockWorth(Material.REDSTONE_BLOCK, 0, 450.0));
                this.saveBlockWorth(new BlockWorth(Material.DRAGON_EGG, 0, 1.5E7));
            }
            catch (IOException ex3) {
                ex3.printStackTrace();
            }
        }
        this.loadBlocks();
    }
}
