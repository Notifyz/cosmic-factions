package net.savage.factions.wealth.util.Files;

import org.bukkit.*;
import org.bukkit.block.*;

import com.massivecraft.factions.*;

import net.savage.factions.wealth.*;
import net.savage.factions.wealth.object.*;

import java.util.*;

public class SpawnerStackingHandler
{
    public void removeSpawner(final Location location, final CreatureSpawner creatureSpawner, final int n) {
        final short typeId = creatureSpawner.getSpawnedType().getTypeId();
        if (FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(typeId) == null) {
            return;
        }
        final FLocation fLocation = new FLocation(location);
        if (Board.getInstance().getFactionAt(fLocation).isSafeZone()) {
            return;
        }
        if (Board.getInstance().getFactionAt(fLocation).isWarZone()) {
            return;
        }
        if (Board.getInstance().getFactionAt(fLocation).isWilderness()) {
            return;
        }
        final String id = Board.getInstance().getFactionAt(fLocation).getId();
        if (!FactionsTopPlugin.getPluginInstance().unsorted.containsKey(id)) {
            return;
        }
        final FTop fTop = FactionsTopPlugin.getPluginInstance().unsorted.get(id);
        final SpawnerWorth spawnerWorth = FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(typeId);
        if (!fTop.spawners.containsKey(spawnerWorth)) {
            return;
        }
        final int intValue = fTop.spawners.get(spawnerWorth);
        final int n2 = intValue - n;
        if (n2 <= 0) {
            final FTop fTop2 = fTop;
            fTop2.spawner_balance -= intValue * spawnerWorth.price;
            fTop.spawners.remove(spawnerWorth);
            return;
        }
        fTop.spawners.put(spawnerWorth, n2);
        final FTop fTop3 = fTop;
        fTop3.spawner_balance -= n * spawnerWorth.price;
    }
    
    public void addSpawner(final Location location, final CreatureSpawner creatureSpawner, final int n) {
        final short typeId = creatureSpawner.getSpawnedType().getTypeId();
        if (FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(typeId) == null) {
            return;
        }
        final FLocation fLocation = new FLocation(location);
        if (Board.getInstance().getFactionAt(fLocation).isSafeZone()) {
            return;
        }
        if (Board.getInstance().getFactionAt(fLocation).isWarZone()) {
            return;
        }
        if (Board.getInstance().getFactionAt(fLocation).isWilderness()) {
            return;
        }
        final String id = Board.getInstance().getFactionAt(fLocation).getId();
        if (!FactionsTopPlugin.getPluginInstance().unsorted.containsKey(id)) {
            return;
        }
        final FTop fTop = FactionsTopPlugin.getPluginInstance().unsorted.get(id);
        final SpawnerWorth spawnerWorth = FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(typeId);
        if (!fTop.spawners.containsKey(spawnerWorth)) {
            fTop.spawners.put(spawnerWorth, n);
            final FTop fTop2 = fTop;
            fTop2.spawner_balance += n * spawnerWorth.price;
            return;
        }
        fTop.spawners.put(spawnerWorth, fTop.spawners.get(spawnerWorth) + n);
        final FTop fTop3 = fTop;
        fTop3.spawner_balance += n * spawnerWorth.price;
    }
    
    public void addMultipleSpawner(final Location location, final HashMap<Short, Integer> hashMap) {
        final FLocation fLocation = new FLocation(location);
        if (Board.getInstance().getFactionAt(fLocation).isSafeZone()) {
            return;
        }
        if (Board.getInstance().getFactionAt(fLocation).isWarZone()) {
            return;
        }
        if (Board.getInstance().getFactionAt(fLocation).isWilderness()) {
            return;
        }
        final String id = Board.getInstance().getFactionAt(fLocation).getId();
        if (!FactionsTopPlugin.getPluginInstance().unsorted.containsKey(id)) {
            return;
        }
        final FTop fTop = FactionsTopPlugin.getPluginInstance().unsorted.get(id);
        for (final Map.Entry<Short, Integer> entry : hashMap.entrySet()) {
            if (FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(entry.getKey()) == null) {
                continue;
            }
            final SpawnerWorth spawnerWorth = FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(entry.getKey());
            if (!fTop.spawners.containsKey(spawnerWorth)) {
                fTop.spawners.put(spawnerWorth, entry.getValue());
                final FTop fTop2 = fTop;
                fTop2.spawner_balance += entry.getValue() * spawnerWorth.price;
            }
            else {
                fTop.spawners.put(spawnerWorth, fTop.spawners.get(spawnerWorth) + entry.getValue());
                final FTop fTop3 = fTop;
                fTop3.spawner_balance += entry.getValue() * spawnerWorth.price;
            }
        }
    }
    
    public void removeMultipleSpawner(final Location location, final HashMap<Short, Integer> hashMap) {
        final FLocation fLocation = new FLocation(location);
        if (Board.getInstance().getFactionAt(fLocation).isSafeZone()) {
            return;
        }
        if (Board.getInstance().getFactionAt(fLocation).isWarZone()) {
            return;
        }
        if (Board.getInstance().getFactionAt(fLocation).isWilderness()) {
            return;
        }
        final String id = Board.getInstance().getFactionAt(fLocation).getId();
        if (!FactionsTopPlugin.getPluginInstance().unsorted.containsKey(id)) {
            return;
        }
        final FTop fTop = FactionsTopPlugin.getPluginInstance().unsorted.get(id);
        for (final Map.Entry<Short, Integer> entry : hashMap.entrySet()) {
            if (FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(entry.getKey()) == null) {
                continue;
            }
            final SpawnerWorth spawnerWorth = FactionsTopPlugin.getPluginInstance().getUtil().getSpawnerWorth(entry.getKey());
            if (!fTop.spawners.containsKey(spawnerWorth)) {
                continue;
            }
            final int intValue = fTop.spawners.get(spawnerWorth);
            final int n = intValue - entry.getValue();
            if (n <= 0) {
                final FTop fTop2 = fTop;
                fTop2.spawner_balance -= intValue * spawnerWorth.price;
                fTop.spawners.remove(spawnerWorth);
            }
            else {
                fTop.spawners.put(spawnerWorth, n);
                final FTop fTop3 = fTop;
                fTop3.spawner_balance -= entry.getValue() * spawnerWorth.price;
            }
        }
    }
}
