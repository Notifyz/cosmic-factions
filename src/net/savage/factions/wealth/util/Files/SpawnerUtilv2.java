package net.savage.factions.wealth.util.Files;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.SpawnerWorth;

public class SpawnerUtilv2
{
    private FactionsTopPlugin factionstopplugin;
    public File spawnersv2;

    public SpawnerUtilv2() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public void saveSpawnerWorth(final SpawnerWorth spawnerWorth) {
        final YamlConfiguration loadConfiguration = YamlConfiguration.loadConfiguration(this.spawnersv2);
        final int n = ((FileConfiguration)loadConfiguration).getKeys(false).size() + 1;
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".material", spawnerWorth.material.name());
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".data", spawnerWorth.data);
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".price", spawnerWorth.price);
        ((FileConfiguration)loadConfiguration).set(String.valueOf(n) + ".spawnername", spawnerWorth.spawnername);
        try {
            ((FileConfiguration)loadConfiguration).save(this.spawnersv2);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void loadSpawners() {
        final YamlConfiguration loadConfiguration = YamlConfiguration.loadConfiguration(this.spawnersv2);
        final Iterator<String> iterator = ((FileConfiguration)loadConfiguration).getKeys(false).iterator();
        while (iterator.hasNext()) {
            final ConfigurationSection configurationSection = ((FileConfiguration)loadConfiguration).getConfigurationSection(iterator.next());
            this.factionstopplugin.getSpawnerWorth().add(new SpawnerWorth(Material.getMaterial(configurationSection.getString("material")), (short)configurationSection.getInt("data"), configurationSection.getDouble("price"), configurationSection.getString("spawnername")));
        }
    }

    public void setup(final File file) {
        if (!file.exists()) {
            file.mkdirs();
        }
        this.spawnersv2 = new File(file + File.separator + "spawner-wealth.yml");
        if (!this.spawnersv2.exists()) {
            try {
                this.spawnersv2.createNewFile();
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 4, 2000.0, "ElderGuardian"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 6, 2000.0, "Stray"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 23, 2000.0, "Husk"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 31, 2000.0, "Donkey"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 32, 2000.0, "Mule"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 34, 2000.0, "Evoker"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 35, 2000.0, "Vex"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 36, 2000.0, "Vindicator"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 37, 2000.0, "Illusioner"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 50, 500000.0, "Creeper"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 51, 70000.0, "Skeleton"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 52, 45000.0, "Spider"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 53, 10000.0, "Giant"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 54, 70000.0, "Zombie"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 55, 10000.0, "Slime"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 56, 20000.0, "Ghast"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 57, 750000.0, "Pigman"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 58, 200000.0, "Enderman"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 59, 2000.0, "CaveSpider"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 60, 1.5E7, "SilverFish"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 61, 350000.0, "Blaze"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 62, 10000.0, "MagmaCube"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 63, 5.0E7, "EnderDragon"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 64, 500000.0, "Wither"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 65, 10000.0, "Bat"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 66, 15000.0, "Witch"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 67, 2000.0, "Endermite"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 68, 2000.0, "Guardian"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 90, 10000.0, "Pig"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 91, 10000.0, "Sheep"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 92, 50000.0, "Cow"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 93, 5000.0, "Chicken"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 94, 10000.0, "Squid"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 95, 10000.0, "Wolf"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 96, 100000.0, "MushroomCow"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 97, 10000.0, "Snowman"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 98, 10000.0, "Ocelot"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 99, 1500000.0, "IronGolem"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 100, 100000.0, "Horse"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 101, 10000.0, "Rabbit"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 102, 2000.0, "PolarBear"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 103, 2000.0, "Llama"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 105, 2000.0, "Parrot"));
                this.saveSpawnerWorth(new SpawnerWorth(Material.MOB_SPAWNER, 120, 120000.0, "Villager"));
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        this.loadSpawners();
    }
}
