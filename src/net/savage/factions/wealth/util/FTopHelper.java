package net.savage.factions.wealth.util;

import org.bukkit.*;
import org.bukkit.block.*;

import com.massivecraft.factions.*;

import net.savage.factions.wealth.*;
import net.savage.factions.wealth.object.*;

public class FTopHelper
{
    private FactionsTopPlugin factionstopplugin;
    
    public FTopHelper() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }
    
    public void handleAdd(final Location location, final CreatureSpawner creatureSpawner) {
        final FLocation fLocation = new FLocation(location);
        final Faction faction = Board.getInstance().getFactionAt(fLocation);
        if (faction.isWilderness() || faction.isSafeZone() || faction.isWarZone()) {
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        if (this.factionstopplugin.is_calculating) {
            final FTop fTop = this.factionstopplugin.unsorted.get(id);
            if (!this.factionstopplugin.tocalculate.contains(fLocation)) {
                this.factionstopplugin.getUtil().addSpawner(fTop, creatureSpawner.getSpawnedType().getTypeId());
            }
        }
        else if (!this.factionstopplugin.is_calculating) {
            this.factionstopplugin.getUtil().addSpawner(this.factionstopplugin.unsorted.get(id), creatureSpawner.getSpawnedType().getTypeId());
        }
    }
    
    public void handleAdd(final CreatureSpawner creatureSpawner) {
        final FLocation fLocation = new FLocation(creatureSpawner.getLocation());
        final Faction faction = Board.getInstance().getFactionAt(fLocation);
        if (faction.isWilderness() || faction.isSafeZone() || faction.isWarZone()) {
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        if (this.factionstopplugin.is_calculating) {
            final FTop fTop = this.factionstopplugin.unsorted.get(id);
            if (!this.factionstopplugin.tocalculate.contains(fLocation)) {
                this.factionstopplugin.getUtil().addSpawner(fTop, creatureSpawner.getSpawnedType().getTypeId());
            }
        }
        else if (!this.factionstopplugin.is_calculating) {
            this.factionstopplugin.getUtil().addSpawner(this.factionstopplugin.unsorted.get(id), creatureSpawner.getSpawnedType().getTypeId());
        }
    }
    
    public void handleRemove(final Location location, final CreatureSpawner creatureSpawner) {
        final FLocation fLocation = new FLocation(location);
        final Faction faction = Board.getInstance().getFactionAt(fLocation);
        if (faction.isWilderness() || faction.isSafeZone() || faction.isWarZone()) {
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        if (this.factionstopplugin.is_calculating) {
            final FTop fTop = this.factionstopplugin.unsorted.get(id);
            if (!this.factionstopplugin.tocalculate.contains(fLocation)) {
                this.factionstopplugin.getUtil().removeSpawner(fTop, creatureSpawner.getSpawnedType().getTypeId());
            }
        }
        else if (!this.factionstopplugin.is_calculating) {
            this.factionstopplugin.getUtil().removeSpawner(this.factionstopplugin.unsorted.get(id), creatureSpawner.getSpawnedType().getTypeId());
        }
    }
    
    public void handleRemove(final CreatureSpawner creatureSpawner) {
        final FLocation fLocation = new FLocation(creatureSpawner.getLocation());
        final Faction faction = Board.getInstance().getFactionAt(fLocation);
        if (faction.isWilderness() || faction.isSafeZone() || faction.isWarZone()) {
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        if (this.factionstopplugin.is_calculating) {
            final FTop fTop = this.factionstopplugin.unsorted.get(id);
            if (!this.factionstopplugin.tocalculate.contains(fLocation)) {
                this.factionstopplugin.getUtil().removeSpawner(fTop, creatureSpawner.getSpawnedType().getTypeId());
            }
        }
        else if (!this.factionstopplugin.is_calculating) {
            this.factionstopplugin.getUtil().removeSpawner(this.factionstopplugin.unsorted.get(id), creatureSpawner.getSpawnedType().getTypeId());
        }
    }
    
    public void handleAdd(final Location location, final short n) {
        final Faction faction = Board.getInstance().getFactionAt(new FLocation(location));
        if (faction.isWilderness() || faction.isSafeZone() || faction.isWarZone()) {
            return;
        }
        final String id = faction.getId();
        if (!this.factionstopplugin.unsorted.containsKey(id)) {
            return;
        }
        this.factionstopplugin.getUtil().addSpawner(this.factionstopplugin.unsorted.get(id), n);
    }
}
