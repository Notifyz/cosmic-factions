package net.savage.factions.wealth.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.savage.factions.wealth.FactionsTopPlugin;
import net.savage.factions.wealth.object.BlockWorth;
import net.savage.factions.wealth.object.FTop;
import net.savage.factions.wealth.object.SpawnerWorth;

public class Util
{
    private FactionsTopPlugin factionstopplugin;

    public Util() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    public String fixColour(final String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public SpawnerWorth getSpawnerWorth(final int n) {
        if (this.factionstopplugin.getSpawnerWorth().size() == 0) {
            return null;
        }
        for (final SpawnerWorth spawnerWorth : this.factionstopplugin.getSpawnerWorth()) {
            if (spawnerWorth.data == n) {
                return spawnerWorth;
            }
        }
        return null;
    }

    public BlockWorth getBlockWorth(final Material material, final short n) {
        if (this.factionstopplugin.getBlockWorth().size() == 0) {
            return null;
        }
        for (final BlockWorth blockWorth : this.factionstopplugin.getBlockWorth()) {
            if (blockWorth.material == material && blockWorth.data == n) {
                return blockWorth;
            }
        }
        return null;
    }

    public ArrayList<FTop> getFactions() {
        ArrayList<FTop> arrayList = new ArrayList<>();
        HashMap<Object, Double> hashMap = new HashMap<>();
        for (Map.Entry<String, FTop> entry : this.factionstopplugin.unsorted.entrySet()) {
            FTop fTop = entry.getValue();
            hashMap.put(fTop, fTop.getTotalValue());
        }
        Map<Object, Double> hashMap2 = sortByValue(hashMap);
        for (Entry<Object, Double> entry : hashMap2.entrySet()) {
            FTop fTop = (FTop)entry.getKey();
            arrayList.add(fTop);
        }
        hashMap.clear();
        hashMap2.clear();
        return arrayList;
    }

    public <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
    	List<Map.Entry<K, V>> list = new LinkedList<>(map.entrySet());
    	Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
    		@Override
    		public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
    			return (e1.getValue()).compareTo(e2.getValue());
    		}
    	});

    	Map<K, V> result = new LinkedHashMap<>();
    	for (Map.Entry<K, V> entry : list) {
    		result.put(entry.getKey(), entry.getValue());
    	}

    	return result;
    }

    public void addBlock(final FTop fTop, final Block block) {
        if (block.getType() == Material.MOB_SPAWNER) {
            this.addSpawner(fTop, ((CreatureSpawner)block.getState()).getSpawnedType().getTypeId());
            return;
        }
        if (this.getBlockWorth(block.getType(), block.getData()) == null) {
            return;
        }
        final BlockWorth blockWorth = this.getBlockWorth(block.getType(), block.getData());
        fTop.block_balance += blockWorth.price;
        if (fTop.blocks.containsKey(blockWorth)) {
            fTop.blocks.put(blockWorth, fTop.blocks.get(blockWorth) + 1);
            return;
        }
        fTop.blocks.put(blockWorth, 1);
    }

    public void removeBlock(final FTop fTop, final Block block) {
        if (block.getType() == Material.MOB_SPAWNER) {
            this.removeSpawner(fTop, ((CreatureSpawner)block.getState()).getSpawnedType().getTypeId());
            return;
        }
        if (this.getBlockWorth(block.getType(), block.getData()) == null) {
            return;
        }
        final BlockWorth blockWorth = this.getBlockWorth(block.getType(), block.getData());
        if (!fTop.blocks.containsKey(blockWorth)) {
            return;
        }
        fTop.block_balance = this.toRemove(fTop.block_balance, blockWorth.price);
        final int remove = this.toRemove(fTop.blocks.get(blockWorth), 1);
        if (remove >= 1) {
            fTop.blocks.put(blockWorth, remove);
            return;
        }
        fTop.blocks.remove(blockWorth);
    }

    public void addSpawner(final FTop fTop, final short n) {
        if (this.getSpawnerWorth(n) == null) {
            return;
        }
        final SpawnerWorth spawnerWorth = this.getSpawnerWorth(n);
        fTop.spawner_balance += spawnerWorth.price;
        if (fTop.spawners.containsKey(spawnerWorth)) {
            fTop.spawners.put(spawnerWorth, fTop.spawners.get(spawnerWorth) + 1);
            return;
        }
        fTop.spawners.put(spawnerWorth, 1);
    }

    public void removeSpawner(final FTop fTop, final short n) {
        if (this.getSpawnerWorth(n) == null) {
            return;
        }
        final SpawnerWorth spawnerWorth = this.getSpawnerWorth(n);
        if (!fTop.spawners.containsKey(spawnerWorth)) {
            return;
        }
        fTop.spawner_balance = this.toRemove(fTop.spawner_balance, spawnerWorth.price);
        final int remove = this.toRemove(fTop.spawners.get(spawnerWorth), 1);
        if (remove >= 1) {
            fTop.spawners.put(spawnerWorth, remove);
            return;
        }
        fTop.spawners.remove(spawnerWorth);
    }

    public int toRemove(final int n, final int n2) {
        int n3;
        if (n - n2 < 1) {
            n3 = 0;
        }
        else {
            n3 = n - n2;
        }
        return n3;
    }

    public double toRemove(final double n, final double n2) {
        double n3;
        if (n - n2 < 1.0) {
            n3 = 0.0;
        }
        else {
            n3 = n - n2;
        }
        return n3;
    }

    public String getCommandSenderName(final CommandSender commandSender) {
        if (commandSender instanceof Player) {
            return ((Player)commandSender).getDisplayName();
        }
        return commandSender.getName();
    }

    public String millis(final long n) {
        final int n2 = (int)(n / 1000L) % 60;
        final int n3 = (int)(n / 60000L % 60L);
        final int n4 = (int)(n / 3600000L % 24L);
        final int n5 = (int)(n / 86400000L % 365L);
        final int n6 = (int)(n / 1000L * 60L * 60L * 24L * 365L % 365L);
        final ArrayList<String> list = new ArrayList<>();
        if (n6 > 0) {
            list.add(String.valueOf(String.valueOf(n6)) + "years");
        }
        if (n5 > 0) {
            list.add(String.valueOf(String.valueOf(n5)) + " days");
        }
        if (n4 > 0) {
            list.add(String.valueOf(String.valueOf(n4)) + " hours");
        }
        if (n3 > 0) {
            list.add(String.valueOf(String.valueOf(n3)) + " minutes");
        }
        if (n2 > 0) {
            list.add(String.valueOf(String.valueOf(n2)) + " seconds");
        }
        String s = "";
        for (int i = 0; i < list.size(); ++i) {
            s = String.valueOf(s) + list.get(i);
            if (i != list.size() - 1) {
                s = String.valueOf(s) + ", ";
            }
        }
        if (s == "") {
            s = "0 sec";
        }
        return s;
    }

    public short getIdFromString(final String s) {
        if (s.equalsIgnoreCase("Elder_Guardian") || s.equalsIgnoreCase("ElderGuardian")) {
            return 4;
        }
        if (s.equalsIgnoreCase("Stray")) {
            return 6;
        }
        if (s.equalsIgnoreCase("Husk")) {
            return 23;
        }
        if (s.equalsIgnoreCase("Donkey")) {
            return 31;
        }
        if (s.equalsIgnoreCase("Mule")) {
            return 32;
        }
        if (s.equalsIgnoreCase("Evoker")) {
            return 34;
        }
        if (s.equalsIgnoreCase("Vex")) {
            return 35;
        }
        if (s.equalsIgnoreCase("Vindicator")) {
            return 36;
        }
        if (s.equalsIgnoreCase("Illusioner")) {
            return 37;
        }
        if (s.equalsIgnoreCase("Creeper")) {
            return 50;
        }
        if (s.equalsIgnoreCase("Skeleton")) {
            return 51;
        }
        if (s.equalsIgnoreCase("Spider")) {
            return 52;
        }
        if (s.equalsIgnoreCase("Giant")) {
            return 53;
        }
        if (s.equalsIgnoreCase("Zombie")) {
            return 54;
        }
        if (s.equalsIgnoreCase("Slime")) {
            return 55;
        }
        if (s.equalsIgnoreCase("Ghast")) {
            return 56;
        }
        if (s.equalsIgnoreCase("Pigman")) {
            return 57;
        }
        if (s.equalsIgnoreCase("Enderman")) {
            return 58;
        }
        if (s.equalsIgnoreCase("CaveSpider") || s.equalsIgnoreCase("Cave Spider")) {
            return 59;
        }
        if (s.equalsIgnoreCase("SilverFish")) {
            return 60;
        }
        if (s.equalsIgnoreCase("Blaze")) {
            return 61;
        }
        if (s.equalsIgnoreCase("MagmaCube")) {
            return 62;
        }
        if (s.equalsIgnoreCase("Bat")) {
            return 65;
        }
        if (s.equalsIgnoreCase("Witch")) {
            return 66;
        }
        if (s.equalsIgnoreCase("Endermite")) {
            return 67;
        }
        if (s.equalsIgnoreCase("Guardian")) {
            return 68;
        }
        if (s.equalsIgnoreCase("Pig")) {
            return 90;
        }
        if (s.equalsIgnoreCase("Sheep")) {
            return 91;
        }
        if (s.equalsIgnoreCase("Cow")) {
            return 92;
        }
        if (s.equalsIgnoreCase("Chicken")) {
            return 93;
        }
        if (s.equalsIgnoreCase("Squid")) {
            return 94;
        }
        if (s.equalsIgnoreCase("Wolf")) {
            return 95;
        }
        if (s.equalsIgnoreCase("MushroomCow")) {
            return 96;
        }
        if (s.equalsIgnoreCase("Snowman")) {
            return 97;
        }
        if (s.equalsIgnoreCase("Ocelot")) {
            return 98;
        }
        if (s.equalsIgnoreCase("IronGolem") || s.equalsIgnoreCase("Iron Golem") || s.equalsIgnoreCase("Iron_Golem") || s.equalsIgnoreCase("VillagerGolem") || s.equalsIgnoreCase("Villager_Golem")) {
            return 99;
        }
        if (s.equalsIgnoreCase("Horse")) {
            return 100;
        }
        if (s.equalsIgnoreCase("Rabbit")) {
            return 101;
        }
        if (s.equalsIgnoreCase("Polarbear")) {
            return 102;
        }
        if (s.equalsIgnoreCase("Llama")) {
            return 103;
        }
        if (s.equalsIgnoreCase("Parrot")) {
            return 105;
        }
        if (s.equalsIgnoreCase("Villager")) {
            return 120;
        }
        return 0;
    }

    public boolean isSystemFactionById(final String s) {
        return s.equalsIgnoreCase("0") || s.equalsIgnoreCase("-1") || s.equalsIgnoreCase("-2");
    }

    public boolean isSystemFaction(final String s) {
        return s.equalsIgnoreCase("Wilderness") || s.equalsIgnoreCase("SafeZone") || s.equalsIgnoreCase("Warzone");
    }

    public short getMobIdFromString(final String s) {
        if (s.equalsIgnoreCase("Elder_Guardian") || s.equalsIgnoreCase("ElderGuardian")) {
            return 4;
        }
        if (s.equalsIgnoreCase("Stray")) {
            return 6;
        }
        if (s.equalsIgnoreCase("Husk")) {
            return 23;
        }
        if (s.equalsIgnoreCase("Donkey")) {
            return 31;
        }
        if (s.equalsIgnoreCase("Mule")) {
            return 32;
        }
        if (s.equalsIgnoreCase("Evoker")) {
            return 34;
        }
        if (s.equalsIgnoreCase("Vex")) {
            return 35;
        }
        if (s.equalsIgnoreCase("Vindicator")) {
            return 36;
        }
        if (s.equalsIgnoreCase("Illusioner")) {
            return 37;
        }
        if (s.equalsIgnoreCase("Creeper")) {
            return 50;
        }
        if (s.equalsIgnoreCase("Skeleton")) {
            return 51;
        }
        if (s.equalsIgnoreCase("Spider")) {
            return 52;
        }
        if (s.equalsIgnoreCase("Giant")) {
            return 53;
        }
        if (s.equalsIgnoreCase("Zombie")) {
            return 54;
        }
        if (s.equalsIgnoreCase("Slime")) {
            return 55;
        }
        if (s.equalsIgnoreCase("Ghast")) {
            return 56;
        }
        if (s.equalsIgnoreCase("Pigman")) {
            return 57;
        }
        if (s.equalsIgnoreCase("Enderman")) {
            return 58;
        }
        if (s.equalsIgnoreCase("CaveSpider") || s.equalsIgnoreCase("Cave Spider")) {
            return 59;
        }
        if (s.equalsIgnoreCase("SilverFish")) {
            return 60;
        }
        if (s.equalsIgnoreCase("Blaze")) {
            return 61;
        }
        if (s.equalsIgnoreCase("MagmaCube")) {
            return 62;
        }
        if (s.equalsIgnoreCase("Bat")) {
            return 65;
        }
        if (s.equalsIgnoreCase("Witch")) {
            return 66;
        }
        if (s.equalsIgnoreCase("Endermite")) {
            return 67;
        }
        if (s.equalsIgnoreCase("Guardian")) {
            return 68;
        }
        if (s.equalsIgnoreCase("Pig")) {
            return 90;
        }
        if (s.equalsIgnoreCase("Sheep")) {
            return 91;
        }
        if (s.equalsIgnoreCase("Cow")) {
            return 92;
        }
        if (s.equalsIgnoreCase("Chicken")) {
            return 93;
        }
        if (s.equalsIgnoreCase("Squid")) {
            return 94;
        }
        if (s.equalsIgnoreCase("Wolf")) {
            return 95;
        }
        if (s.equalsIgnoreCase("MushroomCow")) {
            return 96;
        }
        if (s.equalsIgnoreCase("Snowman")) {
            return 97;
        }
        if (s.equalsIgnoreCase("Ocelot")) {
            return 98;
        }
        if (s.equalsIgnoreCase("IronGolem") || s.equalsIgnoreCase("Iron Golem") || s.equalsIgnoreCase("Iron_Golem") || s.equalsIgnoreCase("VillagerGolem") || s.equalsIgnoreCase("Villager_Golem")) {
            return 99;
        }
        if (s.equalsIgnoreCase("Horse")) {
            return 100;
        }
        if (s.equalsIgnoreCase("Rabbit")) {
            return 101;
        }
        if (s.equalsIgnoreCase("Polarbear")) {
            return 102;
        }
        if (s.equalsIgnoreCase("Llama")) {
            return 103;
        }
        if (s.equalsIgnoreCase("Parrot")) {
            return 105;
        }
        if (s.equalsIgnoreCase("Villager")) {
            return 120;
        }
        return 0;
    }
}
