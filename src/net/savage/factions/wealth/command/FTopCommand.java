package net.savage.factions.wealth.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.P;

import net.savage.factions.wealth.FactionsTopPlugin;

public class FTopCommand implements CommandExecutor
{
    private FactionsTopPlugin factionstopplugin;

    public FTopCommand() {
        this.factionstopplugin = FactionsTopPlugin.getPluginInstance();
    }

    @Override
	public boolean onCommand(final CommandSender commandSender, final Command command, final String s, final String[] array) {
        if (command.getName().equalsIgnoreCase("FTop")) {
            if (!(commandSender instanceof Player)) {
                return false;
            }
            final Player player = (Player)commandSender;
            if (array.length == 0) {
                this.factionstopplugin.getUserInterface().send(player, 1);
                return false;
            } else if (array.length == 1 && array[0].equalsIgnoreCase("calculate")) {
            	if (!commandSender.hasPermission("faction.command.ftop")) {
            		return false;
            	}
            	Bukkit.getScheduler().runTaskAsynchronously(P.getInstance(), () -> {
            		factionstopplugin.calculateFactions();
            	});
            	commandSender.sendMessage(ChatColor.GREEN + "You have successfully reloaded calculations.");
            	return false;
            }
            int int1;
            try {
                int1 = Integer.parseInt(array[0]);
            }
            catch (Exception ex) {
                int1 = 1;
            }
            this.factionstopplugin.getUserInterface().send(player, int1);
        }
        return false;
    }
}
