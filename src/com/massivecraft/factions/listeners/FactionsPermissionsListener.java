package com.massivecraft.factions.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.zcore.persist.MemoryPermissions;
import com.sk89q.worldguard.internal.flywaydb.core.internal.util.Pair;

public class FactionsPermissionsListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Block block = event.getClickedBlock();
		ItemStack item = event.getItem();
		Location location = player.getLocation();
		Faction playerFaction = FPlayers.getInstance().getByPlayer(player).getFaction();
		Faction faction = Board.getInstance().getFactionAt(new FLocation(location));

		if (faction == null || faction.isWarZone() || faction.isSafeZone() || faction.isWilderness()) return; // We don't want to handle system factions.

		if (item == null || item.getType() == Material.AIR || item.getType() == Material.POTION || item.getType() == Material.ENDER_PEARL) return;

		if (playerFaction == null) { // Try to search Key as playerName
			if ((!faction.getPlayerDefaultFPermissions().containsKey(player.getName())) &&
				(!faction.getPlayerFPermissions().containsKey(player.getName()))) {
				event.setCancelled(true);
				return;
			}
		} else { // Try to search Key as factionName n' playerName
			if (playerFaction == faction) return;

			if ((!faction.getPlayerDefaultFPermissions().containsKey(player.getName())) &&
				(!faction.getPlayerFPermissions().containsKey(player.getName())) &&
				(!faction.getFactionDefaultFPermissions().containsKey(playerFaction.getTag())) &&
				(!faction.getFactionDefaultFPermissions().containsKey(playerFaction.getTag()))) {
				event.setCancelled(true);
				return;
			}
		}

		if (faction.getPlayerDefaultFPermissions().containsKey(player.getName())) {
			MemoryPermissions permission = faction.getPlayerDefaultFPermissions().get(player.getName());

			if (!permission.getInsertedChunks().contains(Pair.of(location.getChunk().getX(), location.getChunk().getZ()))) {
				event.setCancelled(true);
				return;
			}

			if (permission.isFullAccess()) {
				return;
			}

			if (permission.isCanUseChests() && block != null && block.getType() == Material.CHEST) {
				return;
			}

			if (permission.isCanInteractAllBlocks()) {
				return;
			}

			if (permission.isCanInteractSponges() && block != null && block.getType() == Material.SPONGE) {
				return;
			}

			if (permission.isCanInteractDispensers() && block != null && block.getType() == Material.DISPENSER) {
				return;
			}

			if (permission.isCanInteractDroppers() && block != null && block.getType() == Material.DROPPER) {
				return;
			}

			if (permission.isCanPlaceEggs() && item != null && item.getType() == Material.MONSTER_EGG) {
				return;
			}

			if (permission.isCanInteractTNT() && block != null && block.getType() == Material.TNT) {
				return;
			}

			if (permission.isCanInteractObsidian() && block != null && block.getType() == Material.OBSIDIAN) {
				return;
			}

			if (permission.isCanInteractSpawners() && block != null && block.getType() == Material.MOB_SPAWNER) {
				return;
			}

			if (permission.isCanInteractHoppers() && block != null && block.getType() == Material.HOPPER) {
				return;
			}

			if (permission.isCanInteractDoorsOrGates() && block != null && (block.getType() == Material.WOOD_DOOR || block.getType() == Material.FENCE_GATE)) {
				return;
			}

			if (permission.isCanInteractPressurePlates() && block != null && block.getType().name().endsWith("PLATE")) {
				return;
			}

			if (permission.isCanInteractLevers() && block != null && block.getType() == Material.LEVER) {
				return;
			}

			if (permission.isCanInteractButtons() && block != null && block.getType() == Material.STONE_BUTTON) {
				return;
			}

			if (permission.isCanInteractTrapdoors() && block != null && block.getType() == Material.TRAP_DOOR) {
				return;
			}

			event.setCancelled(true);
		}
		if (faction.getPlayerFPermissions().containsKey(player.getName())) {
			MemoryPermissions permission = faction.getPlayerFPermissions().get(player.getName());

			if (!permission.getInsertedChunks().contains(Pair.of(location.getChunk().getX(), location.getChunk().getZ()))) {
				event.setCancelled(true);
				return;
			}

			if (permission.isFullAccess()) {
				return;
			}

			if (permission.isCanUseChests() && block != null && block.getType() == Material.CHEST) {
				return;
			}

			if (permission.isCanInteractAllBlocks()) {
				return;
			}

			if (permission.isCanInteractSponges() && block != null && block.getType() == Material.SPONGE) {
				return;
			}

			if (permission.isCanInteractDispensers() && block != null && block.getType() == Material.DISPENSER) {
				return;
			}

			if (permission.isCanInteractDroppers() && block != null && block.getType() == Material.DROPPER) {
				return;
			}

			if (permission.isCanPlaceEggs() && item != null && item.getType() == Material.MONSTER_EGG) {
				return;
			}

			if (permission.isCanInteractTNT() && block != null && block.getType() == Material.TNT) {
				return;
			}

			if (permission.isCanInteractObsidian() && block != null && block.getType() == Material.OBSIDIAN) {
				return;
			}

			if (permission.isCanInteractSpawners() && block != null && block.getType() == Material.MOB_SPAWNER) {
				return;
			}

			if (permission.isCanInteractHoppers() && block != null && block.getType() == Material.HOPPER) {
				return;
			}

			if (permission.isCanInteractDoorsOrGates() && block != null && (block.getType() == Material.WOOD_DOOR || block.getType() == Material.FENCE_GATE)) {
				return;
			}

			if (permission.isCanInteractPressurePlates() && block != null && block.getType().name().endsWith("PLATE")) {
				return;
			}

			if (permission.isCanInteractLevers() && block != null && block.getType() == Material.LEVER) {
				return;
			}

			if (permission.isCanInteractButtons() && block != null && block.getType() == Material.STONE_BUTTON) {
				return;
			}

			if (permission.isCanInteractTrapdoors() && block != null && block.getType() == Material.TRAP_DOOR) {
				return;
			}

			event.setCancelled(true);
		}
		if (faction.getFactionDefaultFPermissions().containsKey(playerFaction.getTag())) {
			MemoryPermissions permission = faction.getFactionDefaultFPermissions().get(playerFaction.getTag());

			if (!permission.getInsertedChunks().contains(Pair.of(location.getChunk().getX(), location.getChunk().getZ()))) {
				event.setCancelled(true);
				return;
			}

			if (permission.isFullAccess()) {
				return;
			}

			if (permission.isCanUseChests() && block != null && block.getType() == Material.CHEST) {
				return;
			}

			if (permission.isCanInteractAllBlocks()) {
				return;
			}

			if (permission.isCanInteractSponges() && block != null && block.getType() == Material.SPONGE) {
				return;
			}

			if (permission.isCanInteractDispensers() && block != null && block.getType() == Material.DISPENSER) {
				return;
			}

			if (permission.isCanInteractDroppers() && block != null && block.getType() == Material.DROPPER) {
				return;
			}

			if (permission.isCanPlaceEggs() && item != null && item.getType() == Material.MONSTER_EGG) {
				return;
			}

			if (permission.isCanInteractTNT() && block != null && block.getType() == Material.TNT) {
				return;
			}

			if (permission.isCanInteractObsidian() && block != null && block.getType() == Material.OBSIDIAN) {
				return;
			}

			if (permission.isCanInteractSpawners() && block != null && block.getType() == Material.MOB_SPAWNER) {
				return;
			}

			if (permission.isCanInteractHoppers() && block != null && block.getType() == Material.HOPPER) {
				return;
			}

			if (permission.isCanInteractDoorsOrGates() && block != null && (block.getType() == Material.WOOD_DOOR || block.getType() == Material.FENCE_GATE)) {
				return;
			}

			if (permission.isCanInteractPressurePlates() && block != null && block.getType().name().endsWith("PLATE")) {
				return;
			}

			if (permission.isCanInteractLevers() && block != null && block.getType() == Material.LEVER) {
				return;
			}

			if (permission.isCanInteractButtons() && block != null && block.getType() == Material.STONE_BUTTON) {
				return;
			}

			if (permission.isCanInteractTrapdoors() && block != null && block.getType() == Material.TRAP_DOOR) {
				return;
			}

			event.setCancelled(true);
		}
		if (faction.getFactionFPermissions().containsKey(playerFaction.getTag())) {
			MemoryPermissions permission = faction.getFactionFPermissions().get(playerFaction.getTag());

			if (!permission.getInsertedChunks().contains(Pair.of(location.getChunk().getX(), location.getChunk().getZ()))) {
				event.setCancelled(true);
				return;
			}

			if (permission.isFullAccess()) {
				return;
			}

			if (permission.isCanUseChests() && block != null && block.getType() == Material.CHEST) {
				return;
			}

			if (permission.isCanInteractAllBlocks()) {
				return;
			}

			if (permission.isCanInteractSponges() && block != null && block.getType() == Material.SPONGE) {
				return;
			}

			if (permission.isCanInteractDispensers() && block != null && block.getType() == Material.DISPENSER) {
				return;
			}

			if (permission.isCanInteractDroppers() && block != null && block.getType() == Material.DROPPER) {
				return;
			}

			if (permission.isCanPlaceEggs() && item != null && item.getType() == Material.MONSTER_EGG) {
				return;
			}

			if (permission.isCanInteractTNT() && block != null && block.getType() == Material.TNT) {
				return;
			}

			if (permission.isCanInteractObsidian() && block != null && block.getType() == Material.OBSIDIAN) {
				return;
			}

			if (permission.isCanInteractSpawners() && block != null && block.getType() == Material.MOB_SPAWNER) {
				return;
			}

			if (permission.isCanInteractHoppers() && block != null && block.getType() == Material.HOPPER) {
				return;
			}

			if (permission.isCanInteractDoorsOrGates() && block != null && (block.getType() == Material.WOOD_DOOR || block.getType() == Material.FENCE_GATE)) {
				return;
			}

			if (permission.isCanInteractPressurePlates() && block != null && block.getType().name().endsWith("PLATE")) {
				return;
			}

			if (permission.isCanInteractLevers() && block != null && block.getType() == Material.LEVER) {
				return;
			}

			if (permission.isCanInteractButtons() && block != null && block.getType() == Material.STONE_BUTTON) {
				return;
			}

			if (permission.isCanInteractTrapdoors() && block != null && block.getType() == Material.TRAP_DOOR) {
				return;
			}

			event.setCancelled(true);
		}
	}
}
