package com.massivecraft.factions.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Function;

import org.bukkit.Chunk;
import org.bukkit.World;

import com.google.gson.JsonDeserializer;
import com.google.gson.JsonSerializer;


public interface JsonTypeAdapter<T> extends JsonDeserializer<T>, JsonSerializer<T>{


    public static Chunk[] findWildernessChunks(World world, int range, int maximum, Function<Chunk, Boolean> wildernessChecker) {
		for (int x = -maximum; x <= maximum; x ++) {
			next: for (int z = -maximum; z <= maximum; z ++) {
				int insertIndex = 0;
				Chunk[] chunks = new Chunk[999];
				Chunk chunk = world.getChunkAt(x, z);
				chunks[insertIndex ++] = chunk;
				if (wildernessChecker.apply(chunk)) {
					for (int xr = -range; xr <= range; xr ++) {
						for (int zr = -range; zr <= range; zr ++) {
							Chunk offset = world.getChunkAt(x + xr, z + zr);
							chunks[insertIndex ++] = offset;
							if (!wildernessChecker.apply(offset)) continue next;
						}
					}
				}
				return chunks;
			}
		}
		return null;
	}

    public static List<Chunk> findWildernessChunks(World world, Function<Chunk, Boolean> wildernessChecker) {
		for (int x = (ThreadLocalRandom.current().nextInt(6000) - 3000); x < 3000; x ++) {
			chunks: for (int z = (ThreadLocalRandom.current().nextInt(6000) - 3000); z < 3000; z ++) {
				Chunk chunk = world.getChunkAt(x >> 4, z >> 4);
				if (wildernessChecker.apply(chunk)) {
					List<Chunk> chunks = new ArrayList<>(Collections.singleton(chunk));
					for (int xo = -1; xo <= 1; xo ++) {
						for (int zo = -1; zo <= 1; zo ++) {
							Chunk other = world.getChunkAt(x + xo, z + zo);
							if (!wildernessChecker.apply(other)) continue chunks;
							chunks.add(other);
						}
					}
					return chunks;
				}
			}
		}
		return Collections.emptyList();
	}

    public static void main(String...strings) {
    	System.out.println((int) Math.pow(3, 2));
    }
}
