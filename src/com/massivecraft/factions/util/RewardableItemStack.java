package com.massivecraft.factions.util;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.inventory.ItemStack;

@SerializableAs("ItemStack")
public class RewardableItemStack implements ConfigurationSerializable {

    private int percentage;
    private ItemStack itemStack;

    public RewardableItemStack(ItemStack itemStack, Integer percentage, Integer amount){
        this.percentage = percentage;
        this.itemStack = itemStack;
        this.itemStack.setAmount(amount);
    }

    public RewardableItemStack(final Map<String, Object> map) {
        this.percentage = (int) map.get("percent");
        this.itemStack = (ItemStack) map.get("itemStack");
    }

    @Override
	public Map<String, Object> serialize() {
        Map<String, Object> map = new HashMap<>();
        map.put("percent", percentage);
        map.put("itemStack", itemStack);
        return map;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }
}