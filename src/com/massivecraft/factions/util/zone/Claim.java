package com.massivecraft.factions.util.zone;

import java.util.Map;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import com.massivecraft.factions.P;
import com.massivecraft.factions.game.Event;


/**
 * An implementation of a {@link NamedCuboid} that represents land for a {@link Event} can own.
 */
public class Claim extends NamedCuboid implements Cloneable, ConfigurationSerializable {

    private static final Random RANDOM = new Random();
    private final UUID claimUniqueID;
    private final UUID eventUUID;

    public Claim(Map<String, Object> map) {
        super(map);

        this.name = (String) map.get("name");
        this.claimUniqueID = UUID.fromString((String) map.get("claimUUID"));
        this.eventUUID = UUID.fromString((String) map.get("eventUUID"));
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = super.serialize();
        map.put("name", name);
        map.put("claimUUID", claimUniqueID.toString());
        map.put("eventUUID", eventUUID.toString());
        return map;
    }

    public Claim(Event Event, Location location) {
        super(location, location);
        this.name = generateName();
        this.eventUUID = Event.getUniqueID();
        this.claimUniqueID = UUID.randomUUID();
    }

    public Claim(Event Event, Location location1, Location location2) {
        super(location1, location2);
        this.name = generateName();
        this.eventUUID = Event.getUniqueID();
        this.claimUniqueID = UUID.randomUUID();
    }

    public Claim(Event Event, World world, int x1, int y1, int z1, int x2, int y2, int z2) {
        super(world, x1, y1, z1, x2, y2, z2);
        this.name = generateName();
        this.eventUUID = Event.getUniqueID();
        this.claimUniqueID = UUID.randomUUID();
    }

    public Claim(Event Event, Cuboid cuboid) {
        super(cuboid);
        this.name = generateName();
        this.eventUUID = Event.getUniqueID();
        this.claimUniqueID = UUID.randomUUID();
    }

    private String generateName() {
        return String.valueOf(RANDOM.nextInt(899) + 100);
    }

    public UUID getClaimUniqueID() {
        return claimUniqueID;
    }

    private Event event;
    private boolean loaded = false;

    public Event getEvent() {
        if (!this.loaded) {
            Event Event = P.getInstance().getEventManager().getEvent(this.eventUUID);
            if (Event instanceof Event) {
                this.event = Event;
            }

            this.loaded = true;
        }

        return this.event;
    }

    /**
     * Gets the formatted name for this {@link Claim}.
     *
     * @return the {@link Claim} formatted name
     */
    public String getFormattedName() {
        return getName() + ": (" + worldName + ", " + x1 + ", " + y1 + ", " + z1 + ") - (" + worldName + ", " + x2 + ", " + y2 + ", " + z2 + ')';
    }

    @Override
    public Claim clone() {
        return (Claim) super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Claim blocks = (Claim) o;

        if (loaded != blocks.loaded) return false;
        if (claimUniqueID != null ? !claimUniqueID.equals(blocks.claimUniqueID) : blocks.claimUniqueID != null) return false;
        if (eventUUID != null ? !eventUUID.equals(blocks.eventUUID) : blocks.eventUUID != null) return false;
        return !(event != null ? !event.equals(blocks.event) : blocks.event != null);
    }

    @Override
    public int hashCode() {
        int result = 0;
        result = 31 * result + (claimUniqueID != null ? claimUniqueID.hashCode() : 0);
        result = 31 * result + (eventUUID != null ? eventUUID.hashCode() : 0);
        result = 31 * result + (event != null ? event.hashCode() : 0);
        result = 31 * result + (loaded ? 1 : 0);
        return result;
    }
}