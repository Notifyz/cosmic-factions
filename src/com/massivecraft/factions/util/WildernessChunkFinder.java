package com.massivecraft.factions.util;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;

public class WildernessChunkFinder {

	public static void doTeleport(Player player) {
		/*Chunk[] chunks = JsonTypeAdapter.findWildernessChunks(player.getWorld(), 3, 3000 >> 4, (Chunk result) -> Board.getInstance().getFactionAt(new FLocation(result)).isWilderness());
		if (chunks != null) {
			for (int i = 0; i < chunks.length; i++) {
				Chunk foundChunk = chunks[i];
				Bukkit.broadcastMessage("Found a chunk with type and location of -> " + i + "::" + (foundChunk.getX() << 4 + 7) + "::" + (foundChunk.getZ() << 4 + 7));
			}
			Chunk chunk = chunks[0]; // choose a chunk?
			int x = chunk.getX() << 4 + 7, z = chunk.getZ() << 4 + 7;
			player.teleport(new Location(chunk.getWorld(), x, chunk.getWorld().getHighestBlockYAt(x, z), z, player.getLocation().getYaw(), player.getLocation().getPitch()));*/
		//}

		List<Chunk> setChunks = JsonTypeAdapter.findWildernessChunks(player.getWorld(), (Chunk result) -> Board.getInstance().getFactionAt(new FLocation(result)).isWilderness());
		if (!setChunks.isEmpty()) {
			for (int i = 1; i < setChunks.size(); i++) {
				Chunk foundChunk = setChunks.get(i);
				Bukkit.broadcastMessage("V2 Found a chunk with type and location of -> " + i + "::" + (foundChunk.getX()) + "::" + (foundChunk.getZ()));
			}
		}
	}
}