package com.massivecraft.factions.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.io.BukkitObjectInputStream;
import org.bukkit.util.io.BukkitObjectOutputStream;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.struct.Role;
import com.massivecraft.factions.zcore.persist.MemoryPermissions;
import com.massivecraft.factions.zcore.persist.MemoryRoles;
import com.massivecraft.factions.zcore.persist.MemoryTypeRoles;

public class InventoryUtil {

	public static String FPERM_MENU_TITLE = "/f perm Menu";
	public static String FPERM_DEFAULT_PERMISSIONS_MENU_TITLE = "Default Permissions";
	public static String FPERM_CURRENT_CHUNK_PERMS_MENU_TITLE = "/f perm: Current Chunk Perms";
	public static String FPERM_EDIT_PLAYER_PERMS_MENU_TITLE = "PLAYER player for CORD1x CORD2z";
	public static String FPERM_EDIT_FACTION_PERMS_MENU_TITLE = "FACTION faction for CORD1x CORD2z";
	public static String FPERM_EDIT_DEFAULT_FACTION_PERMS_MENU_TITLE = "FACTION default faction for CORD1x CORD2z";
	public static String FPERM_EDIT_DEFAULT_PLAYER_PERMS_MENU_TITLE = "PLAYER default player for CORD1x CORD2z";
	public static String FACCESS_MENU_TITLE = "/f access: Select Type";
	public static String FACCESS_PLAYERS_MENU_TITLE = "/f access: Players";
	public static String FACCESS_FACTIONS_MENU_TITLE = "/f access: Factions";

	public static String FROLES_TITLE = "Faction Roles";
	public static String FROLES_RECRUIT_TITLE = "Recruit Role Permissions";
	public static String FROLES_MEMBER_TITLE = "Member Role Permissions";
	public static String FROLES_MODERATOR_TITLE = "Moderator Role Permissions";
	public static String FROLES_COLEADER_TITLE = "Co-Leader Role Permissions";
	public static String FROLES_MEMBERS_TITLE = "Faction Members";
	public static String FROLES_FPLAYER_TITLE = "FPLAYER's Role Permissions";

	public static String FACTION_DISBAND_TITLE = "Confirm /f disband";

	public static Inventory getFPermissionMenu() {
		Inventory inventory = Bukkit.createInventory(null, 27, FPERM_MENU_TITLE);
		inventory.setItem(4, new ItemBuilder(Material.BOOK)
				.displayName("&b&l/f perm Help")
				.lores(Arrays.asList(
						"&7You can use this menu to customize",
						"&7what specific types of blocks your",
						"&7/f access'd factions/players can modify",
						"&7in your current chunk, and by default.",
						"",
						"&b/f access is REQUIRED for /f perm",
						"&7Players and Factions will not show up",
						"&7in this menu until they have /f access",
						"&7to atleast one of your faction's chunks.",
						"",
						"&bCurrent Chunk Permission",
						"&7Edits the permissions of the specified",
						"&7player or faction in the current chunk",
						"&7you are standing in. These permissions",
						"&7&nOVERRUE&7 any Default Permissions.",
						"",
						"&bDefault Permissions",
						"&7Edits the permission for the",
						"&7specified player or faction. These settings",
						"&7are what the given entity will be able to",
						"&7modify in the chunks where no ''Current Chunk''"))
				.build());
		inventory.setItem(12, new ItemBuilder(Material.GRASS)
				.displayName("&b&lCurrent Chunk Permissions")
				.lores(Arrays.asList(
						"&7Click to view all permissions",
						"&7for players and factions."))
				.build());
		inventory.setItem(14, new ItemBuilder(Material.BEACON)
				.displayName("&b&lDefault Permissions")
				.lores(Arrays.asList(
						"&7Click to view all permissions",
						"&7for players and factions."))
				.build());
		inventory.setItem(22, new ItemBuilder(Material.BOOK_AND_QUILL)
				.displayName("&b&l/f roles")
				.lores(Arrays.asList(
						"&7Click to view your faction role permissions.",
						"&7Role permissions allow you to permit different",
						"&7ranking faction members to perform specific tasks."))
				.build());
		return inventory;
	}

	public static Inventory getFAccessMenu() {
		Inventory inventory = Bukkit.createInventory(null, 18, FACCESS_MENU_TITLE);
		inventory.setItem(4, new ItemBuilder(Material.BOOK)
				.displayName("&b&l/f access Help")
				.lores(Arrays.asList(
						"&7You can use this menu to customize",
						"&7both per-player and per-faction access",
						"&7to your faction's claims.",
						"",
						"&b/f access <p/f> <name> <yes/no/none/all>",
						"&7Give a player (p) or faction (f) with <name>",
						"&7access to the current chunk you are in.",
						"",
						"&byes &7= Give Access",
						"&bno &7= Revoke Access",
						"&bnone &7= Revoke Access to ALL chunks",
						"&ball &7= Give Access to ALL chunks",
						"",
						"&b/f access list",
						"&7List all entities with access to",
						"&7the current chunk you are in.",
						"",
						"&b/f access clear",
						"&7Revokes access from all entities to",
						"&7te current chunk you are in."))
				.build());
		inventory.setItem(12, new ItemBuilder(Material.SKULL_ITEM)
				.displayName("&b&lView Players")
				.lores(Arrays.asList(
						"&7Click to view all players",
						"&7that have explicit chunk access."))
				.build());
		inventory.setItem(14, new ItemBuilder(Material.BEACON)
				.displayName("&b&lView Factions")
				.lores(Arrays.asList(
						"&7Click to view all factions",
						"&7that have explicit chunk access."))
				.build());
		return inventory;
	}

	public static Inventory getViewPlayersMenu(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 9, FACCESS_PLAYERS_MENU_TITLE);
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lBack").lores(Collections.singletonList("&7Click to return to previous menu.")).build());
		faction.getPlayerFPermissions().keySet().forEach(name -> {
			MemoryPermissions permission = faction.getPlayerFPermissions().get(name);
			List<String> lores = new ArrayList<>();
			lores.add("");
			if (FPlayers.getInstance().getByOfflinePlayer(Bukkit.getOfflinePlayer(name)).getFaction() != null) {
				lores.add("&f&lFaction");
				lores.add("&f" + FPlayers.getInstance().getByOfflinePlayer(Bukkit.getOfflinePlayer(name)).getFaction().getTag());
				lores.add("");
			}
			lores.add("&e&lChunk Access (" + permission.getInsertedChunks().size() + ')');
			permission.getInsertedChunks().forEach(chunk -> lores.add("&e* &f" + chunk.getLeft() + "x " + chunk.getRight() + 'z'));
			lores.add("");
			lores.add("&aRight-Click &7to clear all access.");
			inventory.addItem(new ItemBuilder(Material.SKULL_ITEM)
					.displayName("&f" + name)
					.lores(lores)
					.build());
		});
		return inventory;
	}

	public static Inventory getViewFactionsMenu(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 9, FACCESS_FACTIONS_MENU_TITLE);
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lBack").lores(Collections.singletonList("&7Click to return to previous menu.")).build());
		faction.getFactionFPermissions().keySet().forEach(name -> {
			MemoryPermissions permission = faction.getFactionFPermissions().get(name);
			List<String> lores = new ArrayList<>();
			lores.add("");
			lores.add("&e&lChunk Access (" + permission.getInsertedChunks().size() + ')');
			permission.getInsertedChunks().forEach(chunk -> lores.add("&e* &f" + chunk.getLeft() + "x " + chunk.getRight() + 'z'));
			lores.add("");
			lores.add("&aRight-Click &7to clear all access.");
			inventory.addItem(new ItemBuilder(Material.ANVIL)
					.displayName(Factions.getInstance().getByTag(name).getColorTo(faction) + "&l" + name)
					.lores(lores)
					.build());
		});
		return inventory;
	}

	public static Inventory getCurrentChunkMenu(Faction faction, Chunk currentChunk) {
		Inventory inventory = Bukkit.createInventory(null, 9, FPERM_CURRENT_CHUNK_PERMS_MENU_TITLE);
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lBack").lores(Collections.singletonList("&7Click to return to previous menu.")).build());
		for (String name : faction.getFactionFPermissions().keySet()) {
			MemoryPermissions permission = faction.getFactionFPermissions().get(name);

			if (!permission.getInsertedChunks().contains(Pair.of(currentChunk.getX(), currentChunk.getZ()))) {
				continue;
			}

			inventory.addItem(new ItemBuilder(Material.STAINED_GLASS_PANE)
					.displayName("&a&lFaction: &f" + name)
					.lores(Collections.singletonList("&aClick &7to view Chunk Faction Permissions"))
					.build());
		}
		for (String name : faction.getPlayerFPermissions().keySet()) {
			MemoryPermissions permission = faction.getPlayerFPermissions().get(name);

			if (!permission.getInsertedChunks().contains(Pair.of(currentChunk.getX(), currentChunk.getZ()))) {
				continue;
			}

			inventory.addItem(new ItemBuilder(Material.SKULL_ITEM)
					.displayName("&a&lPlayer: &f" + name)
					.lores(Arrays.asList(
							"&aClick &7to view player permissions",
							"&7for this chunk"))
					.build());
		}
		return inventory;
	}

	public static Inventory getDefaultPermissionsMenu(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 9, FPERM_DEFAULT_PERMISSIONS_MENU_TITLE);
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lBack").lores(Collections.singletonList("&7Click to return to previous menu.")).build());
		for (String name : faction.getFactionDefaultFPermissions().keySet()) {

			inventory.addItem(new ItemBuilder(Material.STAINED_GLASS_PANE)
					.displayName("&a&lFaction: &f" + name)
					.lores(Collections.singletonList("&aClick &7to view Default Faction permissions."))
					.build());
		}
		for (String name : faction.getPlayerDefaultFPermissions().keySet()) {

			inventory.addItem(new ItemBuilder(Material.SKULL_ITEM)
					.displayName("&a&lPlayer: &f" + name)
					.lores(Collections.singletonList("&aClick &7to view Default Player permissions."))
					.build());
		}
		return inventory;
	}

	public static Inventory getPlayerEditPermissionMenu(MemoryPermissions permissions, String target, int intX, int intZ) {
		Inventory inventory = Bukkit.createInventory(null, 45, FPERM_EDIT_PLAYER_PERMS_MENU_TITLE.replace("PLAYER", target).replace("CORD1", intX + "").replace("CORD2", intZ + ""));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lBack").lores(Collections.singletonList("&7Click to return to previous menu.")).build());
		inventory.setItem(12, new ItemBuilder(Material.BEACON)
				.displayName("&6Full Access")
				.lores(Arrays.asList(
						"&7Access to all faction permissions.",
						"",
						"&7If this is &nALLOWED&7, no other",
						"&7permissions will apply. The",
						"&7entity will have full access to",
						"&7your chunk.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(13, new ItemBuilder(Material.CHEST)
				.displayName("&6Can Use Chests")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Chests.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanUseChests() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(14, new ItemBuilder(Material.DIAMOND_AXE)
				.displayName("&6Claim/Unclaim Land")
				.lores(Arrays.asList(
						"&7Grant the ability to claim or",
						"&7unclaim any chunks for your faction.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanModifyClaims() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(19, new ItemBuilder(Material.GRASS)
				.displayName("&6All Other Blocks")
				.lores(Arrays.asList(
						"&7Grant the ability to break and place other blocks.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractAllBlocks() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(20, new ItemBuilder(Material.SPONGE)
				.displayName("&6Can Use Sponges")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Sponges.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractSponges() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(21, new ItemBuilder(Material.DISPENSER)
				.displayName("&6Can Use Dispensers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Dispensers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDispensers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(22, new ItemBuilder(Material.DROPPER)
				.displayName("&6Can Use Droppers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Droppers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDroppers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(23, new ItemBuilder(Material.MONSTER_EGG).data(50)
				.displayName("&6Can Place Creeper Eggs")
				.lores(Arrays.asList(
						"&7Grant the ability to place Creeper Eggs.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanPlaceEggs() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(24, new ItemBuilder(Material.TNT)
				.displayName("&6Can Use TNT")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with TNT.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractTNT() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(25, new ItemBuilder(Material.OBSIDIAN)
				.displayName("&6Can Use Obsidian")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Obsidian.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractObsidian() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(28, new ItemBuilder(Material.MOB_SPAWNER)
				.displayName("&6Can Use Mob Spawners")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Mob Spawners.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractSpawners() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(29, new ItemBuilder(Material.HOPPER)
				.displayName("&6Can Use Hoppers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Hoppers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractHoppers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(30, new ItemBuilder(Material.WOOD_DOOR)
				.displayName("&6Can Use Doors & Gates")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Doors & Gates.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDoorsOrGates() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(31, new ItemBuilder(Material.WOOD_PLATE)
				.displayName("&6Can Use Pressure Plates")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Pressure Plates.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractPressurePlates() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(32, new ItemBuilder(Material.LEVER)
				.displayName("&6Can Use Levers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Levers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractLevers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(33, new ItemBuilder(Material.STONE_BUTTON)
				.displayName("&6Can Use Buttons")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Buttons.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractButtons() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(34, new ItemBuilder(Material.TRAP_DOOR)
				.displayName("&6Can Use Trap Doors")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Trap Doors.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractTrapdoors() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		return inventory;
	}

	public static Inventory getFactionEditPermissionMenu(MemoryPermissions permissions, String targetFaction, int intX, int intZ) {
		Inventory inventory = Bukkit.createInventory(null, 45, FPERM_EDIT_FACTION_PERMS_MENU_TITLE.replace("FACTION", targetFaction).replace("CORD1", intX + "").replace("CORD2", intZ + ""));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lBack").lores(Collections.singletonList("&7Click to return to previous menu.")).build());
		inventory.setItem(12, new ItemBuilder(Material.BEACON)
				.displayName("&6Full Access")
				.lores(Arrays.asList(
						"&7Access to all faction permissions.",
						"",
						"&7If this is &nALLOWED&7, no other",
						"&7permissions will apply. The",
						"&7entity will have full access to",
						"&7your chunk.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(13, new ItemBuilder(Material.CHEST)
				.displayName("&6Can Use Chests")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Chests.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanUseChests() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(14, new ItemBuilder(Material.DIAMOND_AXE)
				.displayName("&6Claim/Unclaim Land")
				.lores(Arrays.asList(
						"&7Grant the ability to claim or",
						"&7unclaim any chunks for your faction.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanModifyClaims() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(19, new ItemBuilder(Material.GRASS)
				.displayName("&6All Other Blocks")
				.lores(Arrays.asList(
						"&7Grant the ability to break and place other blocks.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractAllBlocks() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(20, new ItemBuilder(Material.SPONGE)
				.displayName("&6Can Use Sponges")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Sponges.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractSponges() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(21, new ItemBuilder(Material.DISPENSER)
				.displayName("&6Can Use Dispensers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Dispensers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDispensers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(22, new ItemBuilder(Material.DROPPER)
				.displayName("&6Can Use Droppers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Droppers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDroppers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(23, new ItemBuilder(Material.MONSTER_EGG).data(50)
				.displayName("&6Can Place Creeper Eggs")
				.lores(Arrays.asList(
						"&7Grant the ability to place Creeper Eggs.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanPlaceEggs() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(24, new ItemBuilder(Material.TNT)
				.displayName("&6Can Use TNT")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with TNT.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractTNT() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(25, new ItemBuilder(Material.OBSIDIAN)
				.displayName("&6Can Use Obsidian")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Obsidian.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractObsidian() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(28, new ItemBuilder(Material.MOB_SPAWNER)
				.displayName("&6Can Use Mob Spawners")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Mob Spawners.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractSpawners() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(29, new ItemBuilder(Material.HOPPER)
				.displayName("&6Can Use Hoppers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Hoppers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractHoppers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(30, new ItemBuilder(Material.WOOD_DOOR)
				.displayName("&6Can Use Doors & Gates")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Doors & Gates.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDoorsOrGates() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(31, new ItemBuilder(Material.WOOD_PLATE)
				.displayName("&6Can Use Pressure Plates")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Pressure Plates.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractPressurePlates() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(32, new ItemBuilder(Material.LEVER)
				.displayName("&6Can Use Levers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Levers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractLevers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(33, new ItemBuilder(Material.STONE_BUTTON)
				.displayName("&6Can Use Buttons")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Buttons.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractButtons() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(34, new ItemBuilder(Material.TRAP_DOOR)
				.displayName("&6Can Use Trap Doors")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Trap Doors.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractTrapdoors() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		return inventory;
	}

	public static Inventory getPlayerEditDefaultPermissionMenu(MemoryPermissions permissions, String target, int intX, int intZ) {
		Inventory inventory = Bukkit.createInventory(null, 45, FPERM_EDIT_DEFAULT_PLAYER_PERMS_MENU_TITLE.replace("PLAYER", target).replace("CORD1", intX + "").replace("CORD2", intZ + ""));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lBack").lores(Collections.singletonList("&7Click to return to previous menu.")).build());
		inventory.setItem(12, new ItemBuilder(Material.BEACON)
				.displayName("&6Full Access")
				.lores(Arrays.asList(
						"&7Access to all faction permissions.",
						"",
						"&7If this is &nALLOWED&7, no other",
						"&7permissions will apply. The",
						"&7entity will have full access to",
						"&7your chunk.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(13, new ItemBuilder(Material.CHEST)
				.displayName("&6Can Use Chests")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Chests.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanUseChests() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(14, new ItemBuilder(Material.DIAMOND_AXE)
				.displayName("&6Claim/Unclaim Land")
				.lores(Arrays.asList(
						"&7Grant the ability to claim or",
						"&7unclaim any chunks for your faction.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanModifyClaims() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(19, new ItemBuilder(Material.GRASS)
				.displayName("&6All Other Blocks")
				.lores(Arrays.asList(
						"&7Grant the ability to break and place other blocks.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractAllBlocks() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(20, new ItemBuilder(Material.SPONGE)
				.displayName("&6Can Use Sponges")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Sponges.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractSponges() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(21, new ItemBuilder(Material.DISPENSER)
				.displayName("&6Can Use Dispensers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Dispensers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDispensers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(22, new ItemBuilder(Material.DROPPER)
				.displayName("&6Can Use Droppers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Droppers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDroppers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(23, new ItemBuilder(Material.MONSTER_EGG).data(50)
				.displayName("&6Can Place Creeper Eggs")
				.lores(Arrays.asList(
						"&7Grant the ability to place Creeper Eggs.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanPlaceEggs() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(24, new ItemBuilder(Material.TNT)
				.displayName("&6Can Use TNT")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with TNT.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractTNT() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(25, new ItemBuilder(Material.OBSIDIAN)
				.displayName("&6Can Use Obsidian")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Obsidian.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractObsidian() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(28, new ItemBuilder(Material.MOB_SPAWNER)
				.displayName("&6Can Use Mob Spawners")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Mob Spawners.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractSpawners() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(29, new ItemBuilder(Material.HOPPER)
				.displayName("&6Can Use Hoppers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Hoppers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractHoppers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(30, new ItemBuilder(Material.WOOD_DOOR)
				.displayName("&6Can Use Doors & Gates")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Doors & Gates.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDoorsOrGates() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(31, new ItemBuilder(Material.WOOD_PLATE)
				.displayName("&6Can Use Pressure Plates")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Pressure Plates.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractPressurePlates() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(32, new ItemBuilder(Material.LEVER)
				.displayName("&6Can Use Levers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Levers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractLevers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(33, new ItemBuilder(Material.STONE_BUTTON)
				.displayName("&6Can Use Buttons")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Buttons.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractButtons() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(34, new ItemBuilder(Material.TRAP_DOOR)
				.displayName("&6Can Use Trap Doors")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Trap Doors.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractTrapdoors() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		return inventory;
	}

	public static Inventory getFactionEditDefaultPermissionMenu(MemoryPermissions permissions, String targetFaction, int intX, int intZ) {
		Inventory inventory = Bukkit.createInventory(null, 45, FPERM_EDIT_DEFAULT_FACTION_PERMS_MENU_TITLE.replace("FACTION", targetFaction).replace("CORD1", intX + "").replace("CORD2", intZ + ""));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lBack").lores(Collections.singletonList("&7Click to return to previous menu.")).build());
		inventory.setItem(12, new ItemBuilder(Material.BEACON)
				.displayName("&6Full Access")
				.lores(Arrays.asList(
						"&7Access to all faction permissions.",
						"",
						"&7If this is &nALLOWED&7, no other",
						"&7permissions will apply. The",
						"&7entity will have full access to",
						"&7your chunk.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(13, new ItemBuilder(Material.CHEST)
				.displayName("&6Can Use Chests")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Chests.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanUseChests() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(14, new ItemBuilder(Material.DIAMOND_AXE)
				.displayName("&6Claim/Unclaim Land")
				.lores(Arrays.asList(
						"&7Grant the ability to claim or",
						"&7unclaim any chunks for your faction.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanModifyClaims() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(19, new ItemBuilder(Material.GRASS)
				.displayName("&6All Other Blocks")
				.lores(Arrays.asList(
						"&7Grant the ability to break and place other blocks.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractAllBlocks() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(20, new ItemBuilder(Material.SPONGE)
				.displayName("&6Can Use Sponges")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Sponges.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractSponges() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(21, new ItemBuilder(Material.DISPENSER)
				.displayName("&6Can Use Dispensers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Dispensers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDispensers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(22, new ItemBuilder(Material.DROPPER)
				.displayName("&6Can Use Droppers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Droppers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDroppers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(23, new ItemBuilder(Material.MONSTER_EGG).data(50)
				.displayName("&6Can Place Creeper Eggs")
				.lores(Arrays.asList(
						"&7Grant the ability to place Creeper Eggs.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanPlaceEggs() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(24, new ItemBuilder(Material.TNT)
				.displayName("&6Can Use TNT")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with TNT.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractTNT() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(25, new ItemBuilder(Material.OBSIDIAN)
				.displayName("&6Can Use Obsidian")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Obsidian.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractObsidian() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(28, new ItemBuilder(Material.MOB_SPAWNER)
				.displayName("&6Can Use Mob Spawners")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Mob Spawners.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractSpawners() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(29, new ItemBuilder(Material.HOPPER)
				.displayName("&6Can Use Hoppers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Hoppers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractHoppers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(30, new ItemBuilder(Material.WOOD_DOOR)
				.displayName("&6Can Use Doors & Gates")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Doors & Gates.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractDoorsOrGates() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(31, new ItemBuilder(Material.WOOD_PLATE)
				.displayName("&6Can Use Pressure Plates")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Pressure Plates.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractPressurePlates() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(32, new ItemBuilder(Material.LEVER)
				.displayName("&6Can Use Levers")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Levers.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractLevers() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(33, new ItemBuilder(Material.STONE_BUTTON)
				.displayName("&6Can Use Buttons")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Buttons.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractButtons() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		inventory.setItem(34, new ItemBuilder(Material.TRAP_DOOR)
				.displayName("&6Can Use Trap Doors")
				.lores(Arrays.asList(
						"&7Grant the ability to place, break",
						"&7and interact with Trap Doors.",
						"",
						"&6&lPermission",
						permissions.isFullAccess() ? "&a&lOVERRIDEN &7(Full Access)" : permissions.isCanInteractTrapdoors() ? "&a&lALLOWED" : "&c&lNOT ALLOWED"))
				.build());
		return inventory;
	}

	public static Inventory getFRolesInventory(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 18, FROLES_TITLE);
		MemoryTypeRoles recruitRoles = faction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false));
		MemoryTypeRoles memberRoles = faction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false));
		MemoryTypeRoles moderatorRoles = faction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false));
		MemoryTypeRoles coleaderRoles = faction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true));
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(0)
				.displayName("&f&lRecruit")
				.lores(Arrays.asList(
						"",
						"&6&lPermissions:",
						" &bInvite Members - " + (recruitRoles.isCanInviteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bKick Members - " + (recruitRoles.isCanKickMembers() ? "&a&lYES" : "&c&lNO"),
						" &bDemote Members - " + (recruitRoles.isCanDemoteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bPromote Members - " + (recruitRoles.isCanPromoteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Relations - " + (recruitRoles.isCanEditRelations() ? "&a&lYES" : "&c&lNO"),
						" &b/f sethome - " + (recruitRoles.isCanSetFHome() ? "&a&lYES" : "&c&lNO"),
						" &b/f setwarp - " + (recruitRoles.isCanSetFWarp() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Faction Tag - " + (recruitRoles.isCanEditTag() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Member Titles - " + (recruitRoles.isCanEditTitles() ? "&a&lYES" : "&c&lNO"),
						" &bUnclaim All - " + (recruitRoles.isCanUnclaimAll() ? "&a&lYES" : "&c&lNO"),
						" &bEdit /f perms - " + (recruitRoles.isCanEditFPerms() ? "&a&lYES" : "&c&lNO")))
				.build());
		inventory.setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(5)
				.displayName("&a&lMember")
				.lores(Arrays.asList(
						"",
						"&6&lPermissions:",
						" &bInvite Members - " + (memberRoles.isCanInviteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bKick Members - " + (memberRoles.isCanKickMembers() ? "&a&lYES" : "&c&lNO"),
						" &bDemote Members - " + (memberRoles.isCanDemoteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bPromote Members - " + (memberRoles.isCanPromoteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Relations - " + (memberRoles.isCanEditRelations() ? "&a&lYES" : "&c&lNO"),
						" &b/f sethome - " + (memberRoles.isCanSetFHome() ? "&a&lYES" : "&c&lNO"),
						" &b/f setwarp - " + (memberRoles.isCanSetFWarp() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Faction Tag - " + (memberRoles.isCanEditTag() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Member Titles - " + (memberRoles.isCanEditTitles() ? "&a&lYES" : "&c&lNO"),
						" &bUnclaim All - " + (memberRoles.isCanUnclaimAll() ? "&a&lYES" : "&c&lNO"),
						" &bEdit /f perms - " + (memberRoles.isCanEditFPerms() ? "&a&lYES" : "&c&lNO")))
				.build());
		inventory.setItem(5, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(3)
				.displayName("&b&lModerator")
				.lores(Arrays.asList(
						"",
						"&6&lPermissions:",
						" &bInvite Members - " + (moderatorRoles.isCanInviteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bKick Members - " + (moderatorRoles.isCanKickMembers() ? "&a&lYES" : "&c&lNO"),
						" &bDemote Members - " + (moderatorRoles.isCanDemoteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bPromote Members - " + (moderatorRoles.isCanPromoteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Relations - " + (moderatorRoles.isCanEditRelations() ? "&a&lYES" : "&c&lNO"),
						" &b/f sethome - " + (moderatorRoles.isCanSetFHome() ? "&a&lYES" : "&c&lNO"),
						" &b/f setwarp - " + (moderatorRoles.isCanSetFWarp() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Faction Tag - " + (moderatorRoles.isCanEditTag() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Member Titles - " + (moderatorRoles.isCanEditTitles() ? "&a&lYES" : "&c&lNO"),
						" &bUnclaim All - " + (moderatorRoles.isCanUnclaimAll() ? "&a&lYES" : "&c&lNO"),
						" &bEdit /f perms - " + (moderatorRoles.isCanEditFPerms() ? "&a&lYES" : "&c&lNO")))
				.build());
		inventory.setItem(7, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(4)
				.displayName("&e&lCo-Leader")
				.lores(Arrays.asList(
						"",
						"&6&lPermissions:",
						" &bInvite Members - " + (coleaderRoles.isCanInviteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bKick Members - " + (coleaderRoles.isCanKickMembers() ? "&a&lYES" : "&c&lNO"),
						" &bDemote Members - " + (coleaderRoles.isCanDemoteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bPromote Members - " + (coleaderRoles.isCanPromoteMembers() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Relations - " + (coleaderRoles.isCanEditRelations() ? "&a&lYES" : "&c&lNO"),
						" &b/f sethome - " + (coleaderRoles.isCanSetFHome() ? "&a&lYES" : "&c&lNO"),
						" &b/f setwarp - " + (coleaderRoles.isCanSetFWarp() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Faction Tag - " + (coleaderRoles.isCanEditTag() ? "&a&lYES" : "&c&lNO"),
						" &bEdit Member Titles - " + (coleaderRoles.isCanEditTitles() ? "&a&lYES" : "&c&lNO"),
						" &bUnclaim All - " + (coleaderRoles.isCanUnclaimAll() ? "&a&lYES" : "&c&lNO"),
						" &bEdit /f perms - " + (coleaderRoles.isCanEditFPerms() ? "&a&lYES" : "&c&lNO")))
				.build());
		inventory.setItem(13, new ItemBuilder(Material.BOOK_AND_QUILL)
				.displayName("&b&lPlayer Role Permissions")
				.lores(Arrays.asList(
						"&7Click to view faction members",
						"&7and their role permissions."))
				.build());
		return inventory;
	}

	public static Inventory getRecruitRolePermissions(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 18, FROLES_RECRUIT_TITLE);
		MemoryTypeRoles recruitRole = faction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanInviteMembers() ? 5 : 14)
				.displayName(recruitRole.isCanInviteMembers() ? "&a&lInvite Members" : "&c&lInvite Members")
				.lores(Arrays.asList(
						"&7Grants the ability to invite",
						"&7players to your faction.",
						recruitRole.isCanInviteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanKickMembers() ? 5 : 14)
				.displayName(recruitRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						recruitRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanKickMembers() ? 5 : 14)
				.displayName(recruitRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						recruitRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanDemoteMembers() ? 5 : 14)
				.displayName(recruitRole.isCanDemoteMembers() ? "&a&lDemote Members" : "&c&lDemote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to demote",
						"&7players from your faction.",
						recruitRole.isCanDemoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanPromoteMembers() ? 5 : 14)
				.displayName(recruitRole.isCanPromoteMembers() ? "&a&lPromote Members" : "&c&lPromote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to promote",
						"&7players from your faction.",
						recruitRole.isCanPromoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanEditRelations() ? 5 : 14)
				.displayName(recruitRole.isCanEditRelations() ? "&a&lEdit Relations" : "&c&lEdit Relations")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7relations with other factions.",
						recruitRole.isCanEditRelations() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(5, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanSetFHome() ? 5 : 14)
				.displayName(recruitRole.isCanSetFHome() ? "&a&l/f sethome" : "&c&l/f sethome")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction home.",
						recruitRole.isCanSetFHome() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(6, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanSetFWarp() ? 5 : 14)
				.displayName(recruitRole.isCanSetFWarp() ? "&a&l/f setwarp" : "&c&l/f setwarp")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction warps.",
						recruitRole.isCanSetFWarp() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(7, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanEditTag() ? 5 : 14)
				.displayName(recruitRole.isCanEditTag() ? "&a&lEdit Faction Tag" : "&c&lEdit Faction Tag")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7the name of your faction.",
						recruitRole.isCanEditTag() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(8, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanEditTitles() ? 5 : 14)
				.displayName(recruitRole.isCanEditTitles() ? "&a&lEdit Member Titles" : "&c&lEdit Member Titles")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7player's title from your faction.",
						recruitRole.isCanEditTitles() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(9, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanUnclaimAll() ? 5 : 14)
				.displayName(recruitRole.isCanUnclaimAll() ? "&a&lUnclaim All" : "&c&lUnclaim All")
				.lores(Arrays.asList(
						"&7Grants the ability to unclaim",
						"&7all land of your faction.",
						recruitRole.isCanUnclaimAll() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(10, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(recruitRole.isCanEditFPerms() ? 5 : 14)
				.displayName(recruitRole.isCanEditFPerms() ? "&a&lEdit /f perms" : "&c&lEdit /f perms")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7the faction permissions.",
						recruitRole.isCanEditFPerms() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(17, new ItemBuilder(Material.ARROW)
				.displayName("&c&lPrevious Menu")
				.lores(Arrays.asList("&7Return to the previous menu"))
				.build());
		return inventory;
	}

	public static Inventory getMemberRolePermissions(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 18, FROLES_MEMBER_TITLE);
		MemoryTypeRoles memberRole = faction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanInviteMembers() ? 5 : 14)
				.displayName(memberRole.isCanInviteMembers() ? "&a&lInvite Members" : "&c&lInvite Members")
				.lores(Arrays.asList(
						"&7Grants the ability to invite",
						"&7players to your faction.",
						memberRole.isCanInviteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanKickMembers() ? 5 : 14)
				.displayName(memberRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						memberRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanKickMembers() ? 5 : 14)
				.displayName(memberRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						memberRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanDemoteMembers() ? 5 : 14)
				.displayName(memberRole.isCanDemoteMembers() ? "&a&lDemote Members" : "&c&lDemote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to demote",
						"&7players from your faction.",
						memberRole.isCanDemoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanPromoteMembers() ? 5 : 14)
				.displayName(memberRole.isCanPromoteMembers() ? "&a&lPromote Members" : "&c&lPromote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to promote",
						"&7players from your faction.",
						memberRole.isCanPromoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanEditRelations() ? 5 : 14)
				.displayName(memberRole.isCanEditRelations() ? "&a&lEdit Relations" : "&c&lEdit Relations")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7relations with other factions.",
						memberRole.isCanEditRelations() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(5, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanSetFHome() ? 5 : 14)
				.displayName(memberRole.isCanSetFHome() ? "&a&l/f sethome" : "&c&l/f sethome")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction home.",
						memberRole.isCanSetFHome() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(6, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanSetFWarp() ? 5 : 14)
				.displayName(memberRole.isCanSetFWarp() ? "&a&l/f setwarp" : "&c&l/f setwarp")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction warps.",
						memberRole.isCanSetFWarp() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(7, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanEditTag() ? 5 : 14)
				.displayName(memberRole.isCanEditTag() ? "&a&lEdit Faction Tag" : "&c&lEdit Faction Tag")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7the name of your faction.",
						memberRole.isCanEditTag() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(8, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanEditTitles() ? 5 : 14)
				.displayName(memberRole.isCanEditTitles() ? "&a&lEdit Member Titles" : "&c&lEdit Member Titles")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7player's title from your faction.",
						memberRole.isCanEditTitles() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(9, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanUnclaimAll() ? 5 : 14)
				.displayName(memberRole.isCanUnclaimAll() ? "&a&lUnclaim All" : "&c&lUnclaim All")
				.lores(Arrays.asList(
						"&7Grants the ability to unclaim",
						"&7all land of your faction.",
						memberRole.isCanUnclaimAll() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(10, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(memberRole.isCanEditFPerms() ? 5 : 14)
				.displayName(memberRole.isCanEditFPerms() ? "&a&lEdit /f perms" : "&c&lEdit /f perms")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7the faction permissions.",
						memberRole.isCanEditFPerms() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(17, new ItemBuilder(Material.ARROW)
				.displayName("&c&lPrevious Menu")
				.lores(Arrays.asList("&7Return to the previous menu"))
				.build());
		return inventory;
	}

	public static Inventory getModeratorRolePermissions(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 18, FROLES_MODERATOR_TITLE);
		MemoryTypeRoles moderatorRole = faction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanInviteMembers() ? 5 : 14)
				.displayName(moderatorRole.isCanInviteMembers() ? "&a&lInvite Members" : "&c&lInvite Members")
				.lores(Arrays.asList(
						"&7Grants the ability to invite",
						"&7players to your faction.",
						moderatorRole.isCanInviteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanKickMembers() ? 5 : 14)
				.displayName(moderatorRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						moderatorRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanKickMembers() ? 5 : 14)
				.displayName(moderatorRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						moderatorRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanDemoteMembers() ? 5 : 14)
				.displayName(moderatorRole.isCanDemoteMembers() ? "&a&lDemote Members" : "&c&lDemote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to demote",
						"&7players from your faction.",
						moderatorRole.isCanDemoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanPromoteMembers() ? 5 : 14)
				.displayName(moderatorRole.isCanPromoteMembers() ? "&a&lPromote Members" : "&c&lPromote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to promote",
						"&7players from your faction.",
						moderatorRole.isCanPromoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanEditRelations() ? 5 : 14)
				.displayName(moderatorRole.isCanEditRelations() ? "&a&lEdit Relations" : "&c&lEdit Relations")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7relations with other factions.",
						moderatorRole.isCanEditRelations() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(5, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanSetFHome() ? 5 : 14)
				.displayName(moderatorRole.isCanSetFHome() ? "&a&l/f sethome" : "&c&l/f sethome")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction home.",
						moderatorRole.isCanSetFHome() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(6, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanSetFWarp() ? 5 : 14)
				.displayName(moderatorRole.isCanSetFWarp() ? "&a&l/f setwarp" : "&c&l/f setwarp")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction warps.",
						moderatorRole.isCanSetFWarp() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(7, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanEditTag() ? 5 : 14)
				.displayName(moderatorRole.isCanEditTag() ? "&a&lEdit Faction Tag" : "&c&lEdit Faction Tag")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7the name of your faction.",
						moderatorRole.isCanEditTag() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(8, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanEditTitles() ? 5 : 14)
				.displayName(moderatorRole.isCanEditTitles() ? "&a&lEdit Member Titles" : "&c&lEdit Member Titles")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7player's title from your faction.",
						moderatorRole.isCanEditTitles() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(9, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanUnclaimAll() ? 5 : 14)
				.displayName(moderatorRole.isCanUnclaimAll() ? "&a&lUnclaim All" : "&c&lUnclaim All")
				.lores(Arrays.asList(
						"&7Grants the ability to unclaim",
						"&7all land of your faction.",
						moderatorRole.isCanUnclaimAll() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(10, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(moderatorRole.isCanEditFPerms() ? 5 : 14)
				.displayName(moderatorRole.isCanEditFPerms() ? "&a&lEdit /f perms" : "&c&lEdit /f perms")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7the faction permissions.",
						moderatorRole.isCanEditFPerms() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(17, new ItemBuilder(Material.ARROW)
				.displayName("&c&lPrevious Menu")
				.lores(Arrays.asList("&7Return to the previous menu"))
				.build());
		return inventory;
	}

	public static Inventory getCoLeaderRolePermissions(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 18, FROLES_COLEADER_TITLE);
		MemoryTypeRoles coLeaderRole = faction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanInviteMembers() ? 5 : 14)
				.displayName(coLeaderRole.isCanInviteMembers() ? "&a&lInvite Members" : "&c&lInvite Members")
				.lores(Arrays.asList(
						"&7Grants the ability to invite",
						"&7players to your faction.",
						coLeaderRole.isCanInviteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanKickMembers() ? 5 : 14)
				.displayName(coLeaderRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						coLeaderRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanKickMembers() ? 5 : 14)
				.displayName(coLeaderRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						coLeaderRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanDemoteMembers() ? 5 : 14)
				.displayName(coLeaderRole.isCanDemoteMembers() ? "&a&lDemote Members" : "&c&lDemote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to demote",
						"&7players from your faction.",
						coLeaderRole.isCanDemoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanPromoteMembers() ? 5 : 14)
				.displayName(coLeaderRole.isCanPromoteMembers() ? "&a&lPromote Members" : "&c&lPromote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to promote",
						"&7players from your faction.",
						coLeaderRole.isCanPromoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanEditRelations() ? 5 : 14)
				.displayName(coLeaderRole.isCanEditRelations() ? "&a&lEdit Relations" : "&c&lEdit Relations")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7relations with other factions.",
						coLeaderRole.isCanEditRelations() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(5, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanSetFHome() ? 5 : 14)
				.displayName(coLeaderRole.isCanSetFHome() ? "&a&l/f sethome" : "&c&l/f sethome")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction home.",
						coLeaderRole.isCanSetFHome() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(6, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanSetFWarp() ? 5 : 14)
				.displayName(coLeaderRole.isCanSetFWarp() ? "&a&l/f setwarp" : "&c&l/f setwarp")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction warps.",
						coLeaderRole.isCanSetFWarp() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(7, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanEditTag() ? 5 : 14)
				.displayName(coLeaderRole.isCanEditTag() ? "&a&lEdit Faction Tag" : "&c&lEdit Faction Tag")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7the name of your faction.",
						coLeaderRole.isCanEditTag() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(8, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanEditTitles() ? 5 : 14)
				.displayName(coLeaderRole.isCanEditTitles() ? "&a&lEdit Member Titles" : "&c&lEdit Member Titles")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7player's title from your faction.",
						coLeaderRole.isCanEditTitles() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(9, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanUnclaimAll() ? 5 : 14)
				.displayName(coLeaderRole.isCanUnclaimAll() ? "&a&lUnclaim All" : "&c&lUnclaim All")
				.lores(Arrays.asList(
						"&7Grants the ability to unclaim",
						"&7all land of your faction.",
						coLeaderRole.isCanUnclaimAll() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(10, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(coLeaderRole.isCanEditFPerms() ? 5 : 14)
				.displayName(coLeaderRole.isCanEditFPerms() ? "&a&lEdit /f perms" : "&c&lEdit /f perms")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7the faction permissions.",
						coLeaderRole.isCanEditFPerms() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(17, new ItemBuilder(Material.ARROW)
				.displayName("&c&lPrevious Menu")
				.lores(Arrays.asList("&7Return to the previous menu"))
				.build());
		return inventory;
	}

	public static Inventory getPlayerRolePermissions(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 18, FROLES_MEMBERS_TITLE);
		faction.getFPlayers().stream().filter(fplayer -> fplayer.getRole() != Role.ADMIN).forEach(fplayer -> {
			MemoryRoles playerRole = faction.getPlayerRoles().getOrDefault(fplayer.getName(), new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false));
			inventory.addItem(new ItemBuilder(Material.SKULL_ITEM)
					.displayName("&f" + fplayer.getName())
					.lores(Arrays.asList(
							"",
							"&6&lRole:",
							" &f&l" + fplayer.getRole().name(),
							"",
							"&6&lPermissions:",
							" &bInvite Members - " + (playerRole.isCanInviteMembers() ? "&a&lYES" : "&c&lNO"),
							" &bKick Members - " + (playerRole.isCanKickMembers() ? "&a&lYES" : "&c&lNO"),
							" &bDemote Members - " + (playerRole.isCanDemoteMembers() ? "&a&lYES" : "&c&lNO"),
							" &bPromote Members - " + (playerRole.isCanPromoteMembers() ? "&a&lYES" : "&c&lNO"),
							" &bEdit Relations - " + (playerRole.isCanEditRelations() ? "&a&lYES" : "&c&lNO"),
							" &b/f sethome - " + (playerRole.isCanSetFHome() ? "&a&lYES" : "&c&lNO"),
							" &b/f setwarp - " + (playerRole.isCanSetFWarp() ? "&a&lYES" : "&c&lNO"),
							" &bEdit Faction Tag - " + (playerRole.isCanEditTag() ? "&a&lYES" : "&c&lNO"),
							" &bEdit Member Titles - " + (playerRole.isCanEditTitles() ? "&a&lYES" : "&c&lNO"),
							" &bUnclaim All - " + (playerRole.isCanUnclaimAll() ? "&a&lYES" : "&c&lNO"),
							" &bEdit /f perms - " + (playerRole.isCanEditFPerms() ? "&a&lYES" : "&c&lNO"),
							"",
							"&7Click to edit player role permissions"))
					.build());
		});
		return inventory;
	}

	public static Inventory getFPlayerRolePermission(String playerName, Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 18, FROLES_FPLAYER_TITLE.replace("FPLAYER", playerName));
		MemoryRoles playerRole = faction.getPlayerRoles().getOrDefault(playerName, new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false));
		inventory.setItem(0, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanInviteMembers() ? 5 : 14)
				.displayName(playerRole.isCanInviteMembers() ? "&a&lInvite Members" : "&c&lInvite Members")
				.lores(Arrays.asList(
						"&7Grants the ability to invite",
						"&7players to your faction.",
						playerRole.isCanInviteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanKickMembers() ? 5 : 14)
				.displayName(playerRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						playerRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(1, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanKickMembers() ? 5 : 14)
				.displayName(playerRole.isCanKickMembers() ? "&a&lKick Members" : "&c&lKick Members")
				.lores(Arrays.asList(
						"&7Grants the ability to kick",
						"&7players from your faction.",
						playerRole.isCanKickMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(2, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanDemoteMembers() ? 5 : 14)
				.displayName(playerRole.isCanDemoteMembers() ? "&a&lDemote Members" : "&c&lDemote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to demote",
						"&7players from your faction.",
						playerRole.isCanDemoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(3, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanPromoteMembers() ? 5 : 14)
				.displayName(playerRole.isCanPromoteMembers() ? "&a&lPromote Members" : "&c&lPromote Members")
				.lores(Arrays.asList(
						"&7Grants the ability to promote",
						"&7players from your faction.",
						playerRole.isCanPromoteMembers() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanEditRelations() ? 5 : 14)
				.displayName(playerRole.isCanEditRelations() ? "&a&lEdit Relations" : "&c&lEdit Relations")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7relations with other factions.",
						playerRole.isCanEditRelations() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(5, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanSetFHome() ? 5 : 14)
				.displayName(playerRole.isCanSetFHome() ? "&a&l/f sethome" : "&c&l/f sethome")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction home.",
						playerRole.isCanSetFHome() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(6, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanSetFWarp() ? 5 : 14)
				.displayName(playerRole.isCanSetFWarp() ? "&a&l/f setwarp" : "&c&l/f setwarp")
				.lores(Arrays.asList(
						"&7Grants the ability to set",
						"&7the faction warps.",
						playerRole.isCanSetFWarp() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(7, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanEditTag() ? 5 : 14)
				.displayName(playerRole.isCanEditTag() ? "&a&lEdit Faction Tag" : "&c&lEdit Faction Tag")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7the name of your faction.",
						playerRole.isCanEditTag() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(8, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanEditTitles() ? 5 : 14)
				.displayName(playerRole.isCanEditTitles() ? "&a&lEdit Member Titles" : "&c&lEdit Member Titles")
				.lores(Arrays.asList(
						"&7Grants the ability to change",
						"&7player's title from your faction.",
						playerRole.isCanEditTitles() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(9, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanUnclaimAll() ? 5 : 14)
				.displayName(playerRole.isCanUnclaimAll() ? "&a&lUnclaim All" : "&c&lUnclaim All")
				.lores(Arrays.asList(
						"&7Grants the ability to unclaim",
						"&7all land of your faction.",
						playerRole.isCanUnclaimAll() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(10, new ItemBuilder(Material.STAINED_GLASS_PANE)
				.data(playerRole.isCanEditFPerms() ? 5 : 14)
				.displayName(playerRole.isCanEditFPerms() ? "&a&lEdit /f perms" : "&c&lEdit /f perms")
				.lores(Arrays.asList(
						"&7Grants the ability to edit",
						"&7the faction permissions.",
						playerRole.isCanEditFPerms() ? "&a&lALLOWED" : "&c&lDENIED"))
				.build());
		inventory.setItem(17, new ItemBuilder(Material.ARROW)
				.displayName("&c&lPrevious Menu")
				.lores(Arrays.asList("&7Return to the previous menu"))
				.build());
		return inventory;
	}

	public static Inventory getDisbandInventory(Faction faction) {
		Inventory inventory = Bukkit.createInventory(null, 9, FACTION_DISBAND_TITLE);
		for (int i = 0; i < 4; i++) {
			inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).data(5).displayName("&a&lConfirm").lores(Arrays.asList("", "&7Confirm disbanding of &a&l" + faction.getTag() + "&7!")).build());
		}
		inventory.setItem(4, new ItemBuilder(Material.STAINED_GLASS_PANE).data(15).displayName("&7" + faction.getTag()).build());
		for (int i = 5; i < 9; i++) {
			inventory.setItem(i, new ItemBuilder(Material.STAINED_GLASS_PANE).data(14).displayName("&c&lCancel").lores(Arrays.asList("", "&7Click to cancel disbanding of &c&l" + faction.getTag() + "&7!")).build());
		}
		return inventory;
	}

    public static String InventoryToString(ItemStack[] items) throws IllegalStateException {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);
            dataOutput.writeInt(items.length);
            for (ItemStack item : items) {
                dataOutput.writeObject(item);
            }
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Unable to save item stacks.", e);
        }
    }

    public static ItemStack[] StringToInventory(String data) throws IOException {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            ItemStack[] items = new ItemStack[dataInput.readInt()];
            for (int i = 0; i < items.length; i++) {
                items[i] = (ItemStack) dataInput.readObject();
            }
            dataInput.close();
            return items;
        } catch (ClassNotFoundException e) {
            throw new IOException("Unable to decode class type.", e);
        }
    }

    public static String toBase64(Inventory inventory) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            BukkitObjectOutputStream dataOutput = new BukkitObjectOutputStream(outputStream);

            // Write the size of the inventory
            dataOutput.writeInt(inventory.getSize());

            // Save every element in the list
            for (int i = 0; i < inventory.getSize(); i++) {
                dataOutput.writeObject(inventory.getItem(i));
            }

            // Serialize that array
            dataOutput.close();
            return Base64Coder.encodeLines(outputStream.toByteArray());
        } catch (Exception e) {
            throw new IllegalStateException("Cannot into itemstacksz!", e);
        }
    }

    public static String toBase64(ItemStack[] is, int size) {
        Inventory inventory = Bukkit.createInventory(null, size);
        inventory.setContents(is);
        return toBase64(inventory);
    }

    public static Inventory fromBase64(String data) {
        try {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64Coder.decodeLines(data));
            BukkitObjectInputStream dataInput = new BukkitObjectInputStream(inputStream);
            Inventory inventory = Bukkit.getServer().createInventory(null, dataInput.readInt());

            // Read the serialized inventory
            for (int i = 0; i < inventory.getSize(); i++) {
                inventory.setItem(i, (ItemStack) dataInput.readObject());
            }
            dataInput.close();
            return inventory;
        } catch (Exception e) {
        }
        return null;
    }

}
