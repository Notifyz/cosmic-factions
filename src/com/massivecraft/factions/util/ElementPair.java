package com.massivecraft.factions.util;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;

import com.google.common.collect.ImmutableMap;

public class ElementPair<T> implements ConfigurationSerializable, Map.Entry<T, T> {

	private T[] elements;

	public ElementPair() {
		Class<T> componentType = null;
		for (Field field: getClass().getDeclaredFields()) {
			if (field.getName().equals("elements")) {
				componentType = (Class<T>) field.getType().getComponentType();
			}
		}
		if (componentType == null) {
			throw new IllegalStateException("unable to obtain component type");
		}
		this.elements = (T[]) Array.newInstance(componentType, 2);
	}

	public ElementPair(T first, T second) {
		this();
		elements[0] = first;
		elements[1] = second;
	}

	public ElementPair(Map<String, Object> properties) {
		this();
		elements[0] = (T) properties.get("left");
		elements[1] = (T) properties.get("right");
	}

	public T left() {
		return getKey();
	}

	public T right() {
		return getValue();
	}

	@Override
	public Map<String, Object> serialize() {
		return ImmutableMap.<String, Object>builder().put("left", elements[0]).put("right", elements[1]).build();
	}

	@Override
	public T getKey() {
		return elements[0];
	}

	@Override
	public T getValue() {
		return elements[1];
	}

	@Override
	public T setValue(T value) {
		return elements[1] = value;
	}

	static {
		ConfigurationSerialization.registerClass(ElementPair.class, "pair");
	}

}
