package com.massivecraft.factions.game.argument;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.P;
import com.massivecraft.factions.argument.FactionArgument;
import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.game.type.ConquestEvent;

public class GameSetEquipmentArgument extends FactionArgument {

	private final P plugin;

	public GameSetEquipmentArgument(P plugin) {
		super("setequipment", "Set equipment for bosses", "faction.command.game.setequipment");

		this.plugin = plugin;
	}
	@Override
	public String getUsage(String label) {
		return '/' + label + ' ' + getName() + " <eventName> <bossArgument>";
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
	       if (args.length < 3) {
	            sender.sendMessage(ChatColor.RED + "Usage: " + getUsage(label));
	            return true;
	        }

	       	Player player = (Player) sender;
	        Event event = plugin.getEventManager().getEvent(args[1]);

	        if (event == null) {
	            sender.sendMessage(ChatColor.RED + "There is not an event named '" + args[1] + "'.");
	            return true;
	        }

	        if (!(event instanceof ConquestEvent)) {
	            sender.sendMessage(ChatColor.RED + "There is not an Conquest event named '" + args[1] + "'.");
	        	return true;
	        }

	        ConquestEvent conquestEvent = (ConquestEvent) event;

	        switch (args[2].toUpperCase()) {
	        case "EXTERMINATORARMOR":
	        	conquestEvent.setExterminatorArmor(player.getInventory().getArmorContents());
	        	player.sendMessage(ChatColor.GREEN + "Successfully set exterminator's armor.");
	        	break;
	        case "EXTERMINATORWEAPON":
	        	conquestEvent.setExterminatorWeapon(player.getItemInHand());
	        	player.sendMessage(ChatColor.GREEN + "Successfully set exterminator's weapon.");
	        	break;
	        case "ENFORCERARMOR":
	        	conquestEvent.setEnforcerArmor(player.getInventory().getArmorContents());
	        	player.sendMessage(ChatColor.GREEN + "Successfully set enforcer's armor.");
	        	break;
	        case "ENFORCERWEAPON":
	        	conquestEvent.setEnforcerWeapon(player.getItemInHand());
	        	player.sendMessage(ChatColor.GREEN + "Successfully set enforcer's weapon.");
	        	break;
	        case "MARKSMANARMOR":
	        	conquestEvent.setMarksmanArmor(player.getInventory().getArmorContents());
	        	player.sendMessage(ChatColor.GREEN + "Successfully set enforcer's armor.");
	        	break;
	        case "MARKSMANWEAPON":
	        	conquestEvent.setMarksmanWeapon(player.getItemInHand());
	        	player.sendMessage(ChatColor.GREEN + "Successfully set enforcer's weapon.");
	        	break;
	        default:
	        	break;
	        }
	        return true;
	    }

}
