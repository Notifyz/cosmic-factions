package com.massivecraft.factions.game.argument;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.massivecraft.factions.P;
import com.massivecraft.factions.argument.FactionArgument;
import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.game.type.ConquestEvent;
import com.massivecraft.factions.util.RewardableItemStack;

public class GameAddItemArgument extends FactionArgument {

	private final P plugin;

	public GameAddItemArgument(P plugin) {
		super("additem", "Add loot item from held slot", "faction.command.game.additem");

		this.plugin = plugin;
	}
	@Override
	public String getUsage(String label) {
		return '/' + label + ' ' + getName() + " <eventName> <chance>";
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
	       if (args.length < 3) {
	            sender.sendMessage(ChatColor.RED + "Usage: " + getUsage(label));
	            return true;
	        }

	        Event event = plugin.getEventManager().getEvent(args[1]);

	        if (event == null) {
	            sender.sendMessage(ChatColor.RED + "There is not an event named '" + args[1] + "'.");
	            return true;
	        }

	        if (!(event instanceof ConquestEvent)) {
	            sender.sendMessage(ChatColor.RED + "There is not an Conquest event named '" + args[1] + "'.");
	        	return true;
	        }

	        ConquestEvent conquestEvent = (ConquestEvent) event;

	        int chance = Integer.parseInt(args[2]);

	        if (chance == -1) {
	        	sender.sendMessage(ChatColor.RED + "Invalid chance, please use numbers.");
	        	return true;
	        }

	        ItemStack heldItem = ((Player) sender).getItemInHand();

	        if (heldItem == null || heldItem.getType() == Material.AIR) {
	        	sender.sendMessage(ChatColor.RED + "Invalid item, please hold valid items.");
	        	return true;
	        }

	        conquestEvent.addItem(new RewardableItemStack(heldItem, chance, heldItem.getAmount() <= 0 ? 1 : heldItem.getAmount()));
	        sender.sendMessage(ChatColor.YELLOW + "Successfully added item to the " + event.getName() + " event.");
	        return true;
	    }

}
