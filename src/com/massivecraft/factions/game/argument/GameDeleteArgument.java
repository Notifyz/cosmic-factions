package com.massivecraft.factions.game.argument;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.massivecraft.factions.P;
import com.massivecraft.factions.argument.FactionArgument;
import com.massivecraft.factions.game.Event;

public class GameDeleteArgument extends FactionArgument{

	private final P plugin;

	public GameDeleteArgument(P plugin) {
		super("delete", "Delete the event", "faction.command.game.delete", "remove");

		this.plugin = plugin;
	}

	@Override
	public String getUsage(String label) {
		return '/' + label + ' ' + getName() + " <eventName>";
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Usage: " + getUsage(label));
            return true;
        }

        Event event = plugin.getEventManager().getEvent(args[1]);

        if (!(event instanceof Event)) {
            sender.sendMessage(ChatColor.RED + "There is not an event named '" + args[1] + "'.");
            return true;
        }

        if (plugin.getEventManager().removeEvent(event, sender)) {
            sender.sendMessage(ChatColor.YELLOW + "Deleted event " + ChatColor.WHITE + event.getName() + ChatColor.YELLOW + '.');
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 2) {
            return Collections.emptyList();
        }
        return plugin.getEventManager().getEvents().stream().map(Event::getName).collect(Collectors.toList());
    }

}