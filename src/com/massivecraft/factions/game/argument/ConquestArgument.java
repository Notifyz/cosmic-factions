package com.massivecraft.factions.game.argument;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.massivecraft.factions.P;
import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.game.type.ConquestEvent;

public class ConquestArgument implements CommandExecutor {

	private final P plugin;

	public ConquestArgument(P plugin) {
		this.plugin = plugin;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		Event event = plugin.getEventManager().getEventTimer().getEvent();

		if (event == null) {
			ConquestEvent conquestEvent = (ConquestEvent) plugin.getEventManager().getEvents().stream().filter(next -> next instanceof ConquestEvent).findFirst().orElse(null);

			if (conquestEvent == null) return true;

			sender.sendMessage(new String[] {
					"",
					ChatColor.translateAlternateColorCodes('&', "&b&l&nJURASSIC CONQUEST INFORMATION"),
					"",
					ChatColor.translateAlternateColorCodes('&', "&d&lLast Conquest: &b" + DurationFormatUtils.formatDurationWords((System.currentTimeMillis() - conquestEvent.getLastActive()), true, true) + " ago"),
					ChatColor.translateAlternateColorCodes('&', "&d&lLast Location: &b" + (int) conquestEvent.getLastChestLocation().getX() + "x " + (int) conquestEvent.getLastChestLocation().getY() + "y " + (int) conquestEvent.getLastChestLocation().getZ() + "z"),
					ChatColor.translateAlternateColorCodes('&', "&d&lLast Conquerer: &b" + conquestEvent.getLastConquerer()),
					""
			});
		} else {
			if (event instanceof ConquestEvent) {
				ConquestEvent conquestEvent = (ConquestEvent) event;

				sender.sendMessage(new String[] {
						"",
						ChatColor.translateAlternateColorCodes('&', "&b&l&nJURASSIC CONQUEST INFORMATION"),
						"",
						ChatColor.translateAlternateColorCodes('&', "&d&lLast Conquest: &bCurrently Active"),
						ChatColor.translateAlternateColorCodes('&', "&d&lLast Location: &b" + (int) conquestEvent.getLastChestLocation().getX() + "x " + (int) conquestEvent.getLastChestLocation().getY() + "y " + (int) conquestEvent.getLastChestLocation().getZ() + "z"),
						ChatColor.translateAlternateColorCodes('&', "&d&lConquest Health: &b" + conquestEvent.getCurrentHealth() + '/' + conquestEvent.getMaximumHealth()),
						""
				});
			}
		}
		return false;
	}

}
