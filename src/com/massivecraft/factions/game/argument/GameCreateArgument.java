package com.massivecraft.factions.game.argument;

import org.apache.commons.lang.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.massivecraft.factions.P;
import com.massivecraft.factions.argument.FactionArgument;
import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.game.type.ConquestEvent;

public class GameCreateArgument extends FactionArgument {

	private final P plugin;

	public GameCreateArgument(P plugin) {
		super("create", "Create new event", "faction.command.game.create");

		this.plugin = plugin;
	}
	@Override
	public String getUsage(String label) {
		return '/' + label + ' ' + getName() + " <eventName> <eventType>";
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
	       if (args.length < 3) {
	            sender.sendMessage(ChatColor.RED + "Usage: " + getUsage(label));
	            return true;
	        }

	        Event event = plugin.getEventManager().getEvent(args[1]);

	        if (event != null) {
	            sender.sendMessage(ChatColor.RED + "There is already an event named " + args[1] + '.');
	            return true;
	        }

	        switch (args[2].toUpperCase()) {
	            case "CONQUEST":
	            	event = new ConquestEvent(args[1]);
	                break;
	            default:
	                sender.sendMessage(ChatColor.RED + "Usage: " + getUsage(label));
	                return true;
	        }

	        plugin.getEventManager().createEvent(event, sender);

	        sender.sendMessage(ChatColor.GOLD + "You have created event " + ChatColor.WHITE + event.getName() + ChatColor.GOLD + " with type " + WordUtils.capitalizeFully(args[2]) + '.');
	        return true;
	    }

}
