package com.massivecraft.factions.game.argument;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

import com.massivecraft.factions.P;
import com.massivecraft.factions.argument.FactionArgument;
import com.massivecraft.factions.game.Event;
/**
 * A {@link EventsArgument} used for starting an event.
 */
public class GameStartArgument extends FactionArgument {

    private final P plugin;

    public GameStartArgument(P plugin) {
        super("start", "Start an event", "faction.command.game.start", new String[] {"host", "begin"});
        this.plugin = plugin;
    }

    @Override
    public String getUsage(String label) {
        return '/' + label + ' ' + getName() + " <eventName>";
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Usage: " + getUsage(label));
            return true;
        }

        Event event = plugin.getEventManager().getEvent(args[1]);

        if (event == null) {
        	sender.sendMessage(ChatColor.RED + "There is not an event named '" + args[1] + "'.");
        	return true;
        }

        if (plugin.getEventManager().getEventTimer().tryContesting(event, sender)) {
            sender.sendMessage(ChatColor.YELLOW + "Successfully contested " + event.getName() + '.');
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        if (args.length != 2) {
            return Collections.emptyList();
        }
        return plugin.getEventManager().getEvents().stream().map(Event::getName).collect(Collectors.toList());
    }
}