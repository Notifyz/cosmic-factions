package com.massivecraft.factions.game;

import com.massivecraft.factions.P;
import com.massivecraft.factions.argument.FactionArgumentExecutor;
import com.massivecraft.factions.game.argument.GameAddItemArgument;
import com.massivecraft.factions.game.argument.GameCreateArgument;
import com.massivecraft.factions.game.argument.GameDeleteArgument;
import com.massivecraft.factions.game.argument.GameSetEquipmentArgument;
import com.massivecraft.factions.game.argument.GameStartArgument;

public class GameExecutor extends FactionArgumentExecutor {

	public GameExecutor(P plugin) {
		super("game");

		addArgument(new GameCreateArgument(plugin));
		addArgument(new GameDeleteArgument(plugin));
		addArgument(new GameStartArgument(plugin));
		addArgument(new GameAddItemArgument(plugin));
		addArgument(new GameSetEquipmentArgument(plugin));
	}
}