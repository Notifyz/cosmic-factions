package com.massivecraft.factions.game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.P;
import com.massivecraft.factions.game.type.ConquestEvent;
import com.massivecraft.factions.util.ElementPair;
import com.massivecraft.factions.util.timer.GlobalTimer;
import com.massivecraft.factions.util.zone.PersistableLocation;

/**
 * Timer that handles the cooldown for events.
 */
public class EventTimer extends GlobalTimer implements Listener {

    public static final long RESCHEDULE_FREEZE_MILLIS = TimeUnit.MINUTES.toMillis(30L);
    public static final String CONQUEST_WORLD_NAME = "level";

    private long startStamp;                 // the milliseconds at when the current event started.
    private long lastContestedEventMillis;   // the milliseconds at when the last event was contested.
    private Event event;

    private final P plugin;

    public Event getEvent() {
        return event;
    }

    public EventTimer(P plugin) {
        super("Event", 0L);
        this.plugin = plugin;
        /*new BukkitRunnable() {
            @Override
            public void run() {
                if (event != null) {
                    event.getEventType().getEventTracker().tick(EventTimer.this, event);
                    return;
                }
            }
        }.runTaskTimer(plugin, 20L, 20L);*/
    }

    @Override
    public String getScoreboardPrefix() {
        return ChatColor.AQUA.toString();
    }

    @Override
    public String getName() {
        return event != null ? event.getName() : "Event";
    }

    @Override
    public boolean clearCooldown() {
        boolean result = super.clearCooldown();
        if (event != null) {
            event.getEventType().getEventTracker().stopTiming(event);
            event = null;
            startStamp = -1L;
            result = true;
        }

        return result;
    }

    @Override
    public long getRemaining() {
        if (event == null) {
            return 0L;
        } else {
            return super.getRemaining();
        }
    }

    /**
     * Handles the winner for this event.
     *
     * @param winner the {@link Player} that won
     */
    public void handleWinner(Player winner) {
        if (event != null) {
        	switch (event.getEventType()) {
			case CONQUEST:
				ConquestEvent conquestEvent = (ConquestEvent) event;
				conquestEvent.setLastConquerer(winner.getName());
				plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&l(!) &b" + winner.getName() + " &7has unlocked the Conquest Chest at &f&l" + (int) conquestEvent.getLastChestLocation().getX() + "&fx &f&l" + (int) conquestEvent.getLastChestLocation().getY() + "&fy &f&l" + (int) conquestEvent.getLastChestLocation().getZ() + "&fz &7its loot is spilling everywhere!"));
				int amountOfWonItems = ThreadLocalRandom.current().nextInt(4, 11);
				for (int i = 0; i < amountOfWonItems; i++) {
					ItemStack item = conquestEvent.getLootItems()[ThreadLocalRandom.current().nextInt(conquestEvent.getLootItems().length)];

					conquestEvent.getLastChestLocation().getWorld().dropItemNaturally(conquestEvent.getLastChestLocation().getLocation().add(0, 1, 0), item);
				}

				Bukkit.getWorld(CONQUEST_WORLD_NAME).getBlockAt((int) conquestEvent.getLastChestLocation().getX(), (int) conquestEvent.getLastChestLocation().getY(), (int) conquestEvent.getLastChestLocation().getZ()).setType(Material.AIR);
				// Remove chest block
				break;
			default:
				break;

        	}

            clearCooldown(); // must always be cooled last as this nulls some variables.
        }
    }

    /**
     * Tries contesting an {@link Event}.
     *
     * @param event the {@link Event} to be contested
     * @param sender       the contesting {@link CommandSender}
     * @return true if the {@link Event} was successfully contested
     */
    public boolean tryContesting(Event event, CommandSender sender) {
        if (this.event != null) {
            sender.sendMessage(ChatColor.RED + "Sorry, there is already a mini-game running.");
            return false;
        }

        // Don't allow events to reschedule their-self before they are allowed to.
        long millis = System.currentTimeMillis();
        /*if (this.lastContestedEventMillis + EventTimer.RESCHEDULE_FREEZE_MILLIS - millis > 0L) {
            sender.sendMessage(ChatColor.RED + "You may not schedule another mini-event for another " + 30 + " minutes.");
            return false;
        }*/

        if (event instanceof ConquestEvent) {
        	ConquestEvent conquestEvent = (ConquestEvent) event;

        	List<Chunk> chunks = findWildernessChunks(Bukkit.getWorld(CONQUEST_WORLD_NAME), (Chunk chunk) -> Board.getInstance().getFactionAt(new FLocation(chunk)).isWilderness());

        	if (chunks.isEmpty()) {
        		sender.sendMessage(ChatColor.RED + "Sorry, we were unable to find free location. Try again.");
        		return false;
        	}

        	//int i = 0;
        	for (Chunk chunk : chunks) {
        		Board.getInstance().setFactionAt(Factions.getInstance().getWarZone(), new FLocation(chunk));
        		//Bukkit.broadcastMessage("coords " + i + ':' + (chunk.getX() << 4) + "::" + (chunk.getZ() << 4));
        		//i++;
        	}

        	int x = chunks.get(0).getX() << 4;
        	int z = chunks.get(0).getZ() << 4;

        	conquestEvent.setLastChestLocation(new PersistableLocation(Bukkit.getWorld(CONQUEST_WORLD_NAME).getBlockAt(x, Bukkit.getWorld(CONQUEST_WORLD_NAME).getHighestBlockYAt(x, z), z).getLocation()));

        	conquestEvent.getLastChestLocation().getLocation().getBlock().setType(Material.CHEST);

        	chunks.forEach(chunk -> conquestEvent.getChunks().add(new ElementPair<>(chunk.getX(), chunk.getZ())));
        	conquestEvent.setCurrentHealth(150);
        	conquestEvent.setMaximumHealth(150);
        	conquestEvent.setActive(true);
        }

        lastContestedEventMillis = millis;
        startStamp = millis;
        this.event = event;

        event.getEventType().getEventTracker().onContest(event, this);
        return true;
    }

    public static List<Chunk> findWildernessChunks(World world, Function<Chunk, Boolean> wildernessChecker) {
		for (int x = (ThreadLocalRandom.current().nextInt(6000) - 3000); x < 3000; x += 16) {
			chunks: for (int z = (ThreadLocalRandom.current().nextInt(6000) - 3000); z < 3000; z += 16) {
				Chunk chunk = world.getChunkAt(x >> 4, z >> 4);
				if (wildernessChecker.apply(chunk)) {
					List<Chunk> chunks = new ArrayList<>(Collections.singleton(chunk));
					for (int xo = -1; xo <= 1; xo ++) {
						for (int zo = -1; zo <= 1; zo ++) {
							if (xo == 0 && zo == 0) continue;
							Chunk other = world.getChunkAt((x >> 4) + xo, (z >> 4) + zo);
							if (!wildernessChecker.apply(other)) continue chunks;
							chunks.add(other);
						}
					}
					return chunks;
				}
			}
		}
		return Collections.emptyList();
	}

    /**
     * Tries forcefully contesting an {@link Event}.
     *
     * @param event the {@link Event} to be contested
     * @param sender       the contesting {@link CommandSender}
     * @return true if the {@link Event} was successfully contested
     */
    public boolean tryForceContest(Event event, CommandSender sender) {
        if (this.event != null) {
            sender.sendMessage(ChatColor.RED + "Sorry, there is already a mini-game running.");
            return false;
        }

        long millis = System.currentTimeMillis();


        lastContestedEventMillis = millis;
        startStamp = millis;
        this.event = event;

        event.getEventType().getEventTracker().onContest(event, this);
        return true;
    }

    /**
     * Gets the total uptime in milliseconds since the event started.
     *
     * @return the time in milliseconds since event started
     */
    public long getUptime() {
        return System.currentTimeMillis() - startStamp;
    }

    /**
     * Gets the time in milliseconds since the event started.
     *
     * @return the time in milliseconds since event started
     */
    public long getStartStamp() {
        return startStamp;
    }

    /**
     * Check if event contesting has cooldown.
     *
     * @return true if last contested is cooldown is active
     */
    public boolean hasEventCooldown() {
    	return this.lastContestedEventMillis + EventTimer.RESCHEDULE_FREEZE_MILLIS - System.currentTimeMillis() > 0L;
    }
}