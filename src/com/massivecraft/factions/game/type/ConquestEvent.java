package com.massivecraft.factions.game.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.util.ElementPair;
import com.massivecraft.factions.util.MiscUtil;
import com.massivecraft.factions.util.RewardableItemStack;
import com.massivecraft.factions.util.zone.PersistableLocation;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConquestEvent extends Event implements ConfigurationSerializable {

	protected boolean active;

	protected int currentHealth, maximumHealth;

	protected transient long lastTouched, lastActive;

	protected PersistableLocation lastChestLocation;

	protected List<ElementPair<Integer>> chunks = new ArrayList<>();

	protected List<RewardableItemStack> loot = new ArrayList<>();

	protected String lastConquerer;

	protected ItemStack[] exterminatorArmor;
	protected ItemStack exterminatorWeapon;

	protected ItemStack[] enforcerArmor;
	protected ItemStack enforcerWeapon;

	protected ItemStack[] marksmanArmor;
	protected ItemStack marksmanWeapon;

	protected ItemStack[] items = new ItemStack[100];

	protected BukkitTask startAnnouncer, activeAnnouncer;

	public ConquestEvent(String name) {
		super(name);
	}

    public ConquestEvent(Map<String, Object> map) {
        super(map);

		List<RewardableItemStack> fakeList = MiscUtil.createList(map.get("lootItems"), RewardableItemStack.class);
		List<RewardableItemStack> itemStacks;
		if (fakeList == null) {
			itemStacks = new ArrayList<>();
		} else {
			itemStacks = fakeList;
		}
		for (RewardableItemStack itemStack : itemStacks) {
			setupRarity(itemStack.getItemStack(), itemStack.getPercentage());
			loot.add(itemStack);
		}

        this.active = (boolean) map.get("active");
        this.maximumHealth = (int) map.get("maximumHealth");
        this.lastChestLocation = (PersistableLocation) map.get("lastChestLocation");
        MiscUtil.createList(map.get("chunks"), ElementPair.class).stream().forEach(chunks::add);
        this.lastActive = Long.parseLong((String) map.get("lastActive"));
        this.lastConquerer = (String) map.get("lastConquerer");
        this.exterminatorArmor = MiscUtil.deserialize((String) map.get("exterminatorArmor"));
        this.exterminatorWeapon = (ItemStack) map.get("exterminatorWeapon");
        this.enforcerArmor = MiscUtil.deserialize((String) map.get("enforcerArmor"));
        this.enforcerWeapon = (ItemStack) map.get("enforcerWeapon");
        this.marksmanArmor = MiscUtil.deserialize((String) map.get("marksmanArmor"));
        this.marksmanWeapon = (ItemStack) map.get("marksmanWeapon");
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = super.serialize();
		map.put("lootItems", loot);
        map.put("active", active);
        map.put("maximumHealth", maximumHealth);
        map.put("lastChestLocation", lastChestLocation);
        map.put("chunks", chunks);
        map.put("lastActive", Long.toString(lastActive));
        map.put("lastConquerer", lastConquerer);
        map.put("exterminatorArmor", MiscUtil.serialize(exterminatorArmor));
        map.put("exterminatorWeapon", exterminatorWeapon);
        map.put("enforcerArmor", MiscUtil.serialize(enforcerArmor));
        map.put("enforcerWeapon", enforcerWeapon);
        map.put("marksmanArmor", MiscUtil.serialize(marksmanArmor));
        map.put("marksmanWeapon", marksmanWeapon);
        return map;
    }

	@Override
	public EventType getEventType() {
		return EventType.CONQUEST;
	}

	public ItemStack[] getLootItems() { // We need to use this loot of {@link items} instead of loot, due to percentage accelerator.
		return items;
	}

	public void addItem(RewardableItemStack itemStack) {
		setupRarity(itemStack.getItemStack(), itemStack.getPercentage());
		loot.add(itemStack);
	}


    public void setupRarity(ItemStack stack, int percent) {
        int currentItems = 0;
        for(ItemStack item : this.items) {
            if(item != null && item.getType() != Material.AIR) {
                ++currentItems;
            }
        }
        for(int min = Math.min(100, currentItems + percent), i = currentItems; i < min; ++i) {
            this.items[i] = stack;
        }
    }
}