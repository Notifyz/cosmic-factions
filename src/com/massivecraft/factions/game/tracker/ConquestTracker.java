package com.massivecraft.factions.game.tracker;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.P;
import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.game.EventTimer;
import com.massivecraft.factions.game.type.ConquestEvent;
import com.massivecraft.factions.game.type.EventType;
import com.massivecraft.factions.integration.EventTracker;
import com.massivecraft.factions.util.particle.ParticleEffect;

public class ConquestTracker implements EventTracker, Listener {

	private final P plugin;
	private final List<Entity> bosses = new ArrayList<>();

	public ConquestTracker(P plugin) {
		this.plugin = plugin;
		Bukkit.getPluginManager().registerEvents(this, plugin);
	}

	@Override
	public EventType getEventType() {
		return EventType.CONQUEST;
	}

	@Override
	public void tick(EventTimer eventTimer, Event event) {

	}

	@Override
	public void onContest(Event event, EventTimer eventTimer) {
		ConquestEvent conquestEvent = (ConquestEvent) event;
		plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&f&l** &b&lJURASSIC CONQUEST &f&l**"));
		plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&7A &bConquest Chest &7has spawned at &f&l" + (int) conquestEvent.getLastChestLocation().getX() + "&fx &f&l" + (int) conquestEvent.getLastChestLocation().getY() + "&fy &f&l" + (int) conquestEvent.getLastChestLocation().getZ() + "&fz"));
		plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', ""));
		plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&f&l(!) &7Conquest Chest contains &bfaction crystals &7and other extremely rare and powerful loot. But BEWARE! &7They are defended by &f&ndangerous space demons."));

		Bukkit.getScheduler().runTaskLater(plugin, () -> {
		conquestEvent.setActiveAnnouncer(new BukkitRunnable() {
				@Override
				public void run() {
					int ago = (int) Math.ceil(TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - eventTimer.getStartStamp()));
					plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&f&l** &b&lJURASSIC CONQUEST &f&l**"));
					plugin.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&7A Conquest Chest &7has spawned &f&n" + ago + " Minutes ago &7at &f&l" + (int) conquestEvent.getLastChestLocation().getX() + "&fx &f&l" + (int) conquestEvent.getLastChestLocation().getY() + "&fy &f&l" + (int) conquestEvent.getLastChestLocation().getZ() + "&fz"));
				}
			}.runTaskTimer(plugin, 0L, 6000L));
		}, 6000L);
	}

	@Override
	public void stopTiming(Event event) {
		ConquestEvent conquestEvent = (ConquestEvent) event;
		conquestEvent.setLastActive(System.currentTimeMillis());
		conquestEvent.setActive(false);

		if (conquestEvent.getStartAnnouncer() != null) {
			conquestEvent.getStartAnnouncer().cancel();
		}

		if (conquestEvent.getActiveAnnouncer() != null) {
			conquestEvent.getActiveAnnouncer().cancel();
		}

		bosses.stream().filter(entity -> entity != null && (!entity.isDead())).forEach(Entity::remove); // Remove all existent mobs.
		bosses.clear();

		Bukkit.getScheduler().runTaskLater(plugin, () -> {
			conquestEvent.getChunks().forEach(pair -> {
				Chunk chunk = Bukkit.getWorld(EventTimer.CONQUEST_WORLD_NAME).getChunkAt(pair.left(), pair.right());
				Board.getInstance().setFactionAt(Factions.getInstance().getWilderness(), new FLocation(chunk));
			});
		}, 20L * 600L); // After 10mins, warzone get's unclaimed aka replaced with wilderness
	}

	@EventHandler
	public void onChunkUnload(ChunkUnloadEvent event) {
		for (Entity entity : event.getChunk().getEntities()) {
			if (bosses.contains(entity)) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onBossDeath(EntityDeathEvent event) {
		if (!bosses.contains(event.getEntity())) return;

		event.setDroppedExp(0);
		event.getDrops().clear();
	}

	@EventHandler
	public void onChestInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		Block block = event.getClickedBlock();

		if (block == null) return;

		if (block.getType() != Material.CHEST) return;

		Event activeEvent = plugin.getEventManager().getEventTimer().getEvent();

		if (activeEvent == null) return;

		if (!(activeEvent instanceof ConquestEvent)) return;

		ConquestEvent conquestEvent = (ConquestEvent) activeEvent;

		if (!conquestEvent.isActive()) return;

		if (!conquestEvent.getLastChestLocation().getLocation().equals(block.getLocation())) return;

		if (conquestEvent.getLastTouched() + 2000 > System.currentTimeMillis()) {
			event.setCancelled(true); // We need to cancel it, because meanwhile the cooldown they would be able to open/break it.
			return;
		}

		int next = 0;
		conquestEvent.setCurrentHealth(next = conquestEvent.getCurrentHealth() - 1);
		conquestEvent.setLastTouched(System.currentTimeMillis());
		if (next > 0) {
			if (next == 149) { // First hit, so we need to spawn all bosses
				spawnExterminator(block.getLocation().add(1.5, 0, 1.5), conquestEvent);
				spawnExterminator(block.getLocation().add(-1.5, 0, 1.5), conquestEvent);
				spawnExterminator(block.getLocation().add(1.5, 0, -1.5), conquestEvent);

				spawnEnforcer(block.getLocation().add(2.5, 0, 1.5), conquestEvent);
				spawnEnforcer(block.getLocation().add(2.5, 0, -1.5), conquestEvent);
				spawnEnforcer(block.getLocation().add(-2.5, 0, 1.5), conquestEvent);

				spawnMarksman(block.getLocation().add(-1.5, 0, 2.5), conquestEvent);
				spawnMarksman(block.getLocation().add(1.5, 0, -2.5), conquestEvent);
				spawnMarksman(block.getLocation().add(-1.5, 0, 2.5), conquestEvent);
			}
			if (ThreadLocalRandom.current().nextInt(1, 100) == 12) {
				switch (ThreadLocalRandom.current().nextInt(1, 4)) {
				case 1:
					spawnExterminator(block.getLocation().add(1.5, 0, -1.5), conquestEvent);
					break;
				case 2:
					spawnEnforcer(block.getLocation().add(2.5, 0, -1.5), conquestEvent);
					break;
				case 3:
					spawnMarksman(block.getLocation().add(-1.5, 0, 2.5), conquestEvent);
					break;
				default:
					break;
				}
			}
			for (Entity entity : block.getLocation().getWorld().getNearbyEntities(block.getLocation(), 150.0D, 150.0D, 150.0D)) {
				if (entity instanceof Player) {
					Player nearby = (Player) entity;

					player.setVelocity(new Vector(0.5D, 0.5D, 0.5D));

					ParticleEffect.CLOUD.display(0.0F, 0.0F, 0.0F, 0.3F, 20, block.getLocation().add(0.0D, 1.0D, 0.0D), nearby);

					nearby.playSound(nearby.getLocation(), Sound.ZOMBIE_WOODBREAK, 0.5F, 1.0F);
					nearby.sendMessage(ChatColor.GRAY + " ** Conquest Chest: " + ChatColor.AQUA + next + ChatColor.GRAY + " / 150");
				}
			}
		} else {
			plugin.getEventManager().getEventTimer().handleWinner(player);
		}
		event.setCancelled(true);
	}

	public void spawnExterminator(Location location, ConquestEvent event) {
		PigZombie pigZombie = location.getWorld().spawn(location, PigZombie.class);
		pigZombie.setBaby(true);
		pigZombie.setAngry(true);
		pigZombie.setRemoveWhenFarAway(false);
		pigZombie.setCustomName(ChatColor.LIGHT_PURPLE + ChatColor.BOLD.toString() + "Jurassic Exterminator");
		pigZombie.setCustomNameVisible(true);
		pigZombie.getEquipment().setArmorContents(event.getExterminatorArmor());
		pigZombie.getEquipment().setItemInHand(event.getExterminatorWeapon());
		pigZombie.setMaxHealth(750);
		pigZombie.setHealth(750);
		pigZombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 3));

		bosses.add(pigZombie);
	}

	public void spawnEnforcer(Location location, ConquestEvent event) {
		Zombie zombie = location.getWorld().spawn(location, Zombie.class);
		zombie.setRemoveWhenFarAway(false);
		zombie.setCustomName(ChatColor.GOLD + ChatColor.BOLD.toString() + "Jurassic Enforcer");
		zombie.setCustomNameVisible(true);
		zombie.getEquipment().setArmorContents(event.getEnforcerArmor());
		zombie.getEquipment().setItemInHand(event.getEnforcerWeapon());
		zombie.setMaxHealth(1500);
		zombie.setHealth(1500);
		zombie.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 3));

		bosses.add(zombie);
	}

	public void spawnMarksman(Location location, ConquestEvent event) {
		Skeleton skeleton = location.getWorld().spawn(location, Skeleton.class);
		skeleton.setRemoveWhenFarAway(false);
		skeleton.setCustomName(ChatColor.DARK_PURPLE + ChatColor.BOLD.toString() + "Timeworn Marksman");
		skeleton.setCustomNameVisible(true);
		skeleton.getEquipment().setArmorContents(event.getMarksmanArmor());
		skeleton.getEquipment().setItemInHand(event.getMarksmanWeapon());
		skeleton.setMaxHealth(400);
		skeleton.setHealth(400);
		skeleton.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 3));

		bosses.add(skeleton);
	}

	/*@EventHandler Actually it's claimed as warzone so we shouldn't mind.
	public void onChestBreak(BlockBreakEvent event) {
		Block block = event.getBlock();

		if (block.getType() != Material.CHEST) return;

		Event activeEvent = plugin.getEventManager().getEventTimer().getEvent();

		if (activeEvent == null) return;

		if (!(activeEvent instanceof ConquestEvent)) return;

		ConquestEvent conquestEvent = (ConquestEvent) activeEvent;

		if (!conquestEvent.isActive()) return;


	}*/
}
