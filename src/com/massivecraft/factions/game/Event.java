package com.massivecraft.factions.game;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.serialization.ConfigurationSerializable;

import com.massivecraft.factions.game.type.EventType;

public abstract class Event implements ConfigurationSerializable {

    /* Gets the {@link EventType} of this {@link CapturableFaction}.
    *
    * @return the {@link EventType}
    */
   public abstract EventType getEventType();

    protected final UUID uniqueID;
    protected String name;

    protected long creationMillis = System.currentTimeMillis();  // the system millis when the event was created
    public long lastRenameMillis;  // the system millis when the event was last renamed

    public Event(String name) {
        this.uniqueID = UUID.randomUUID();
        this.name = name;
    }

    public Event(Map<String, Object> map) {
        this.uniqueID = UUID.fromString((String) map.get("uniqueID"));
        this.name = (String) map.get("name");
        this.creationMillis = Long.parseLong((String) map.get("creationMillis"));
        this.lastRenameMillis = Long.parseLong((String) map.get("lastRenameMillis"));
    }

    @Override
    public Map<String, Object> serialize() {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("uniqueID", uniqueID.toString());
        map.put("name", name);
        map.put("creationMillis", Long.toString(creationMillis));
        map.put("lastRenameMillis", Long.toString(lastRenameMillis));
        return map;
    }

    /**
     * Gets the unique ID of this {@link Event}.
     *
     * @return the {@link UUID}
     */
    public UUID getUniqueID() {
        return uniqueID;
    }

    /**
     * Gets the name of this {@link Event}.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name of this {@link Event}.
     *
     * @param name the name to set
     * @return true if the name was successfully set
     */
    public boolean setName(String name) {
        return setName(name, Bukkit.getConsoleSender());
    }

    /**
     * Sets the name of this {@link Event}.
     *
     * @param name   the name to set
     * @param sender the setting {@link CommandSender}
     * @return true if the name was successfully set
     */
    public boolean setName(String name, CommandSender sender) {
        if (this.name.equals(name)) {
            return false;
        }

        this.lastRenameMillis = System.currentTimeMillis();
        this.name = name;
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event iEvent = (Event) o;

        if (creationMillis != iEvent.creationMillis) return false;
        if (lastRenameMillis != iEvent.lastRenameMillis) return false;
        if (uniqueID != null ? !uniqueID.equals(iEvent.uniqueID) : iEvent.uniqueID != null) return false;
        return !(name != null ? !name.equals(iEvent.name) : iEvent.name != null);
    }

    @Override
    public int hashCode() {
        int result;
        result = uniqueID != null ? uniqueID.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (creationMillis ^ (creationMillis >>> 32));
        result = 31 * result + (int) (lastRenameMillis ^ (lastRenameMillis >>> 32));
        return result;
    }
}