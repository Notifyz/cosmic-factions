package com.massivecraft.factions.argument;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.text.WordUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableList;

/**
 * Helper class used to build commands that have multiple
 * sub-commands referred to as {@link FactionArgument}s
 */
public abstract class FactionArgumentExecutor implements CommandExecutor, TabCompleter {

    protected final List<FactionArgument> arguments = new ArrayList<>();
    protected final String label;

    public FactionArgumentExecutor(String label) {
        this.label = label;
    }

    /**
     * Checks if this executor contains an {@link FactionArgument}.
     *
     * @param argument the argument to check for
     * @return true if this executor contains argument
     */
    public boolean containsArgument(FactionArgument argument) {
        return arguments.contains(argument);
    }

    /**
     * Adds an {@link FactionArgument} to this {@link FactionArgumentExecutor}.
     *
     * @param argument the {@link FactionArgument} to add
     */
    public void addArgument(FactionArgument argument) {
        arguments.add(argument);
    }

    /**
     * Removes an {@link FactionArgument} from this {@link FactionArgumentExecutor}.
     *
     * @param argument the {@link FactionArgument} to remove
     */
    public void removeArgument(FactionArgument argument) {
        arguments.remove(argument);
    }

    /**
     * Gets a command argument that has a given name
     * or alias included in this executor.
     *
     * @param id the name to search for
     * @return the command argument, null if absent
     */
    public FactionArgument getArgument(String id) {
        for (FactionArgument argument : arguments) {
            String name = argument.getName();
            if (name.equalsIgnoreCase(id) || Arrays.asList(argument.getAliases()).contains(id.toLowerCase())) {
                return argument;
            }
        }

        return null;
    }

    /**
     * Gets the label of the command this executor is for.
     *
     * @return the command for this executor
     */
    public String getLabel() {
        return label;
    }

    /**
     * Gets an {@link ImmutableList} of the {@link FactionArgument}s
     * used for this executor.
     *
     * @return list of {@link FactionArgument}s for this executor
     */
    public List<FactionArgument> getArguments() {
        return ImmutableList.copyOf(arguments);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.AQUA + "*** " + WordUtils.capitalizeFully(this.label) + " Help ***");
            for (FactionArgument argument : arguments) {
                String permission = argument.getPermission();
                if (permission == null || sender.hasPermission(permission)) {
                    sender.sendMessage(ChatColor.GRAY + argument.getUsage(label) + " - " + argument.getDescription() + '.');
                }
            }

            return true;
        }

        FactionArgument argument = getArgument(args[0]);
        String permission = (argument == null) ? null : argument.getPermission();
        if (argument == null || (permission != null && !sender.hasPermission(permission))) {
            sender.sendMessage(ChatColor.RED + WordUtils.capitalizeFully(this.label) + " sub-command " + args[0] + " not found.");
            return true;
        }

        argument.onCommand(sender, command, label, args);
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String label, String[] args) {
        List<String> results = new ArrayList<>();
        if (args.length < 2) {
            for (FactionArgument argument : arguments) {
                String permission = argument.getPermission();
                if (permission == null || sender.hasPermission(permission)) {
                    results.add(argument.getName());
                }
            }
        } else {
            FactionArgument argument = getArgument(args[0]);
            if (argument == null) {
                return results;
            }

            String permission = argument.getPermission();
            if (permission == null || sender.hasPermission(permission)) {
                results = argument.onTabComplete(sender, command, label, args);
                if (results == null) {
                    return null;
                }
            }
        }

        return getCompletions(args, results);
	}

	public static List<String> getCompletions(String[] args, List<String> input) {
		return getCompletions(args, input, 80);
	}

	public static List<String> getCompletions(String[] args, List<String> input, int limit) {
		Preconditions.checkNotNull((Object) args);
		Preconditions.checkArgument(args.length != 0);
		String argument = args[args.length - 1];
		return input.stream().filter(string -> string.regionMatches(true, 0, argument, 0, argument.length())).limit(limit).collect(Collectors.toList());
	}
}