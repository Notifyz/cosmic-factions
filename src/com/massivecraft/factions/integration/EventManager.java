package com.massivecraft.factions.integration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.MemorySection;
import org.bukkit.craftbukkit.v1_8_R3.util.LongHash;
import org.bukkit.event.Listener;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Table;
import com.massivecraft.factions.P;
import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.game.EventTimer;
import com.massivecraft.factions.util.Config;
import com.massivecraft.factions.util.zone.Claim;

public class EventManager implements Listener, EventInterfaceManager {

    // Cached for faster lookup for events. Potentially usage Guava Cache for
    // future implementations (database).
	private final String FLATFILE_NAME = "events";
    private final Table<String, Long, Claim> claimPositionMap = HashBasedTable.create();
    private final ConcurrentMap<UUID, Event> eventsUUIDMap = new ConcurrentHashMap<>();
    private final Map<String, UUID> eventsNameMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

    private final EventTimer eventTimer;

    private Config config;
    private final P plugin;

    public EventManager(P plugin) {
        this.plugin = plugin;
        this.eventTimer = new EventTimer(plugin);
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.reloadEventData();
    }

    public EventTimer getEventTimer() {
    	return eventTimer;
    }

    @Override
    public Map<String, UUID> getEventsNameMap() {
        return this.eventsNameMap;
    }

    @Override
    public ImmutableList<Event> getEvents() {
        return ImmutableList.copyOf(this.eventsUUIDMap.values());
    }

    @Override
    public Claim getClaimAt(World world, int x, int z) {
        return this.claimPositionMap.get(world.getName(), LongHash.toLong(x, z));
    }

    @Override
    public Claim getClaimAt(Location location) {
        return this.getClaimAt(location.getWorld(), location.getBlockX(), location.getBlockZ());
    }

    @Override
    public Event getEventAt(World world, int x, int z) {
        Claim claim = this.getClaimAt(world, x, z);
        if (claim != null) {
            Event Event = claim.getEvent();
            if (Event != null) {
                return Event;
            }
        }
        return null;
    }

    @Override
    public Event getEventAt(Location location) {
        return getEventAt(location.getWorld(), location.getBlockX(), location.getBlockZ());
    }

    @Override
    public Event getEventAt(Block block) {
        return getEventAt(block.getLocation());
    }

    @Override
    public Event getEvent(String eventName) {
        UUID uuid = eventsNameMap.get(eventName);
        return uuid == null ? null : eventsUUIDMap.get(uuid);
    }

    @Override
    public Event getEvent(UUID eventUUID) {
        return eventsUUIDMap.get(eventUUID);
    }

    @Override
    public boolean containsEvent(Event Event) {
        return eventsNameMap.containsKey(Event.getName());
    }

    @Override
    public boolean createEvent(Event Event) {
        return createEvent(Event, Bukkit.getConsoleSender());
    }

    @Override
    public boolean createEvent(Event Event, CommandSender sender) {
        if (this.eventsUUIDMap.putIfAbsent(Event.getUniqueID(), Event) != null) {
            return false;  // event already exists.
        }

        this.eventsNameMap.put(Event.getName(), Event.getUniqueID());
        return true;
    }

    @Override
    public boolean removeEvent(Event Event, CommandSender sender) {
        if (!this.eventsUUIDMap.containsKey(Event.getUniqueID())) {
            return false;
        }

        this.eventsUUIDMap.remove(Event.getUniqueID());
        this.eventsNameMap.remove(Event.getName());

        return true;
    }

    private void cacheEvent(Event Event) {
        eventsNameMap.put(Event.getName(), Event.getUniqueID());
        eventsUUIDMap.put(Event.getUniqueID(), Event);
    }

    @Override
    public void reloadEventData() {
        this.eventsNameMap.clear();
        this.config = new Config(plugin, FLATFILE_NAME);

        Object object = config.get(FLATFILE_NAME);
        if (object instanceof MemorySection) {
            MemorySection section = (MemorySection) object;
            for (String eventName : section.getKeys(false)) {
                Object next = config.get(section.getCurrentPath() + '.' + eventName);
                if (next instanceof Event) {
                    cacheEvent((Event) next);
                }
            }
        } else if (object instanceof List<?>) {
            List<?> list = (List<?>) object;
            for (Object next : list) {
                if (next instanceof Event) {
                    cacheEvent((Event) next);
                }
            }
        }
    }

    @Override
    public void saveEventData() {
        this.config.set(FLATFILE_NAME, new ArrayList<>(eventsUUIDMap.values()));
        this.config.save();
    }

    public Config getEventConfig() {
    	return config;
    }
}