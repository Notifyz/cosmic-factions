package com.massivecraft.factions.integration;

import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.game.EventTimer;
import com.massivecraft.factions.game.type.EventType;

public interface EventTracker {

    EventType getEventType();

    /**
     * Handles ticking every 5 seconds
     *
     * @param eventTimer   the timer
     * @param eventFaction the faction
     */
    void tick(EventTimer eventTimer, Event event);

    void onContest(Event event, EventTimer eventTimer);

    void stopTiming(Event event);
}