package com.massivecraft.factions.integration;

import java.util.Map;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;

import com.google.common.collect.ImmutableList;
import com.massivecraft.factions.game.Event;
import com.massivecraft.factions.util.zone.Claim;

/**
 * Interface for managing {@link Event}s in the plugin.
 */
public interface EventInterfaceManager {

    Map<String, ?> getEventsNameMap();

    /**
     * Gets all the available {@link Event}s held by this manager.
     *
     * @return an immutable list of {@link Event}s
     */
    ImmutableList<Event> getEvents();

    /**
     * Gets the optional {@link Claim} at a given {@link Location}.
     *
     * @param location the {@link Location} to look at
     * @return the {@link Claim} at the given {@link Location}
     */
    Claim getClaimAt(Location location);

    /**
     * Gets the optional {@link Claim} at given co-ordinates.
     *
     * @param world the world to get claim in
     * @param x     the x co-ordinate to get at
     * @param z     the z co-ordinate to get at
     * @return the claim at co-ordinates
     */
    Claim getClaimAt(World world, int x, int z);

    /**
     * Gets the event which owns a {@link Claim} at a
     * given {@link Location}.
     *
     * @param location the {@link Location} to check
     * @return the {@link Event} owning the claim at {@link Location}
     */
    Event getEventAt(Location location);

    /**
     * Gets the event which owns a {@link Claim} at the position of a given {@link Block}.
     *
     * @param block the {@link Block} to get at
     * @return the {@link Event} owning the claim at {@link Block}
     */
    Event getEventAt(Block block);

    /**
     * Gets a {@link Event} which owns a {@link Claim} at specific
     * co-ordinates.
     *
     * @param world the {@link World} to get for
     * @param x     the x coordinate to get at
     * @param z     the z coordinate to get at
     * @return the {@link Event} owning the {@link Claim}
     */
    Event getEventAt(World world, int x, int z);

    /**
     * Gets a {@link Event} with a given name.
     *
     * @param event Name the search string to lookup
     * @return {@link Event} with name of id
     */
    Event getEvent(String eventName);

    /**
     * Gets a {@link Event} with a given UUID.
     *
     * @param event UUID the search UUID to lookup
     * @return {@link Event} with uuid
     */
    Event getEvent(UUID eventUUID);

    /**
     * Checks if the manager holds a {@link Event}.
     *
     * @param Event the {@link Event} to check
     * @return true if manager contains the {@link Event}
     */
    boolean containsEvent(Event Event);

    /**
     * Creates a {@link Event} for this manager.
     *
     * @param Event the {@link Event} to add
     * @return true if the {@link Event} successfully created
     */
    boolean createEvent(Event Event);

    /**
     * Saves a {@link Event} to this manager by a
     * given {@link CommandSender}.
     *
     * @param sender  the {@link CommandSender} of creations
     * @param Event the {@link Event} to add
     * @return true if the {@link Event} was successfully created
     */
    boolean createEvent(Event Event, CommandSender sender);

    /**
     * Removes a {@link Event} from this manager by a
     * given {@link CommandSender}.
     *
     * @param Event the {@link Event} to remove
     * @param sender  the {@link CommandSender} of removal
     * @return true if the {@link Event} was successfully removed
     */
    boolean removeEvent(Event Event, CommandSender sender);

    /**
     * Loads the {@link Event} data from storage.
     */
    void reloadEventData();

    /**
     * Saves the {@link Event} data to storage.
     */
    void saveEventData();
}