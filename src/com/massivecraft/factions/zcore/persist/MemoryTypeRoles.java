package com.massivecraft.factions.zcore.persist;

import java.lang.reflect.Field;

import com.google.gson.JsonObject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
public class MemoryTypeRoles {

	protected boolean canInviteMembers;
	protected boolean canKickMembers;
	protected boolean canDemoteMembers;
	protected boolean canPromoteMembers;
	protected boolean canEditRelations;
	protected boolean canSetFHome;
	protected boolean canSetFWarp;
	protected boolean canEditTag;
	protected boolean canEditTitles;
	protected boolean canUnclaimAll;
	protected boolean canEditFPerms;

	public MemoryTypeRoles(JsonObject object) {
		for (Field field: MemoryTypeRoles.class.getDeclaredFields()) {
			if (field.getType().isPrimitive() && object.has(field.getName())) {
				field.setAccessible(true);
				try {
					field.setBoolean(this, object.get(field.getName()).getAsBoolean());
				} catch (ReflectiveOperationException exception) {
					exception.printStackTrace();
				}
			}
		}
	}

	public JsonObject writeToJson() {
		JsonObject object = new JsonObject();
		for (Field field: MemoryTypeRoles.class.getDeclaredFields()) {
			if (field.getType().isPrimitive()) {
				field.setAccessible(true);
				try {
					object.addProperty(field.getName(), (Boolean) field.get(this));
				} catch (ReflectiveOperationException exception) {
					exception.printStackTrace();
				}
			}
		}
		return object;
	}
}