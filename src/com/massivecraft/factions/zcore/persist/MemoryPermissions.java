package com.massivecraft.factions.zcore.persist;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor @NoArgsConstructor
public class MemoryPermissions {

	protected boolean fullAccess;
	protected boolean canUseChests;
	protected boolean canModifyClaims;
	protected boolean canInteractAllBlocks;
	protected boolean canInteractSponges;
	protected boolean canInteractDispensers;
	protected boolean canInteractDroppers;
	protected boolean canPlaceEggs;
	protected boolean canInteractTNT;
	protected boolean canInteractObsidian;
	protected boolean canInteractSpawners;
	protected boolean canInteractHoppers;
	protected boolean canInteractDoorsOrGates;
	protected boolean canInteractPressurePlates;
	protected boolean canInteractLevers;
	protected boolean canInteractButtons;
	protected boolean canInteractTrapdoors;
	protected List<Pair<Integer, Integer>> insertedChunks = new ArrayList<>();

	public MemoryPermissions(JsonObject object) {
		if (insertedChunks == null) { // idk the instantiation order
			insertedChunks = new ArrayList<>();
		}
		for (Field field: MemoryPermissions.class.getDeclaredFields()) {
			if (field.getType().isPrimitive() && object.has(field.getName())) {
				field.setAccessible(true);
				try {
					field.setBoolean(this, object.get(field.getName()).getAsBoolean());
				} catch (ReflectiveOperationException exception) {
					exception.printStackTrace();
				}
			}
		}
		if (object.has("insertedChunks")) {
			JsonArray chunks = object.get("insertedChunks").getAsJsonArray();
			for (JsonElement element: chunks) {
				if (element.isJsonObject()) {
					JsonObject other = element.getAsJsonObject();
					insertedChunks.add(Pair.of(other.get("x").getAsInt(), other.get("z").getAsInt()));
				}
			}
		}
	}

	public JsonObject writeToJson() {
		JsonObject object = new JsonObject();
		JsonArray chunks = new JsonArray();
		for (Field field: MemoryPermissions.class.getDeclaredFields()) {
			if (field.getType().isPrimitive()) {
				field.setAccessible(true);
				try {
					object.addProperty(field.getName(), (Boolean) field.get(this));
				} catch (ReflectiveOperationException exception) {
					exception.printStackTrace();
				}
			}
		}
		for (Pair<Integer, Integer> pair: insertedChunks) {
			JsonObject coordinates = new JsonObject();
			coordinates.addProperty("x", pair.getLeft());
			coordinates.addProperty("z", pair.getRight());
			chunks.add(coordinates);
		}
		object.add("insertedChunks", chunks);
		return object;
	}


}
