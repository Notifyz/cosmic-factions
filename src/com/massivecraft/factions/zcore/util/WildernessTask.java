package com.massivecraft.factions.zcore.util;

import java.lang.ref.WeakReference;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.massivecraft.factions.P;

import lombok.Data;

@Data
public final class WildernessTask implements Runnable {

    private static final Random random = new Random();

    private final int taskId;

    private final WeakReference<Player> player;

    private final World world;

    private final P plugin;

    private final WildernessWorldSettings worldSettings;

    private int searchCount;

    private WildernessFactionsTask factionsTask;

    public WildernessTask(P plugin, Player player, World world, WildernessWorldSettings worldSettings) {
        taskId = Bukkit.getScheduler().runTaskTimer(plugin, this, 1, 1).getTaskId();
        this.plugin = plugin;
        this.player = new WeakReference<>(player);
        this.world = world;
        this.worldSettings = worldSettings;
    }

    @Override
    public void run() {
        // Cancel task if the player went offline.
        Player player = this.player.get();
        if (player == null || !player.isOnline()) {
            cancel();
            return;
        }

        if (factionsTask != null) {
            switch (factionsTask.getState()) {
                case SEARCHING:
                    return;
                case ALLOWED:
                    // Teleport player to the destination.
                    int x = factionsTask.getX();
                    int y = factionsTask.getY();
                    int z = factionsTask.getZ();
                    player.teleport(new Location(world, x + 0.5, y + 0.5, z + 0.5));
                    player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 90, 0));
                    // Inform the player the teleportation was a success.
                    player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!) &eYou have been teleported to a random spot in the Wilderness."));

                    // Stop looking for any new locations.
                    cancel();
                    return;
                case DENIED:
                    factionsTask = null;
            }
        }

        // Cancel task if there have been too many search attempts.
        if (++searchCount > 100) {
            cancel();
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cSorry, we couldn't find a spot for you."));
            return;
        }

        // Generate a random set of coordinates.
        int x = worldSettings.getCenterX() + random.nextInt(worldSettings.getRange()) - (worldSettings.getRange() / 2);
        int z = worldSettings.getCenterZ() + random.nextInt(worldSettings.getRange()) - (worldSettings.getRange() / 2);
        int y = world.getHighestBlockYAt(x, z);

        // Do nothing if destination is not safe.

        // Check to see if the location is surrounded by wilderness.
        factionsTask = new WildernessFactionsTask(x, y, z, 10, world);
        Bukkit.getScheduler().runTaskAsynchronously(plugin, factionsTask);
    }

    public void cancel() {
        Bukkit.getScheduler().cancelTask(taskId);
    }

}