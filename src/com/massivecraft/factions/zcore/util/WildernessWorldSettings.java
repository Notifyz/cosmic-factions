package com.massivecraft.factions.zcore.util;

import lombok.Data;

@Data
public final class WildernessWorldSettings {

    private int centerX = 0;

    private int centerZ = 0;

    private int range = 3000;
}