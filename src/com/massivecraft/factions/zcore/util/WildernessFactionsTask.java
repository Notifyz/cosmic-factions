package com.massivecraft.factions.zcore.util;

import org.bukkit.Location;
import org.bukkit.World;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;

import lombok.Data;
@Data
public final class WildernessFactionsTask implements Runnable {

    private final int x, y, z, radius;

    private final World world;

    private volatile State state = State.SEARCHING;

    @Override
    public void run() {
        for (int chunkX = -radius; chunkX <= radius; chunkX++) {
            for (int chunkZ = -radius; chunkZ <= radius; chunkZ++) {
                Location location = new Location(world, (chunkX << 4) + x, y, (chunkZ << 4) + z);
                if (!Board.getInstance().getFactionAt(new FLocation(location)).isWilderness()) {
                    state = State.DENIED;
                    return;
                }
            }
        }

        state = State.ALLOWED;
    }

    public enum State {

        SEARCHING, ALLOWED, DENIED

    }

}