package com.massivecraft.factions.cmd;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

public class CmdTeamLocation implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;
		Faction faction = FPlayers.getInstance().getByPlayer(player).getFaction();

        if (faction == null || faction.isNone()) {
        	player.sendMessage(ChatColor.RED + "You are not member of any faction.");
            return false;
        }

        faction.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!) &a" + player.getName() + "'s coordinates: &f&l" + player.getLocation().getBlockX() + "x " + player.getLocation().getBlockY() + "y " + player.getLocation().getBlockZ() + "z &e&l(!)"));
		return false;
	}

}
