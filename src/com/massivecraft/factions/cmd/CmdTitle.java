package com.massivecraft.factions.cmd;

import org.bukkit.ChatColor;

import com.massivecraft.factions.Conf;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.Permission;
import com.massivecraft.factions.struct.Role;
import com.massivecraft.factions.zcore.persist.MemoryRoles;
import com.massivecraft.factions.zcore.persist.MemoryTypeRoles;
import com.massivecraft.factions.zcore.util.TL;
import com.massivecraft.factions.zcore.util.TextUtil;

public class CmdTitle extends FCommand {

    public CmdTitle() {
        this.aliases.add("title");

        this.requiredArgs.add("player name");
        this.optionalArgs.put("title", "");

        this.permission = Permission.TITLE.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeModerator = false;
        senderMustBeAdmin = false;
    }

    @Override
    public void perform() {
        FPlayer you = this.argAsBestFPlayerMatch(0);
        if (you == null) {
            return;
        }

        if ((!containsRolePermission(fme.getRole(), myFaction)) && (!myFaction.getPlayerRoles().getOrDefault(me.getName(), new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanEditTitles())) {
        	me.sendMessage(ChatColor.RED + "Error, you don't have permissions.");
        	return;
        }

        args.remove(0);
        String title = TextUtil.implode(args, " ");

        title = title.replaceAll(",", "");

        if (!canIAdministerYou(fme, you)) {
            return;
        }

        // if economy is enabled, they're not on the bypass list, and this command has a cost set, make 'em pay
        if (!payForCommand(Conf.econCostTitle, TL.COMMAND_TITLE_TOCHANGE, TL.COMMAND_TITLE_FORCHANGE)) {
            return;
        }

        you.setTitle(sender, title);

        // Inform
        myFaction.msg(TL.COMMAND_TITLE_CHANGED, fme.describeTo(myFaction, true), you.describeTo(myFaction, true));
    }

    public boolean containsRolePermission(Role role, Faction faction) {
    	switch (role) {
		case ADMIN:
			return true;
		case COLEADER:
			return myFaction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true)).isCanEditTitles();
		case MODERATOR:
			return myFaction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false)).isCanEditTitles();
		case NORMAL:
			return myFaction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanEditTitles();
		case RECRUIT:
			return myFaction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanEditTitles();
		default:
			break;

    	}
    	return false;
    }

    @Override
    public TL getUsageTranslation() {
        return TL.COMMAND_TITLE_DESCRIPTION;
    }

}
