package com.massivecraft.factions.cmd;

import org.apache.commons.lang3.tuple.Pair;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;

import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.util.InventoryUtil;
import com.massivecraft.factions.zcore.persist.MemoryPermissions;
import com.massivecraft.factions.zcore.util.TL;

public class CmdAccess extends FCommand {

    public CmdAccess() {
        super();
        this.aliases.add("access");

        this.disableOnLock = true;

        this.optionalArgs.put("p", "player");
        this.optionalArgs.put("f", "faction");
        this.optionalArgs.put("list", "list");
        this.optionalArgs.put("clear", "clear");
        this.optionalArgs.put("yes", "yes");
        this.optionalArgs.put("no", "no");
        this.optionalArgs.put("none", "none");
        this.optionalArgs.put("all", "all");

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeAdmin = true;
    }

    @Override
    public void perform() { // /f access <p/f> <name> <yes/no/none/all>
        Faction faction = this.argAsFaction(0, myFaction);
        if (faction == null) {
        	me.sendMessage(ChatColor.RED + "You are not member of any faction.");
            return;
        }

        if (args.size() == 0) {
            me.openInventory(InventoryUtil.getFAccessMenu());
            return;
        } else if (args.size() == 2) { // Default perms
        	boolean chosenFaction = args.get(0).equalsIgnoreCase("f");
        	Chunk chunk = me.getLocation().getChunk();
        	if (chosenFaction) {
        		Faction searchFaction = Factions.getInstance().getByTag(args.get(1));

        		if (searchFaction == null) {
        			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
        			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
        			return;
        		}

        		if (faction.getFactionDefaultFPermissions().containsKey(searchFaction.getTag())) {
        			me.sendMessage(ChatColor.RED + searchFaction.getTag() + " already has a default permission list!");
        			me.sendMessage(ChatColor.GRAY + "Use '/f access' to view their permissions!");
        			return;
        		}

        		MemoryPermissions permission = new MemoryPermissions();
        		permission.getInsertedChunks().add(Pair.of(chunk.getX(), chunk.getZ()));
        		faction.getFactionDefaultFPermissions().put(searchFaction.getTag(), permission);
        		me.sendMessage(ChatColor.RED + searchFaction.getTag() + " has been granted a default permission set.");
        	} else {
        		Player target = Bukkit.getPlayer(args.get(1));

        		if (target == null) {
        			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
        			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
        			return;
        		}

        		if (faction.getPlayerDefaultFPermissions().containsKey(target.getName())) {
        			me.sendMessage(ChatColor.RED + target.getName() + " already has a default permission list!");
        			me.sendMessage(ChatColor.GRAY + "Use '/f access' to view their permissions!");
        			return;
        		}

        		MemoryPermissions permission = new MemoryPermissions();
        		permission.getInsertedChunks().add(Pair.of(chunk.getX(), chunk.getZ()));
        		faction.getPlayerDefaultFPermissions().put(target.getName(), permission);
        		me.sendMessage(ChatColor.RED + target.getName() + " has been granted a default permission set.");
        	}
        	return;
        } else if (args.size() == 3) {
        	boolean chosenFaction = args.get(0).equalsIgnoreCase("f");
            Chunk chunk = me.getLocation().getChunk();
        	if (chosenFaction) {
        		Faction searchFaction = Factions.getInstance().getByTag(args.get(1));
        		switch (args.get(2).toLowerCase()) {
        		case "yes":
            		if (searchFaction == null) {
            			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
            			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
            			return;
            		}

            		if (faction.getFactionFPermissions().containsKey(searchFaction.getTag())) {
            			me.sendMessage(ChatColor.RED + "That faction already has access to this chunk.");
            			return;
            		}

            		MemoryPermissions permission = new MemoryPermissions();
            		permission.getInsertedChunks().add(Pair.of(chunk.getX(), chunk.getZ()));
            		faction.getFactionFPermissions().put(searchFaction.getTag(), permission);
            		me.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&l(!) &aThe faction " + searchFaction.getTag() + " now has access to the claim " + chunk.getX() + "x " + chunk.getZ() + "z! Use /f perm to customize what they can edit in this chunk."));
        			break;
        		case "no":
            		if (searchFaction == null) {
            			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
            			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
            			return;
            		}

            		if (!faction.getFactionFPermissions().containsKey(searchFaction.getTag())) {
            			me.sendMessage(ChatColor.RED + "That faction does not have access to this chunk.");
            			return;
            		}

            		if (!faction.getFactionFPermissions().get(searchFaction.getTag()).getInsertedChunks().contains(Pair.of(chunk.getX(), chunk.getZ()))) {
            			me.sendMessage(ChatColor.RED + "That faction does not have access to this chunk.");
            			return;
            		}

            		faction.getFactionFPermissions().get(searchFaction.getTag()).getInsertedChunks().remove(Pair.of(chunk.getX(), chunk.getZ()));
            		me.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cThe faction " + searchFaction.getTag() + " no longer has access to the claim " + chunk.getX() + "x " + chunk.getZ() + "z!"));
        			break;
        		case "none":
        			if (searchFaction == null) {
            			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
            			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
            			return;
            		}

            		if (!faction.getFactionFPermissions().containsKey(searchFaction.getTag())) {
            			me.sendMessage(ChatColor.RED + "That faction did not have access to any of your land.");
            			return;
            		}

            		faction.getFactionFPermissions().remove(searchFaction.getTag());
            		me.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &n" + searchFaction.getTag() + "&c no longer has access to any of your land."));
        			break;
        		case "all":
        			if (searchFaction == null) {
            			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
            			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
            			return;
            		}
            		MemoryPermissions permissions = new MemoryPermissions();
            		faction.getAllClaims().stream().map(FLocation::getChunk).forEach(chunks -> permissions.getInsertedChunks().add(Pair.of(chunks.getX(), chunks.getZ())));
            		faction.getFactionFPermissions().put(searchFaction.getTag(), permissions);
            		me.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!) &eThe faction " + searchFaction.getTag() + " now has access to all of your land."));
        			break;
        		}
        	} else {
        		Player target = Bukkit.getPlayer(args.get(1));
        		switch (args.get(2).toLowerCase()) {
        		case "yes":
            		if (target == null) {
            			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
            			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
            			return;
            		}

            		if (faction.getPlayerFPermissions().containsKey(target.getName())) {
            			me.sendMessage(ChatColor.RED + "That player already has access to this chunk.");
            			return;
            		}

            		MemoryPermissions permission = new MemoryPermissions();
            		permission.getInsertedChunks().add(Pair.of(chunk.getX(), chunk.getZ()));
            		faction.getPlayerFPermissions().put(target.getName(), permission);
            		me.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a&l(!) &aThe player " + target.getName() + " now has access to the claim " + chunk.getX() + "x " + chunk.getZ() + "z! Use /f perm to customize what they can edit in this chunk."));
        			break;
        		case "no":
            		if (target == null) {
            			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
            			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
            			return;
            		}

            		if (!faction.getPlayerFPermissions().containsKey(target.getName())) {
            			me.sendMessage(ChatColor.RED + "That player does not have access to this chunk.");
            			return;
            		}

            		if (!faction.getPlayerFPermissions().get(target.getName()).getInsertedChunks().contains(Pair.of(chunk.getX(), chunk.getZ()))) {
            			me.sendMessage(ChatColor.RED + "That player does not have access to this chunk.");
            			return;
            		}

            		faction.getPlayerFPermissions().get(target.getName()).getInsertedChunks().remove(Pair.of(chunk.getX(), chunk.getZ()));
            		me.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &cThe player " + target.getName() + " no longer has access to the claim " + chunk.getX() + "x " + chunk.getZ() + "z!"));
        			break;
        		case "none":
        			if (target == null) {
            			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
            			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
            			return;
            		}

            		if (!faction.getPlayerFPermissions().containsKey(target.getName())) {
            			me.sendMessage(ChatColor.RED + "That player did not have access to any of your land.");
            			return;
            		}

            		faction.getPlayerFPermissions().remove(target.getName());
            		me.sendMessage(ChatColor.translateAlternateColorCodes('&', "&c&l(!) &n" + target.getName() + "&c no longer has access to any of your land."));
        			break;
        		case "all":
        			if (target == null) {
            			me.sendMessage(ChatColor.translateAlternateColorCodes('&',
            			"&c&l(!) &cThe faction or player ''&d" + args.get(1) + "&c'' could not be found."));
            			return;
            		}
            		MemoryPermissions permissions = new MemoryPermissions();
            		faction.getAllClaims().stream().map(FLocation::getChunk).forEach(chunks -> permissions.getInsertedChunks().add(Pair.of(chunks.getX(), chunks.getZ())));
            		faction.getPlayerFPermissions().put(target.getName(), permissions);
            		me.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e&l(!) &eThe player " + target.getName() + " now has access to all of your land."));
        			break;
        		}
        	}
        	return;
        } else {
        	me.sendMessage(ChatColor.RED + "Invalid arguments.");
        	me.sendMessage(ChatColor.RED + "/f access <p/f/list/clear> <name> <yes/no/none/all>");
        	return;
        }
    }

    @Override
    public TL getUsageTranslation() {
        return TL.COMMAND_PERM_DESCRIPTION;
    }

}