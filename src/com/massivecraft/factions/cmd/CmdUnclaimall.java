package com.massivecraft.factions.cmd;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.Conf;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.P;
import com.massivecraft.factions.event.LandUnclaimAllEvent;
import com.massivecraft.factions.integration.Econ;
import com.massivecraft.factions.struct.Permission;
import com.massivecraft.factions.struct.Role;
import com.massivecraft.factions.zcore.persist.MemoryRoles;
import com.massivecraft.factions.zcore.persist.MemoryTypeRoles;
import com.massivecraft.factions.zcore.util.TL;

public class CmdUnclaimall extends FCommand {

    public CmdUnclaimall() {
        this.aliases.add("unclaimall");
        this.aliases.add("declaimall");

        //this.requiredArgs.add("");
        //this.optionalArgs.put("", "");

        this.permission = Permission.UNCLAIM_ALL.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = false;
        senderMustBeModerator = false;
        senderMustBeAdmin = false;
    }

    @Override
    public void perform() {
        if ((!containsRolePermission(fme.getRole(), myFaction)) && (!myFaction.getPlayerRoles().getOrDefault(me.getName(), new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanUnclaimAll())) {
        	me.sendMessage(ChatColor.RED + "Error, you don't have permissions.");
        	return;
        }

        if (Econ.shouldBeUsed()) {
            double refund = Econ.calculateTotalLandRefund(myFaction.getLandRounded());
            if (Conf.bankEnabled && Conf.bankFactionPaysLandCosts) {
                if (!Econ.modifyMoney(myFaction, refund, TL.COMMAND_UNCLAIMALL_TOUNCLAIM.toString(), TL.COMMAND_UNCLAIMALL_FORUNCLAIM.toString())) {
                    return;
                }
            } else {
                if (!Econ.modifyMoney(fme, refund, TL.COMMAND_UNCLAIMALL_TOUNCLAIM.toString(), TL.COMMAND_UNCLAIMALL_FORUNCLAIM.toString())) {
                    return;
                }
            }
        }

        LandUnclaimAllEvent unclaimAllEvent = new LandUnclaimAllEvent(myFaction, fme);
        Bukkit.getServer().getPluginManager().callEvent(unclaimAllEvent);
        if (unclaimAllEvent.isCancelled()) {
            return;
        }

        Board.getInstance().unclaimAll(myFaction.getId());
        myFaction.msg(TL.COMMAND_UNCLAIMALL_UNCLAIMED, fme.describeTo(myFaction, true));

        if (Conf.logLandUnclaims) {
            P.p.log(TL.COMMAND_UNCLAIMALL_LOG.format(fme.getName(), myFaction.getTag()));
        }
    }

    public boolean containsRolePermission(Role role, Faction faction) {
    	switch (role) {
		case ADMIN:
			return true;
		case COLEADER:
			return myFaction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true)).isCanUnclaimAll();
		case MODERATOR:
			return myFaction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false)).isCanUnclaimAll();
		case NORMAL:
			return myFaction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanUnclaimAll();
		case RECRUIT:
			return myFaction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanUnclaimAll();
		default:
			break;

    	}
    	return false;
    }

    @Override
    public TL getUsageTranslation() {
        return TL.COMMAND_UNCLAIMALL_DESCRIPTION;
    }

}
