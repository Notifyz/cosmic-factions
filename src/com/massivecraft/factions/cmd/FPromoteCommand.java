package com.massivecraft.factions.cmd;

import org.bukkit.ChatColor;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.Permission;
import com.massivecraft.factions.struct.Role;
import com.massivecraft.factions.zcore.fperms.Access;
import com.massivecraft.factions.zcore.fperms.PermissableAction;
import com.massivecraft.factions.zcore.persist.MemoryRoles;
import com.massivecraft.factions.zcore.persist.MemoryTypeRoles;
import com.massivecraft.factions.zcore.util.TL;

public class FPromoteCommand extends FCommand {

    public int relative = 0;

    public FPromoteCommand() {
        super();

        this.requiredArgs.add("player");

        this.permission = Permission.PROMOTE.node;
        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;
        senderMustBeAdmin = false;
    }

    @Override
    public void perform() {
        FPlayer target = this.argAsBestFPlayerMatch(0);
        if (target == null) {
            msg(TL.GENERIC_NOPLAYERFOUND, this.argAsString(0));
            return;
        }

        if (!target.getFaction().equals(myFaction)) {
            msg(TL.COMMAND_PROMOTE_WRONGFACTION, target.getName());
            return;
        }

        Access access = myFaction.getAccess(fme.getRole(), PermissableAction.PROMOTE);

        // Well this is messy.
        if ((access == null || access == Access.DENY || (access == Access.UNDEFINED)) && ( (!containsRolePermission(fme.getRole(), myFaction)) && (!myFaction.getPlayerRoles().containsKey(me.getName())))) {
            fme.msg(TL.COMMAND_NOACCESS);
            return;
        }

        if ((!containsRolePermission(fme.getRole(), myFaction)) && (relative > 0 ? (!myFaction.getPlayerRoles().getOrDefault(me.getName(), new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanPromoteMembers()) : (!myFaction.getPlayerRoles().getOrDefault(me.getName(), new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanDemoteMembers()))) {
        	me.sendMessage(ChatColor.RED + "Error, you don't have permissions.");
        	return;
        }

        Role current = target.getRole();
        Role promotion = Role.getRelative(current, +relative);

        if (promotion == null) {
            fme.msg(TL.COMMAND_PROMOTE_NOTTHATPLAYER);
            return;
        }

        // Don't allow people to promote people to their same or higher rnak.
        if (fme.getRole().value <= promotion.value) {
            fme.msg(TL.COMMAND_PROMOTE_NOT_ALLOWED);
            return;
        }

        String action = relative > 0 ? TL.COMMAND_PROMOTE_PROMOTED.toString() : TL.COMMAND_PROMOTE_DEMOTED.toString();

        // Success!
        target.setRole(promotion);
        if (target.isOnline()) {
            target.msg(TL.COMMAND_PROMOTE_TARGET, action, promotion.nicename);
        }

        fme.msg(TL.COMMAND_PROMOTE_SUCCESS, action, target.getName(), promotion.nicename);
    }

    @Override
    public TL getUsageTranslation() {
        return TL.COMMAND_PROMOTE_DESCRIPTION;
    }

    public boolean containsRolePermission(Role role, Faction faction) {
    	if (relative > 0) {
			switch (role) {
			case ADMIN:
				return true;
			case COLEADER:
				return myFaction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true)).isCanPromoteMembers();
			case MODERATOR:
				return myFaction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false)).isCanPromoteMembers();
			case NORMAL:
				return myFaction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanPromoteMembers();
			case RECRUIT:
				return myFaction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanPromoteMembers();
			default:
				break;
			}
    	} else {
			switch (role) {
			case ADMIN:
				return true;
			case COLEADER:
				return myFaction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true)).isCanDemoteMembers();
			case MODERATOR:
				return myFaction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false)).isCanDemoteMembers();
			case NORMAL:
				return myFaction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanDemoteMembers();
			case RECRUIT:
				return myFaction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanDemoteMembers();
			default:
				break;
			}
		}
    	return false;
    }
}
