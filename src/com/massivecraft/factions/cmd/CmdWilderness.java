package com.massivecraft.factions.cmd;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.P;
import com.massivecraft.factions.zcore.util.WildernessTask;
import com.massivecraft.factions.zcore.util.WildernessWorldSettings;

public class CmdWilderness implements CommandExecutor {

	public Map<UUID, Long> cooldown = new HashMap<>();

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			return false;
		}

		Player player = (Player) sender;

		if (cooldown.containsKey(player.getUniqueId())) {
			if ((cooldown.get(player.getUniqueId()) + TimeUnit.MINUTES.toMillis(5L)) > System.currentTimeMillis()) {
                player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&cYou &ncannot&c use /wilderness as you are on cooldown!"));
				return false;
			}
		}

		if ((!player.getWorld().getName().equalsIgnoreCase("world")) &&
			(!player.getWorld().getName().equalsIgnoreCase("world2")) &&
			(!player.getWorld().getName().equalsIgnoreCase("level")) &&
			(!player.getWorld().getName().equals("level2"))) {
			player.sendMessage(ChatColor.RED + "You can't use wilderness in current world.");
			return false;
		}

		cooldown.put(player.getUniqueId(), System.currentTimeMillis());
		new WildernessTask(P.getInstance(), player, player.getWorld(), new WildernessWorldSettings());
		return false;
	}

    public static String formatElapsingMiliseconds(long timestamp) {

    	timestamp = System.currentTimeMillis() - timestamp;

        Calendar cal = Calendar.getInstance();

        cal.set(Calendar.SECOND, adjustTime(timestamp, TimeUnit.MILLISECONDS, TimeUnit.SECONDS));
        timestamp -= TimeUnit.SECONDS.toNanos(TimeUnit.MILLISECONDS.toSeconds(timestamp));

        cal.set(Calendar.MINUTE, adjustTime(timestamp, TimeUnit.MILLISECONDS, TimeUnit.MINUTES));
        timestamp -= TimeUnit.MINUTES.toNanos(TimeUnit.MILLISECONDS.toMinutes(timestamp));

        cal.set(Calendar.HOUR_OF_DAY, adjustTime(timestamp, TimeUnit.MILLISECONDS, TimeUnit.HOURS));

        return new SimpleDateFormat("mm:ss").format(cal.getTime());
    }

    public static int adjustTime(long timestamp, TimeUnit from, TimeUnit to) {
        return (int) to.convert(timestamp, from);
    }
}
