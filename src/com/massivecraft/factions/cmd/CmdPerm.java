package com.massivecraft.factions.cmd;

import org.bukkit.ChatColor;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.Role;
import com.massivecraft.factions.util.InventoryUtil;
import com.massivecraft.factions.zcore.persist.MemoryRoles;
import com.massivecraft.factions.zcore.persist.MemoryTypeRoles;
import com.massivecraft.factions.zcore.util.TL;

public class CmdPerm extends FCommand {

    public CmdPerm() {
        super();
        this.aliases.add("perm");
        this.aliases.add("perms");
        this.aliases.add("permission");
        this.aliases.add("permissions");

        this.optionalArgs.put("relation", "relation");
        this.optionalArgs.put("action", "action");
        this.optionalArgs.put("access", "access");


        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeModerator = false;

    }

    @Override
    public void perform() {
        Faction faction = this.argAsFaction(0, myFaction);
        if (faction == null) {
        	me.sendMessage(ChatColor.RED + "You are not member of any faction.");
            return;
        }

        if ((!containsRolePermission(fme.getRole(), myFaction)) && (!myFaction.getPlayerRoles().getOrDefault(me.getName(), new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanEditFPerms())) {
        	me.sendMessage(ChatColor.RED + "Error, you don't have permissions.");
        	return;
        }

        if (args.size() == 0) {
            me.openInventory(InventoryUtil.getFPermissionMenu());
            return;
        }
    }

    public boolean containsRolePermission(Role role, Faction faction) {
    	switch (role) {
		case ADMIN:
			return true;
		case COLEADER:
			return myFaction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true)).isCanEditFPerms();
		case MODERATOR:
			return myFaction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false)).isCanEditFPerms();
		case NORMAL:
			return myFaction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanEditFPerms();
		case RECRUIT:
			return myFaction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanEditFPerms();
		default:
			break;

    	}
    	return false;
    }

    @Override
    public TL getUsageTranslation() {
        return TL.COMMAND_PERM_DESCRIPTION;
    }

}