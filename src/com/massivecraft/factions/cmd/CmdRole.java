package com.massivecraft.factions.cmd;

import org.bukkit.ChatColor;

import com.massivecraft.factions.Faction;
import com.massivecraft.factions.util.InventoryUtil;
import com.massivecraft.factions.zcore.util.TL;

public class CmdRole extends FCommand {

    public CmdRole() {
        super();
        this.aliases.add("role");
        this.aliases.add("roles");

        this.disableOnLock = true;

        senderMustBePlayer = true;
        senderMustBeMember = true;
        senderMustBeAdmin = true;
    }

    @Override
    public void perform() {
        Faction faction = this.argAsFaction(0, myFaction);
        if (faction == null) {
        	me.sendMessage(ChatColor.RED + "You are not member of any faction.");
            return;
        }

        if (args.size() == 0) {
            me.openInventory(InventoryUtil.getFRolesInventory(faction));
            return;
        }
    }

    @Override
    public TL getUsageTranslation() {
        return TL.COMMAND_PERM_DESCRIPTION;
    }

}