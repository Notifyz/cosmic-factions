package com.massivecraft.factions.cmd;

import org.bukkit.ChatColor;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.P;
import com.massivecraft.factions.struct.Permission;
import com.massivecraft.factions.struct.Relation;
import com.massivecraft.factions.struct.Role;
import com.massivecraft.factions.util.LazyLocation;
import com.massivecraft.factions.zcore.fperms.Access;
import com.massivecraft.factions.zcore.fperms.PermissableAction;
import com.massivecraft.factions.zcore.persist.MemoryRoles;
import com.massivecraft.factions.zcore.persist.MemoryTypeRoles;
import com.massivecraft.factions.zcore.util.TL;

public class CmdSetFWarp extends FCommand {

    public CmdSetFWarp() {
        super();

        this.aliases.add("setwarp");
        this.aliases.add("sw");

        this.requiredArgs.add("warp name");
        this.optionalArgs.put("password", "password");

        this.senderMustBeMember = true;
        this.senderMustBeModerator = false;
        this.senderMustBePlayer = true;

        this.permission = Permission.SETWARP.node;
    }

    @Override
    public void perform() {
        if (!(fme.getRelationToLocation() == Relation.MEMBER)) {
            fme.msg(TL.COMMAND_SETFWARP_NOTCLAIMED);
            return;
        }

        Access access = myFaction.getAccess(fme, PermissableAction.SETWARP);
        // This statement allows us to check if they've specifically denied it, or default to
        // the old setting of allowing moderators to set warps.
        if ((access == Access.DENY || (access == Access.UNDEFINED)) && ( (!containsRolePermission(fme.getRole(), myFaction)) && (!myFaction.getPlayerRoles().containsKey(me.getName())))) {
            fme.msg(TL.GENERIC_NOPERMISSION, "set warp");
            return;
        }

        if ((!containsRolePermission(fme.getRole(), myFaction)) && (!myFaction.getPlayerRoles().getOrDefault(me.getName(), new MemoryRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanInviteMembers())) {
        	me.sendMessage(ChatColor.RED + "Error, you don't have permissions.");
        	return;
        }

        int maxWarps = P.p.getConfig().getInt("max-warps", 5);
        if (maxWarps <= myFaction.getWarps().size()) {
            fme.msg(TL.COMMAND_SETFWARP_LIMIT, maxWarps);
            return;
        }

        if (!transact(fme)) {
            return;
        }

        String warp = argAsString(0);
        String password = argAsString(1);

        LazyLocation loc = new LazyLocation(fme.getPlayer().getLocation());
        myFaction.setWarp(warp, loc);
        if (password != null) {
            myFaction.setWarpPassword(warp, password);
        }
        fme.msg(TL.COMMAND_SETFWARP_SET, warp, password != null ? password : "");
    }

    private boolean transact(FPlayer player) {
        return !P.p.getConfig().getBoolean("warp-cost.enabled", false) || player.isAdminBypassing() || payForCommand(P.p.getConfig().getDouble("warp-cost.setwarp", 5), TL.COMMAND_SETFWARP_TOSET.toString(), TL.COMMAND_SETFWARP_FORSET.toString());
    }

    public boolean containsRolePermission(Role role, Faction faction) {
    	switch (role) {
		case ADMIN:
			return true;
		case COLEADER:
			return myFaction.getTypeRoles().getOrDefault(Role.COLEADER, new MemoryTypeRoles(true, true, true, true, true, true, true, true, true, true, true)).isCanSetFWarp();
		case MODERATOR:
			return myFaction.getTypeRoles().getOrDefault(Role.MODERATOR, new MemoryTypeRoles(true, true, false, false, true, true, true, false, false, false, false)).isCanSetFWarp();
		case NORMAL:
			return myFaction.getTypeRoles().getOrDefault(Role.NORMAL, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanSetFWarp();
		case RECRUIT:
			return myFaction.getTypeRoles().getOrDefault(Role.RECRUIT, new MemoryTypeRoles(false, false, false, false, false, false, false, false, false, false, false)).isCanSetFWarp();
		default:
			break;

    	}
    	return false;
    }

    @Override
    public TL getUsageTranslation() {
        return TL.COMMAND_SETFWARP_DESCRIPTION;
    }
}
