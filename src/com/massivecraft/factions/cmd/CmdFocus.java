package com.massivecraft.factions.cmd;

import org.bukkit.ChatColor;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.zcore.util.TL;

public class CmdFocus
        extends FCommand {
    public CmdFocus() {
       aliases.add("focus");

       requiredArgs.add("player");


       senderMustBePlayer = true;
       senderMustBeMember = false;
       senderMustBeModerator = true;
       senderMustBeAdmin = false;
    }

    @Override
	public void perform() {
        FPlayer target = argAsFPlayer(0);
        if (target == null) {
            return;
        }
        if (target.getFaction().getId().equalsIgnoreCase(myFaction.getId())) {
            fme.sendMessage(ChatColor.RED + "You can't focus members in your faction.");
            return;
        }
        if ((myFaction.getFocused() != null) && (myFaction.getFocused().equalsIgnoreCase(target.getName()))) {
            myFaction.setFocused(null);
            myFaction.sendMessage(ChatColor.RED + "Your faction is no longer focusing " + target.getName());
            return;
        }
        myFaction.sendMessage(ChatColor.GREEN + ChatColor.BOLD.toString() + "Your faction is now focusing " + ChatColor.YELLOW + target.getName());
        myFaction.setFocused(target.getName());
    }

	@Override
	public TL getUsageTranslation() {
		return TL._DEFAULT;
	}
}

